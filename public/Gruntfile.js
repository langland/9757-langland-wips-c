var pkg = require('./package.json');

module.exports = function(grunt) {

    grunt.loadNpmTasks("grunt-contrib-less");
    grunt.loadNpmTasks("grunt-closure-tools");
    grunt.loadNpmTasks('grunt-contrib-watch');
	
    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),

        less: {
            development: {
                options: {
                    compress: true
                },
                files: {
                    "css/main.css": "source/less/main.less"
                }
            }
        },
        
         closureCompiler:  {

            options: {
                compilerFile: "closure/compiler.jar",
                checkModified: true,

                compilerOpts: {
                    compilation_level: "SIMPLE_OPTIMIZATIONS"
                },
                execOpts: {
                   maxBuffer: 999999 * 1024
                }
            },

            frontend: {
                src: pkg.jsSources,
                dest: "js/wips.js"
            }
        },
        
        
        watch: {
            less: {
                files: ['source/less/*'],
                tasks: ['less']
            },
            scripts: {
                files: ['source/js/**/*.js'],
                tasks: ['closureCompiler']
            }
        },
        
    });

    //grunt.registerTask("default", ["less", "closureCompiler"]);
    grunt.registerTask("default", ["watch"]);
    grunt.registerTask("all", ["less", "closureCompiler"]);
};



