angular.module("wips.controllers", [
    'ngSanitize',
    
    'wips.services',
    'wips.directives',
    'angularModalService',
    'bootstrap3-typeahead',
    'ngRoute',
    'angular-loading-bar', 
    'angular-click-outside',
])
//.run(['$anchorScroll', function($anchorScroll) {
//  $anchorScroll.yOffset = 0;   // always scroll by 50 extra pixels
///}])
.controller("WipsController", [
//'$anchorScroll',            
    "$scope",
    "$http",
    "$location",
    "$filter",
    "$window",
    "WipsService",
    "AdminService",
    "ModalService",
    "$log",
    "$document",
    "$timeout",
    "$element",
    function (
    //    $anchorScroll,
        $scope,
        $http,
        $location,
        $filter,
        $window,
        WipsService,
        AdminService,
        ModalService,
        $log,
        $document,
        $timeout,
        $element) {
              
              
            // set to false in production
            $scope.showDebug = false;
            $scope.showDebugMessage = false;
            
            $scope.setDebugInfo = function($info) {
                if ($scope.showDebugMessage) {
                    $log.debug($info);
                }
            }
            
            $scope.getCookie = function(name) {
                var match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
                if (match) return match[2];
            }
            
            // initialise some variables and arrays and objects.
            $scope.currentWipParentID = 0;
            $scope.currentWipID = 0;

            /* CAPI */
            $scope.currentCapiJID = 0;
            $scope.currentCapiJob = {};

            $scope.wipsList = [];
            $scope.wipsForm = {};
            $scope.job = {};
            $scope.wipsStatus = {};
            $scope.wipsStages = {};
            $scope.allActivePersons = [];
            $scope.user = {};
            $scope.currentSql = '';
            $scope.updateRow = 0;
            
            $scope.isUpdated = false;
            $scope.isNew = false;
         
            function resetCurrentMessage() {
                $scope.currentMessage = {};
                $scope.currentMessage.type = '';
                $scope.currentMessage.text = '';
                $scope.currentMessage.show = false;    
            }
            resetCurrentMessage();
           
            $scope.defaultUserViewId = 0;
            $scope.resourcesDisplayLimit = 3;
         
            $scope.productionControl = {};
         
            $scope.init = function() {
                
                // at this point we should have a user session set up, so lets go
                // and get the user session data, 
                // including the default view if there is one set.
                WipsService.getUserSession().then(
                    function successCallback(response) {
                        $scope.user = response.data.user;
                        $scope.defaultUserViewId = response.data.default_view;
                         $scope.setDebugInfo($scope.user);
                    }, 
                    function errorCallback(response) {
                        $log.error('error~controller:WipsController|method:getUserSession');
                        $log.error(response);
                    }
                ); 
            }
            
            /*
            function getAllUsers() {
                    
                    AdminService.getAllUsers().then(
                        function successCallback(response) {
                            $scope.adminAllUsers = response.data.users;
                        },
                        function errorCallback(response) {
                            $log.error('error');
                            $log.error(response);
                        }
                    );
            
                    
                }
                */
                
            /*   
            $scope.adminGetCurrentUser = function(urid) {
                    
                AdminService.getUser(urid).then(
                    function successCallback(response) {
                        $scope.adminCurrentUser = response.data.user;
                    },
                    function errorCallback(response) {
                        $log.error('error');
                        $log.error(response);
                    }
                );
            };
            */
           
            $scope.locationChangeCheckViewUpdate = function(location) {
                if ($scope.viewUpdated) {
                    $scope.promptSaveMessage('setLocationPath', location);
                }
                else {
                    $scope.setLocationPath(location);
                }
            };
            
            $scope.updateWipsLinkCheckViewUpdate = function(pjid) {
                if ($scope.viewUpdated) {
                    $scope.promptSaveMessage('updateWipsLink', pjid);
                }
                else {
                    $scope.updateWipsLink(pjid);
                }
            };
           
           
            // control the site content with routing
            // set scope variables based upon URL in order to show and the correct page content    
            $scope.$on('$locationChangeStart', function(event) {
                
                $scope.activeMenu = '';
                $scope.productionControl = {};
               // resetCurrentMessage();
                
                $scope.hiliteRowKey = null;
                
                // check the session is still active, if not then chuck them out. 
                WipsService.getUserSession().then(
                    function successCallback(response) {
                        $scope.user = response.data.user;
                        // check if user session still active...
                        if (!$scope.user) {
                            
                            //.. if not then refirect home
                            $window.location.href = "/?sx=1";
                            return;
                        
                        }
                        else {
                            var page = $location.path().split("/")[1];
                            var arg1 = $location.path().split("/")[2];
                            var arg2 = $location.path().split("/")[3];
                            var arg3 = $location.path().split("/")[4];
                            // decide where to go if the user has entered the root
                            if (typeof page == 'undefined') {
                                if ($scope.user.user_type == 6) {
                                    $scope.setLocationPath('production');
                                 
                                }
                                else {
                                    $scope.switchViewPath($scope.defaultUserViewId);
                                }
                                
                            }

                            // decide where to go if the user has entered the root
                            if (typeof page == 'undefined') {
                                if ($scope.user.user_type == 6) {
                                    $scope.setLocationPath('production');
                                 
                                }
                                else {
                                    $scope.switchViewPath($scope.defaultUserViewId);
                                }
                                
                            }

                            switch(page) {
                                
                                case 'admin': 
                                    $scope.currentWipParentID = 0;
                                    $scope.currentWipID = 0;
                                    $scope.activeMenu = 'admin';
                                
                                    /*
                                    if (arg1 == 'users') {
                                        resetAdminCurrentUser();
                                        
                                        if (arg2 == 'add') {
                                            resetAdminCurrentUser();
                                            $scope.pageDisplay = 'admin-users-form';

                                        }
                                        else if (arg2 == 'edit') {
                                            $scope.pageDisplay = 'admin-users-form';
                                            $scope.adminCurrentUserID = arg3;
                                            $scope.adminGetCurrentUser($scope.adminCurrentUserID);

                                            // get the user object
                                        }
                                        else if (arg2 == 'password') {
                                            $scope.pageDisplay = 'admin-users-password';
                                            $scope.adminCurrentUserID = arg3;
                                            $scope.adminGetCurrentUser($scope.adminCurrentUserID);

                                            // get the user object
                                        }
                                        else if (arg2 == 'view') {
                                            $scope.pageDisplay = 'admin-users-view';
                                            $scope.adminCurrentUserID = arg3;
                                            $scope.adminGetCurrentUser($scope.adminCurrentUserID);
                                        }
                                        else {

                                            $scope.pageDisplay = 'admin-users-list';
                                            getAllUsers();
                                        }
                                        
                                        break;
                                        
                                    }
                                    */
                                   /*
                                    else if (arg1 == 'import') {
                                       $scope.pageDisplay = 'admin-finance-import';  
                                    }
                                    */
                                    if (arg1 == 'locked-wips') {
                                        $scope.pageDisplay = 'admin-locked-wips';  
                                    }
                                    else if (arg1 == 'people-selects') {
                                        $scope.pageDisplay = 'admin-people-selects';  
                                    }
                                    else {
                                        $scope.pageDisplay = 'admin-options'; 
                                    }
                                    break;
                                
                                /*    
                                case 'functional-tests':
                                    $scope.currentWipParentID = 0;
                                    $scope.currentWipID = 0;
                                    $scope.pageDisplay = 'function-test';
                                    break;

                                case 'color-test':
                                    $scope.currentWipParentID = 0;
                                    $scope.currentWipID = 0;
                                    $scope.pageDisplay = 'color-test';
                                    break;
                                */

                                case 'job-search':
                                    $scope.unlockWip($scope.currentWipParentID);
                                    $scope.currentWipParentID = 0;
                                    $scope.currentWipID = 0;
                                    $scope.pageTitle = "Job Search";
                                    $scope.pageDisplay = 'job-search';
                                    break;

                                case 'add-wip':
                                    $scope.unlockWip($scope.currentWipParentID);
                                    regenerateSelectLists();
                                    $scope.currentWipParentID = arg1;
                                    $scope.currentCapiJID = 0;
                                    $scope.currentWipID = 0;
                                    getJobInfo();
                                    $scope.pageTitle = "Edit/Create WIPs";
                                    $scope.pageDisplay = 'edit-create-wips';
                                    break;

                                /* CAPI */    
                                case 'add-wip-capi': 
                                    // if we have got to this point we know it is not locked
                                    regenerateSelectLists();
                                    $scope.currentWipParentID = 0;
                                    $scope.currentCapiJID = arg1;
                                    $scope.currentWipID = 0;
                                    getCapiJobInfo();
                                    $scope.pageTitle = "Edit/Create WIPs";
                                    $scope.pageDisplay = 'edit-create-wips';
                                    break;

                                case 'update-wip': 
                                    // if we have got to this point we know it is not locked
                                    $scope.lockWip(arg1);
                                    regenerateSelectLists();
                                    $scope.currentWipParentID = arg1;
                                    $scope.currentCapiJID = 0;
                                    $scope.currentWipID = 0;
                                    getJobInfo();
                                    $scope.pageTitle = "Edit/Create WIPs";
                                    $scope.pageDisplay = 'edit-create-wips';
                                    break;

                              
                                case 'view-wip': 
                                    break;
                                    
                                case 'production':
                                    $scope.pageTitle = "Production";
                                  //  $scope.pageDisplay = 'production';
                                    $scope.activeMenu = 'production';
                                    if (arg1 == 'closed') {
                                        $scope.productionControl.action = 'closed';
                                        $scope.pageDisplay = 'production-closed';
                                        
                                    }
                                    else if (arg1 == 'add') {
                                        $scope.pageDisplay = 'production-add';
                                        
                                    }
                                    else if (arg1 == 'add-record') {
                                        $scope.productionControl.action = 'add';
                                        $scope.productionControl.pjid = arg2;
                                        $scope.productionControl.capi_jid = arg3;
                                        $scope.pageDisplay = 'production-add-record';
                                        // get parent details....                                        
                                        
                                    }
                                    else if (arg1 == 'update-record') {
                                        $scope.productionControl.action = 'update';
                                        $scope.productionControl.pr_pid = arg2;
                                        $scope.pageDisplay = 'production-add-record';
                                        // get parent details....                                        
                                        
                                    }
                                    
                                    else { // default... (!arg1 || arg1 == '' || arg1 == 'active')
                                        $scope.productionControl.action = 'active';
                                         $scope.pageDisplay = 'production-active';
                                    }
                                    break;

                                default:
                                case 'my-views': 
                                    $scope.unlockWip($scope.currentWipParentID);
                                    arg1 = arg1 ? arg1 : 0;
                                    $scope.hiliteRowKey = -1;
                                    $scope.displaySector = arg1; 
                                    $scope.displayView = arg1;
                                    $scope.currentWipParentID = 0;
                                    $scope.currentWipID = 0;
                                    $scope.pageTitle = "Your Views";
                                    $scope.pageDisplay = 'wips-views-main';

                                    if (arg1 > 0) {
                                        $scope.getViewsData(arg1);
                                    }
                                    else {
                                        $scope.resetDataFilters(); 
                                        $scope.resetColOrders(); 
                                        $scope.resetShowCols(); 
                                        $scope.getWips(); 
                                        // chuck a message up.
                                    //    if ($scope.userViews.length == 0) {
                                    //        $scope.appIntroMessage();
                                    //    }

                                    }
                                    if ($scope.updateRow != 0) {
                                        $timeout(testTimer, 1000);
                                        $timeout(testTimer2, 7000);
                                    }
                                    
                                    break;
                            }
                        }
                    }, 
                    function errorCallback(response) {
                        $log.error('error~controller:WipsController|method:getUserSession');
                        $log.error(response);
                    }
                );
                
            });
        
            function testTimer() {
                var elem = '#' + $scope.updateRow;
                if (!$(elem).visible(true)) {
                    //$anchorScroll($scope.updateRow);
                   // var position = $(elem).position();
                    //scroll(0,position.top);
                    $.scrollTo(elem, 1000, {
                        onAfter: function() {
                            $('#job-selection div.updated').addClass('updatedfade');
                        }
                    });
                
                
                }
                else {
                    $('#job-selection div.updated').addClass('updatedfade');
                }
               
            }
         
            function testTimer2() {
                $scope.updateRow = 0;
            }
            
            $scope.goHome = function() {
                $scope.switchViewPath($scope.defaultUserViewId);
            }
        
        
            // set the location path
            $scope.setLocationPath = function(path) {
                $location.path("/" + path);
            }
            
            
            // jump to a new view
            $scope.switchViewPath = function(v_id) {
                
                if ($scope.viewUpdated) {
                    $scope.promptSaveMessage('switchViewPath', v_id);
                }
                else {
                    $scope.displayControlPanelClosed = true;
                    if (v_id == 0) {
                        $scope.displayControlPanelClosed = false;
                    }
                    $scope.filterTabOpen = false;
                    $location.path("/my-views/" + v_id);
                }
            }
                   
            function resetWipsFormParentJob() {
                $scope.wipsForm.clientCode = '';
                $scope.wipsForm.clientBrand = '';
                $scope.wipsForm.jobNumber = '';
                $scope.wipsForm.jobNumberDisplay = '';
                $scope.wipsForm.isParent = false;
                $scope.wipsForm.childJobs = [];
                $scope.wipsForm.showChildJobs = false;
                $scope.wipsForm.parentJobNumber = '';
                $scope.wipsForm.projectType = '';
                $scope.wipsForm.projectTitle = '';
                $scope.wipsForm.projectDescription = '';
                $scope.wipsForm.projectDigital = '';
                $scope.wipsForm.clientSector = '';
                $scope.wipsForm.leadAccountHandler = '';
                $scope.wipsForm.financeData = false;

                $scope.wipsForm.legacyData = false;
                $scope.wipsForm.legacyJobName = '';
                $scope.wipsForm.legacyJobNumberDisplay = '';
                $scope.wipsForm.currentJobNumberDisplay = '';
            }

            function resetWipsFormWipsData() {
                $scope.wipsForm.currentStatus = '';
                $scope.wipsForm.statusNotes = '';
                $scope.wipsForm.currentStage = [];
                $scope.wipsForm.resourceRequest = [];
                $scope.wipsForm.resourceRequest[0] = '';
                $scope.wipsForm.resourceRequestClient = [];
                $scope.wipsForm.resourceRequestClient[0] = 0;
                $scope.wipsForm.liveDate = '';
                $scope.wipsForm.countries = '';
                $scope.wipsForm.items = '';
                $scope.wipsForm.accountHandler1 = '';
                $scope.wipsForm.accountHandler2 = '';
                $scope.wipsForm.deliveryLead = '';
                $scope.wipsForm.leadCreative1 = ''; 
                $scope.wipsForm.leadCreative2 = '';
                $scope.wipsForm.lastUpdated = '';
                $scope.wipsForm.lastUpdatedBy = '';
                $scope.wipsForm.showChildJobsshow = false;
                $scope.wipsForm.accountHandler1_id = '0';
                $scope.wipsForm.accountHandler2_id = '0';
                $scope.wipsForm.deliveryLead1_id = '0';
                $scope.wipsForm.leadCreative1_id = '0'; 
                $scope.wipsForm.leadCreative2_id = '0';


            }
            
            $scope.back = function() {
                window.history.back();
            };
        
            resetWipsFormParentJob();
            resetWipsFormWipsData();
            
            // Get all person Information via a service
            function getAllActivePersons()  {
                WipsService.getAllActivePersons().then(
                    function successCallback(response) {
                        $scope.allActivePersons = response.data.persons;
                        $scope.setDebugInfo('success~ controller:WipsController|method:getAllActivePersons');
                        $scope.setDebugInfo($scope.allActivePersons);
                    },
                    function errorCallback(response) {
                        $log.error('error~controller:WipsController|method:getUserSession');
                        $log.error(response);
                    }
                );      
            }
            getAllActivePersons();
            
        
            // get all WIPS Status via a service..
            function getWipsStatuses() {
                $scope.wipsStatus = {};
                WipsService.getWipsStatuses().then(
                    function successCallback(response) {
                        $scope.wipsStatus = response.data.statuses;
                        $scope.setDebugInfo('success~ controller:WipsController|method:getWipsStatuses');
                        $scope.setDebugInfo($scope.wipsStatus);
                    },
                    function errorCallback(response) {
                        $log.error('error~controller:WipsController|method:getWipsStatuses');
                        $log.error(response);
                    }
                );
            }

            // get all WIPS stages via a service....
            function getWipsStages() {
                $scope.wipsStages = {};
                WipsService.getWipsStages().then(
                    function successCallback(response) {
                        $scope.wipsStages = response.data.stages;
                        $scope.setDebugInfo('success~ controller:WipsController|method:getWipsStages');
                        $scope.setDebugInfo($scope.wipsStages);
                    },
                    function errorCallback(response) {
                        $log.error('error~controller:WipsController|method:getWipsStages');
                        $log.error(response);
                    }
                );
            }
           
            // regenerate the lists used for selects on the form...
            function regenerateSelectLists() {
                getWipsStatuses();
                getWipsStages();
            }
            regenerateSelectLists(); 
        
            // get full job information via  a service based on parent job id
            function getJobFull(pjid) {
                WipsService.getJob(pjid).then(
                    function successCallback(response) {
                        $scope.job = response.data.job;
                    },
                   function errorCallback(response) {
                        $log.error('error~controller:WipsController|method:getJobFull');
                        $log.error(response);
                    }
                );
            }

            function getJobInfo() {

                resetWipsFormParentJob();
                resetWipsFormWipsData();
             
                WipsService.getJob($scope.currentWipParentID).then(
                    function successCallback(response) {
                        var job = response.data.job;
                        $scope.wipsForm.currentWipParentID = $scope.currentWipParentID;
                        $scope.wipsForm.clientCode = job.client;
                        $scope.wipsForm.clientBrand = job.brand;
                        
                        $scope.wipsForm.jobNumber = job.number;
                        $scope.wipsForm.jobNumberShorthand = job.job_number_shorthand;
                        
                        
                        $scope.wipsForm.jobNumberDisplay = job.job_number_display_inline;
                        $scope.wipsForm.isParent = job.is_parent;
                        $scope.wipsForm.childJobs = job.child_jobs;
                        
                        $scope.wipsForm.parentJobNumber = job.parent_number;
                        $scope.wipsForm.projectTypeRaw = job.type;
                        
                        $scope.wipsForm.projectType = job.job_type;
                        $scope.wipsForm.projectTitle = job.title;
                        $scope.wipsForm.projectDescription = job.description;
                        
                        $scope.wipsForm.projectDigitalRaw = job.digital;
                        $scope.wipsForm.projectDigital = job.digital_formatted;
                        $scope.wipsForm.clientSectorRaw = job.sector;
                        $scope.wipsForm.clientSector = job.job_sector;
                        $scope.wipsForm.leadAccountHandler = job.account_handler;
                        
                        $scope.wipsForm.sectorCell = job.sector_cell;
                        
                        $scope.wipsForm.financeData = job.finance_data;
                        $scope.wipsForm.jobEstimates = job.job_estimates;
                        $scope.wipsForm.projectTeam = job.project_team;

                        $scope.wipsForm.legacyData = job.legacy_data;
                        $scope.wipsForm.legacyJobName = job.job_title_leg;
                        $scope.wipsForm.legacyJobNumberDisplay = job.job_number_leg_display_inline;
                        $scope.wipsForm.currentJobNumberDisplay = job.job_number_curr_display_inline;
                        
                        if (typeof job.wips[0] != 'undefined') {

                            $scope.currentWipID = job.wips[0].id;
                            $scope.wipsForm.currentStatus = job.wips[0].status_id;
                            $scope.wipsForm.statusNotes = job.wips[0].status_notes;
                            $scope.wipsForm.currentStage = job.wips[0].stages_a;
                            
                            $scope.wipsForm.resourceRequest = job.wips[0].resource_requests;
                            $scope.wipsForm.resourceRequestClient = job.wips[0].resource_requests_client;
                            $scope.wipsForm.liveDate = job.wips[0].live_date;
                            $scope.wipsForm.countries = job.wips[0].countries;
                            
                            $scope.wipsForm.items = parseInt(job.wips[0].no_items);
                            $scope.wipsForm.lastUpdated = job.wips[0].created_on_formatted;
                            $scope.wipsForm.lastUpdatedBy = job.wips[0].created_by_name;
                            
                            $scope.wipsForm.accountHandler1 = job.wips[0].account_handler1;
                            $scope.wipsForm.accountHandler2 = job.wips[0].account_handler2;
                            $scope.wipsForm.deliveryLead = job.wips[0].delivery_lead1;
                            $scope.wipsForm.leadCreative1 = job.wips[0].lead_creative1;
                            $scope.wipsForm.leadCreative2 = job.wips[0].lead_creative2;
                            
                            $scope.wipsForm.accountHandler1_id = job.wips[0].account_handler1_id;
                            $scope.wipsForm.accountHandler2_id = job.wips[0].account_handler2_id;
                            $scope.wipsForm.deliveryLead1_id = job.wips[0].delivery_lead1_id;
                            $scope.wipsForm.leadCreative1_id = job.wips[0].lead_creative1_id; 
                            $scope.wipsForm.leadCreative2_id = job.wips[0].lead_creative2_id;
                        }
                        else {
                            // no wips so lets set some default values
                            $scope.wipsForm.currentStatus = 1;
                            
                            if (job.is_parent) {
                                $scope.wipsForm.items = job.child_jobs.length;
                            }
                            
                            if (job.default_account_handler_id != "0") {
                            $scope.wipsForm.accountHandler1_id = job.default_account_handler_id;
                                if (job.digital == 0) {
                                    $scope.wipsForm.deliveryLead1_id = job.default_account_handler_id;
                                }
                            }
                        }
                    },
                    function errorCallback(response) {
                        $log.error('error~controller:WipsController|method:getJobInfo');
                        $log.error(response);
                    }
                );
                
            }
        
            /* CAPI */
            function getCapiJobInfo() {

                let token = $scope.getCookie('cw_access_token');
                
                WipsService.getCapiJob($scope.currentCapiJID, token).then(
                    function successCallback(response) {
                       buildCapiJobInfo(response.data.data.data[0]);
                    },
                    function errorCallback(response) {
                        $log.error('error~controller:WipsJobsearchController|method:searchJobs');
                        $log.error(response);
                        
                    }

               );
            }

            /* CAPI */
            function buildCapiJobInfo(res) {

                resetWipsFormParentJob();
                resetWipsFormWipsData();

                // save job for later...
                $scope.currentCapiJob = res;

                // populate form elements...
                $scope.wipsForm.currentWipParentID = 0;
                $scope.wipsForm.clientCode = res.client_name_short;
                $scope.wipsForm.clientBrand = res.brand_name;
                
                $scope.wipsForm.jobNumber = res.numbers[0].number;
                $scope.wipsForm.jobNumberShorthand = res.numbers[0].number;
                        
                $scope.wipsForm.jobNumberDisplay = res.numbers[0].number;
                
                $scope.wipsForm.isParent = 0;
                $scope.wipsForm.childJobs = 0;
                        
                $scope.wipsForm.parentJobNumber = '';
                $scope.wipsForm.projectTypeRaw = res.location;
                        
                $scope.wipsForm.projectType = res.location_name;
                $scope.wipsForm.projectTitle = res.job_name;
                $scope.wipsForm.projectDescription = '';
                        
                $scope.wipsForm.projectDigitalRaw = res.digital;
                $scope.wipsForm.projectDigital = res.digital ? 'YES' : 'NO';
                $scope.wipsForm.clientSectorRaw = res.sector_id;
                $scope.wipsForm.clientSector = res.sector_id < 5 ? res.sector_name : res.discipline_name;
                $scope.wipsForm.leadAccountHandler = res.lead_account_handler_name;
                        
              //  $scope.wipsForm.sectorCell = job.sector_cell;
                        
                $scope.wipsForm.currentJobNumberDisplay = res.numbers[0].number;
                
            }





            $scope.toggleCurrentStage = function(key) {
                var idx = $scope.wipsForm.currentStage.indexOf(key); 
                if (idx > -1) {
                    $scope.wipsForm.currentStage.splice(idx, 1);
                }
                else {
                    $scope.wipsForm.currentStage.push(key);
                }
            };
            
            $scope.removeResourceRequest = function(key) {
                if ($scope.wipsForm.resourceRequest.length > 1) {
                    $scope.wipsForm.resourceRequest.splice(key, 1);
                    $scope.wipsForm.resourceRequestClient.splice(key, 1);
                }
                else {
                    $scope.wipsForm.resourceRequest[0] = '';
                    $scope.wipsForm.resourceRequestClient[0] = 0;
                }
            };
            
            $scope.addResourceRequest = function() {
                $scope.wipsForm.resourceRequest.push("");
                $scope.wipsForm.resourceRequestClient.push(0);
            };
        
            $scope.clearAllResourceRequests = function() {
                $scope.wipsForm.resourceRequest = [];
                $scope.wipsForm.resourceRequest[0] = "";
                $scope.wipsForm.resourceRequestClient = [];
                $scope.wipsForm.resourceRequestClient[0] = 0;
            };
        
            $scope.isWipsFormDisabled = function() {
                return false;
            };
            
            $scope.displayText = function(item) {
                return item.name;
            };
        
            $scope.submitWip = function() {
                
                $scope.updateRow = 0;
                
                if ($scope.currentWipParentID > 0)
                    $scope.unlockWip($scope.currentWipParentID);
                
                if ($scope.currentWipParentID == 0 && $scope.currentCapiJID > 0) {

                    // we must save the capi deails into parent_jobs / parent_job numbers / _pjid_capi 
                    // and then we can save the wip
                    // do it all in one big service
                    WipsService.saveCapiJobAndWip($scope.currentCapiJob, $scope.wipsForm).then(
                        function successCallback(response) {
                            $scope.message = response.data.message;
                            $scope.updateRow = String(response.data.message);
                            $scope.currentCapiJob = {};
                            $scope.currentCapiJID = 0;
                            $scope.confirmWipsSaved();
                        },
                        function errorCallback(response) {
                            $log.error('error~controller:WipsController|method:submitWip');
                            $log.error(response);
                        }

                    );    

                }
                else {

                    WipsService.saveWip($scope.currentWipParentID, $scope.wipsForm).then(
                        function successCallback(response) {
                            $scope.message = response.data.message;
                            $scope.updateRow = String(response.data.message);
                            $scope.confirmWipsSaved();
                        },
                        function errorCallback(response) {
                            $log.error('error~controller:WipsController|method:submitWip');
                            $log.error(response);
                        }
                    );
                }

             

            };
            
            
            
            
            
            
            $scope.updateWipsLink = function(pjid) {
                // do the check in here for locks, and the re-direct if not locked...
                WipsService.isWipLocked(pjid).then(
                
                   function successCallback(response) {
                        var wipLockDetails =  response.data.lockdata;
                        if (wipLockDetails == false) {
                            $scope.setLocationPath('update-wip/' + pjid);
                        }
                        else { // set message..
                            $scope.popLockMessage(wipLockDetails);
                       }
                    },
                    function errorCallback(response) {
                        $log.error('error~controller:WipsController|method:updateWipsLink');
                        $log.error(response);
                    }
                );
            }
            
            
           
            /**
             *  Filters and Settings control 
             *  (- possibly move to a new controller?)
             */
            $scope.hiddenColsCount = 0;
            $scope.showCols = {};
            $scope.exportFieldList = {} 
            
            $scope.colsLabels = {
                'client' : 'Client',
                'sector' : 'Sector',
                'jnumber' : 'Job no.',
                'number' : 'Job no.',
                'job_number_display_raw_inline' : 'Job no.',
                'jtype' : 'Job type',
                'type' : 'Job type',
                'project' : 'Project',
                'title' : 'Project',
                'digital' : 'Digital',
                'digital_formatted' : 'Digital',
                'countries' : 'Countries',
                'items' : 'No. of items',
                'status' : 'Status',
                'status_notes' : 'Status notes',
                'stages' : 'Stage(s)',
                'resources' : 'Resource requests',
                'livedate' : 'Live date',
                'live_date' : 'Live date',
                'account_handlers' : 'Account Handler(s)',
                'lead_delivery' : 'Lead Delivery',
                'creatives' : 'Lead Creative(s)', 
                'account_handlers_x' : 'Account Handler',
                'delivery_leads_x' : 'Lead Delivery',
                'lead_creatives_x' : 'Lead Creative', 
                'last_updated' : 'Last Updated',
                'created_on_raw' : 'Last Updated',
                'actions' : 'Actions',
                'total_costs' : 'Costs to date',
                'total_costs_x' : 'Costs to date',
            }
            
            $scope.exportNamesLookup = {
                'client' : 'client',
                'sector' : 'sector',
             //   'jnumber' : 'job_number_display_raw_inline2',
                'jnumber' : 'job_number_shorthand',
                'jtype' : 'type',
                'project' : 'title',
                'digital' : 'digital_formatted',
                'countries' : 'countries',
                'items' : 'no_items',
                'status' : 'status',
                'status_notes' : 'status_notes',
                'stages' : 'stages_string',
                'resources' : 'resource_requests_x',
                'livedate' : 'live_date',
                'account_handlers' : 'account_handlers_x',
                'lead_delivery' : 'delivery_leads_x',
                'creatives' : 'lead_creatives_x', 
                'last_updated' : 'created_on_formatted',
                'total_costs' : 'total_costs_x'
                
            }
            
            
            function calculateHiddenCols() {
                var hiddenCols = 0;
                $scope.exportFieldList = {} 
                Object.keys($scope.showCols).forEach(function(key) {
                    if(!$scope.showCols[key]) {
                        hiddenCols++;
                    }
                    else {
                        if (key != 'actions')
                            $scope.exportFieldList[$scope.exportNamesLookup[key]] = $scope.colsLabels[key];
                    }
                });
                $scope.hiddenColsCount = hiddenCols;
            };
            
            $scope.resetColOrders = function() {
                $scope.orderColumn = 'client';
                $scope.orderOrder = 'asc';
            }
            
            $scope.resetShowCols = function() {
                $scope.showCols = {
                    'client' : true,
                    'sector' : true,
                    'jnumber' : true,
                    'jtype' : true,
                    'project' : true,
                    'digital' : true,
                    'countries' : true,
                    'items' : true,
                    'status' : true,
                    'status_notes' : true,
                    'stages' : true,
                    'resources' : true,
                    'livedate' : true,
                    'account_handlers' : true,
                    'lead_delivery' : true,
                    'creatives' : true, 
                    'total_costs' : true,
                    'last_updated' : true,
                    'actions' : true,
                    
                };
                calculateHiddenCols();
            }
        
            $scope.dataFilters = [];

            // get sectors from db via a service
            function getWipsSectors()  {
                WipsService.getWipsSectors().then(
                    function successCallback(response) {
                        $scope.wipsSectors = response.data.sectors;
                        $scope.setDebugInfo('success~ controller:WipsController|method:getWipsSectors');
                        $scope.setDebugInfo($scope.wipsSectors);
                    },
                    function errorCallback(response) {
                        $log.error('error~controller:WipsController|method:getWipsSectors');
                        $log.error(response);
                    }
                );      
            }
            getWipsSectors();

            // get job types from db via a service
            function getWipsJobTypes()  {
                WipsService.getWipsJobTypes().then(
                    function successCallback(response) {
                        $scope.jobTypes = response.data.jobtypes;
                        $scope.setDebugInfo('success~ controller:WipsController|method:getWipsJobTypes');
                        $scope.setDebugInfo($scope.jobTypes);
                    },
                    function errorCallback(response) {
                        $log.error('error~controller:WipsController|method:getWipsJobTypes');
                        $log.error(response);
                    }
                );      
            }
            getWipsJobTypes();

            // function to get values based on key/values pairs
            $scope.getObjValueByKey = function(obj, keyFld, keyVal, searchField) {
                for(var i = 0; i < obj.length; i += 1) {
                    if (obj[i][keyFld] == keyVal)
                       return(obj[i][searchField]);
                }
            }
            
            // get active list of clients for the filter selection
            function getActiveWipsClients()  {
                WipsService.getActiveWipsClients().then(
                    function successCallback(response) {
                        $scope.wipsClients = response.data.clients;
                        $scope.setDebugInfo('success~ controller:WipsController|method:getActiveWipsClients');
                        $scope.setDebugInfo($scope.wipsClients);
                    },
                    function errorCallback(response) {
                        $log.error('error~controller:WipsController|method:getActiveWipsClients');
                        $log.error(response);
                    }
                );      
            }
            getActiveWipsClients();
            
            $scope.filterTabOpen = false;
            
            $scope.toggleFilterTabOpen = function(tab) {
                if ($scope.filterTabOpen == tab) {
                    $scope.filterTabOpen = false;
                }
                else {
                    $scope.filterTabOpen = tab;
                } 
            }
            
            $scope.closeFilterTab = function(tab) {
                if ($scope.filterTabOpen == tab) {
                    $scope.filterTabOpen = false;
                }
            }
            
            
            
            $scope.toggleShowOnlyCancelled = function() {
                $scope.viewUpdated = true;
                if ($scope.dataFilters.only_cancelled == false) {
                    $scope.dataFilters.only_cancelled = true; 
                    $scope.dataFilters.only_completed = false; 
                    $scope.dataFilters.show_cancelled = false;
                    $scope.dataFilters.show_completed = false
                    $scope.dataFilters.status = [];
                }
                else {
                   $scope.dataFilters.only_cancelled = false; 
                   
                }
                $scope.filterTabOpen = false;
                $scope.getWips();
            }
            
            $scope.toggleShowOnlyCompleted = function() {
                $scope.viewUpdated = true;
                if ($scope.dataFilters.only_completed == false) {
                    $scope.dataFilters.only_completed = true; 
                    $scope.dataFilters.only_cancelled = false; 
                    $scope.dataFilters.show_cancelled = false;
                    $scope.dataFilters.show_completed = false
                    $scope.dataFilters.status = [];
                }
                else {
                   $scope.dataFilters.only_completed = false; 
                }
                $scope.filterTabOpen = false;
                $scope.getWips();
            }
            
            $scope.toggleShowCompleted = function() {
                $scope.viewUpdated = true;
                if ($scope.dataFilters.show_completed == false) {
                    $scope.dataFilters.show_completed = true;
                    $scope.dataFilters.only_completed = false; 
                    $scope.dataFilters.only_cancelled = false; 
                }
                else {
                    $scope.dataFilters.show_completed = false;
                }
                $scope.filterTabOpen = false;
                $scope.getWips();
            }
            
            $scope.toggleShowCancelled = function() {
                $scope.viewUpdated = true;
                if ($scope.dataFilters.show_cancelled == false) {
                    $scope.dataFilters.show_cancelled = true;
                    $scope.dataFilters.only_completed = false; 
                    $scope.dataFilters.only_cancelled = false; 
                }
                else {
                    $scope.dataFilters.show_cancelled = false;
                }
                $scope.filterTabOpen = false;
                $scope.getWips();
            }
            
            $scope.toggleShowOnlyMine = function() {
                $scope.viewUpdated = true;
                if ($scope.dataFilters.only_mine == false) {
                    $scope.dataFilters.only_mine = true;
                }
                else {
                    $scope.dataFilters.only_mine = false;
                }
                $scope.filterTabOpen = false;
                $scope.getWips();
            }
            
            $scope.jobDigital = [
                {'key': '0' , 'digital': 'No'},
                {'key': '1' , 'digital': 'Yes'},
            ];
            
            /* reset the filters object */
            $scope.resetDataFilters = function() {
                
                $scope.filterTabOpen = false;
                $scope.dataFilters = {
                    'sectors' : [],
                    'clients' : [],
                    'types' : [],
                    'digital' : [],
                    'status' : [],
                    'show_completed' : false,
                    'show_cancelled' : false,
                    'only_completed' : false,
                    'only_cancelled' : false,
                    'only_mine' : false,
                    'stages' : [],
                    'resource_requests' : false,
                    'account_handlers' : [],
                    'lead_delivery' : [],
                    'lead_creatives' : [],
                }
            }
            
            $scope.resetColOrders();
            $scope.resetShowCols();
            $scope.resetDataFilters();
            
            /* function to toggle a filter value */
            $scope.toggleFilters = function(filter, key) {
                $scope.viewUpdated = true;
                var thisfilter = $scope.dataFilters[filter];
                var elemindex = thisfilter.indexOf(key);
                if (elemindex > -1) {
                    thisfilter.splice(elemindex, 1);
                }
                else {
                    
                    if (filter == 'status') {
                        $scope.dataFilters.only_completed = false;
                        $scope.dataFilters.only_cancelled = false;
                    }
                    
                    thisfilter.push(key);
                } 
                $scope.getWips();
                $scope.filterTabOpen = false;
            }

            /* function to toggle a filter 'tab' show/hide */
            $scope.toggleShowCols = function(key) {
                $scope.viewUpdated = true;
                if (!$scope.showCols[key]) {
                    $scope.showCols[key] = true;
                }
                else { 
                    $scope.showCols[key] = false;
                }
                calculateHiddenCols();
            }
            $scope.wipsList = [];
        
            /* function to get wips records based on filters and using a service */
            $scope.getWips = function() {
                // reset job list
                $scope.wipsList = [];
                // wips service to get jobs.....
                WipsService.getWips(JSON.stringify($scope.dataFilters)).then(
                    function successCallback(response) {
                        var tempWipsList =  response.data.wips;
                        $scope.currentSql = response.data.sql;
                        
                        $scope.setDebugInfo('success~ controller:WipsController|method:getWips');
                        $scope.setDebugInfo(tempWipsList);
                        
                        // apply ordering here before injecting into scope variable..
                        // need to call different function depending upon type so this is
                        // 'if' is a bit clunky
                        if ($scope.orderColumn == 'client' ||
                            $scope.orderColumn == 'type' ||
                            $scope.orderColumn == 'sector' ||
                            $scope.orderColumn == 'title' ||
                            $scope.orderColumn == 'digital_formatted' ||
                            $scope.orderColumn == 'status' ||
                            $scope.orderColumn == 'live_date' ||
                            $scope.orderColumn == 'lead_creatives_x' ||
                            $scope.orderColumn == 'delivery_leads_x' ||
                            $scope.orderColumn == 'account_handlers_x') {
                            tempWipsList.sort($scope.compareString);
                        }
                        else {
                            tempWipsList.sort($scope.compareInt);
                        }
                        $scope.wipsList = tempWipsList;
                        $scope.setDebugInfo($scope.wipsList);
                    },
                    
                    function errorCallback(response) {
                        $log.error('error~controller:WipsController|method:getWips');
                        $log.error(response);
                    }
                );
            }
            
            
            // funcition to hilite a row on the WIPS table when clicking.
            $scope.toggleHiliteRowKey = function(key) {
                if ($scope.hiliteRowKey == key) {
                    $scope.hiliteRowKey = -1;
                }
                else {
                    $scope.hiliteRowKey = key;
                }
            }
            
            // function to clear and reset the wips list
            $scope.resetWipsList = function() {
                $scope.wipsList = [];
            }

            // function to compare strings - used for ordering 
            // force all to uppercase for fair comparison
            $scope.compareString = function(a, b) {
                if (a[$scope.orderColumn].toUpperCase() < b[$scope.orderColumn].toUpperCase())
                    return ($scope.orderOrder == 'asc' ? -1 : 1);
                if (a[$scope.orderColumn].toUpperCase() > b[$scope.orderColumn].toUpperCase())
                    return ($scope.orderOrder == 'asc' ? 1 : -1);
                return 0;
            }
            
            // function to compare integers - used for ordering 
            $scope.compareInt = function(a, b) {
                if (parseInt(a[$scope.orderColumn]) < parseInt(b[$scope.orderColumn]))
                    return ($scope.orderOrder == 'asc' ? -1 : 1);
                if (parseInt(a[$scope.orderColumn]) > parseInt(b[$scope.orderColumn]))
                    return ($scope.orderOrder == 'asc' ? 1 : -1);
                return 0;
            }

            // sort the data accordingly
            $scope.sortData = function(column, ord, integer) {
                $scope.viewUpdated = true;
                if ($scope.orderColumn != column || $scope.orderOrder != ord) {
                    $scope.orderColumn = column;
                    $scope.orderOrder = ord;

                    if (integer) {
                        $scope.wipsList.sort($scope.compareInt);
                    }
                    else {
                        $scope.wipsList.sort($scope.compareString);
                    }
                }
            }
            
            // don't think this is being used??? 
            /*
            $scope.getRandom = function(min, max, prefix) {
                var num =  prefix + Math.floor(Math.random() * (max - min + 1)) + min;
                alert(num);
                return num;
            }
            */
            
            /**
             *  Views and View Tab Managedment
             *  (- possibly move to a new controller later?)
             */
            $scope.displayView = 0;
            
            $scope.loadViewForm = {};
            $scope.loadViewForm.viewId = 0;
            
            $scope.loadViewForm.saveTitle = '';
            $scope.loadViewForm.saveDescription = '';
            
            $scope.loadViewForm.fullViewsData = {};
            $scope.viewUpdated = false;
            
            $scope.userViews = [];
            $scope.displayControlPanelClosed = true; 
        
            // get user views via a service
            $scope.getUserViews = function() {
                $scope.userViews = [];
                WipsService.getUserViews().then(
                    function successCallback(response) {
                        $scope.userViews = response.data.views;
                     //   $scope.defaultUserViewId = 48; // needs to be resolved from the service...
                    },
                    function errorCallback(response) {
                        $log.error('error~controller:WipsController|method:getUserViews');
                        $log.error(response);
                    } 
                );
            }
            $scope.getUserViews();
            
          //  $scope.showmyviews = function() {
           //     $scope.switchViewPath($scope.defaultUserViewId);
           // }     
            
            // get inidividual view data via a service
            $scope.getViewsData = function(vid) {

                WipsService.getViewData(vid).then(
                    
                    function successCallback(response) {
  
                        $scope.viewUpdated = false;
                      
                        $scope.setDebugInfo('success~ controller:WipsController|method:getViewsData');
                        $scope.setDebugInfo(response.data.viewdata);
  
                        $scope.loadViewForm.fullViewsData = response.data.viewdata;
                        
                        $scope.viewTitle = response.data.viewdata.name;
                        $scope.viewDescription = response.data.viewdata.description; 
                        
                        $scope.viewPublicPrivate = $scope.loadViewForm.fullViewsData.private == 1 ? 'private' : 'public';
                        $scope.viewSetDefault = $scope.loadViewForm.fullViewsData.default;
                        
                        $scope.dataFilters = response.data.viewdata.filters; 
                        $scope.showCols = response.data.viewdata.columns; 
                        
                        // orders...
                        $scope.orderColumn = response.data.viewdata.ordercol;
                        $scope.orderOrder = response.data.viewdata.orderorder;
                        
                        calculateHiddenCols();
                        $scope.getWips();
                    },
                    function errorCallback(response) {
                        $log.error('error~controller:WipsController|method:getViewsData');
                        $log.error(response);
                    }
                );
        }    
            
        // lock a WIP (via a service)   
        $scope.lockWip = function(pjid) {
            WipsService.lockWip(pjid);
        }     
       
        // unlock a WIP (via a service)
        $scope.unlockWip = function(pjid) {
            if (pjid > 0)
                WipsService.unlockWip(pjid);
        }   
        
        // See if a WIP is locked (via a service)
        $scope.isWipLocked = function(pjid) {
            WipsService.isWipLocked(pjid).then(
                function successCallback(response) {
                    return response.data.lockdata;
                },
                function errorCallback(response) {
                    $log.error('error~controller:WipsController|method:isWipLocked');
                    $log.error(response);
                }
            );
        }
        

        /* function to call the 'view job' modal window */
        /***** NEED TO DO SOME WORK HERE TO REMOVE TIMER ****/
        $scope.viewJob = function(pjid, capi_job) {
        //    getJobFull(pjid);
            $scope.job.showChildJobsshow = false;     
        //    setTimeout(function() {
                ModalService.showModal({
                    templateUrl: "public/templates/modal/view-job.html",
                    controller: "ViewjobController",
                    inputs: {
                       // job: $scope.job
                       pjid: pjid,
                       capi_job: capi_job
                    } 
                }).then(function(modal) {
                    modal.element.modal();
                    modal.close.then(function(result) {});
                });
          //  }, 1000);
        };

        
        /* function to call the save/update view modal window */
        $scope.saveViewPop = function() {
            ModalService.showModal({
                templateUrl: "public/templates/modal/save-view-pop.html",
                controller: "WipsSaveviewController",
                inputs: {
                    displayView: $scope.displayView,
                    showCols: $scope.showCols, 
                    dataFilters: $scope.dataFilters, 
                    orderColumn: $scope.orderColumn, 
                    orderOrder: $scope.orderOrder,
                    viewTitle: $scope.viewTitle,
                    viewDescription: $scope.viewDescription,
                    viewPublicPrivate: $scope.viewPublicPrivate,
                    viewSetDefault: $scope.viewSetDefault,
                    viewSql : $scope.currentSql
                }
                }).then(function(modal) {
                    modal.element.modal();
                    modal.close.then(function(result, setdefault) {
                    if (result > 0) {
                      //  if ($scope.viewSetDefault == true) {
                      //      $scope.defaultUserViewId = result;
                      //  }
                        
                        $scope.viewUpdated = false;
                        $scope.getUserViews();
                        $scope.switchViewPath(result);
                    }
                });
            });
        }
        
        
        $scope.removeViewPop = function() {
             ModalService.showModal({
                templateUrl: "public/templates/modal/remove-view-pop.html",
                controller: "WipsRemoveviewController",
                inputs: {
                    activeViewID: $scope.displayView,
                    activeViewTitle: $scope.viewTitle,
                    activeViewDescription: $scope.viewDescription,
                }
                }).then(function(modal) {
                    modal.element.modal();
                    modal.close.then(function(result) {
                        $scope.viewUpdated = false;
                        if (result!=0) {
                        
                            if ($scope.userViews.length > 1) {
                                if ($scope.displayView != $scope.userViews[0].id) {
                                    $scope.switchViewPath($scope.userViews[0].id)
                                }
                                else {
                                    $scope.switchViewPath($scope.userViews[1].id)
                                }
                            }
                            else {
                                 $scope.switchViewPath(0)   
                            }
                            
                            $scope.getUserViews();
                        }
                });
            });
        }
        
        /* generic message pop-up */
        $scope.modalMessage = function(type, text, title) {
            ModalService.showModal({
                templateUrl: "public/templates/modal/message-box.html",
                controller: "MessageController",
                inputs: {
                    type: type,
                    text: text,
                    title: title
                }
                }).then(function(modal) {
                    modal.element.modal();
                    modal.close.then(function(result) {
                        if ($scope.displaySector === 0) {
                            $scope.setLocationPath('my-views');
                        }
                        else {
                           $scope.setLocationPath('my-views/' + $scope.displaySector);
                       }
                    });
            });
        };
        
        
        /* confirm wip saved message */
        $scope.confirmWipsSaved = function() {
            ModalService.showModal({
                templateUrl: "public/templates/modal/confirm-wips-saved.html",
                controller: "WipsConfirmSaveController",
                inputs: {}
                }).then(function(modal) {
                    modal.element.modal();
                    modal.close.then(function(result) {
                        
                        if ($scope.displaySector === 0) {
                            $scope.setLocationPath('my-views');
                        }
                        else {
                           $scope.setLocationPath('my-views/' + $scope.displaySector);
                        }
                        
                        
                    });
            });
        };
        
        
        
        
        /* intro/instruction message on first time login and future help */
        $scope.appIntroMessage = function() {
            ModalService.showModal({
                templateUrl: "public/templates/modal/app-intro-pop.html",
                controller: "WipsAppIntroController",
                inputs: {}
                }).then(function(modal) {
                    
                    modal.element.modal();
                    modal.close.then(function(result) {
                        
                    });
            });
        }
        
        /* locked wip pop-up message */
        $scope.popLockMessage = function(lockdata) {
            ModalService.showModal({
                templateUrl: "public/templates/modal/lock-pop.html",
                controller: "WipsLockMessageController",
                inputs: {
                    lockdata: lockdata,
                }
                }).then(function(modal) {
                    modal.element.modal();
                    modal.close.then(function(result) {
               });
            });
        }
        
        $scope.promptSaveMessage = function(type, arg1) {
            ModalService.showModal({
                templateUrl: "public/templates/modal/prompt-save-view-pop.html",
                controller: "WipsPromptviewController",
                inputs: {
                    displayView: $scope.displayView,
                    showCols: $scope.showCols, 
                    dataFilters: $scope.dataFilters, 
                    orderColumn: $scope.orderColumn, 
                    orderOrder: $scope.orderOrder,
                    viewSql : $scope.currentSql,
                    viewTitle: $scope.viewTitle,
                }
                }).then(function(modal) {
                    modal.element.modal();
                    modal.close.then(function(result) {
                    if (result > 0) {
                        $scope.viewUpdated = false;
                        $scope.getUserViews();
                        if (type === 'switchViewPath') {
                            $scope.switchViewPath(arg1);
                        }
                        else if (type === 'setLocationPath') {
                            $scope.setLocationPath(arg1);
                        }
                        else if (type === 'updateWipsLink') {
                            $scope.updateWipsLink(arg1);
                            
                        }
                    }
                });
            });
            
        };
        
        /*  */
        $scope.popResourceRequests = function(wip) {
            
            ModalService.showModal({
                templateUrl: "public/templates/modal/resource-requests-pop.html",
                controller: "WipsResourcePopController",
                inputs: {
                    wip: wip,
                }
                }).then(function(modal) {
                    modal.element.modal();
                    modal.close.then(function(result) {
               });
            });
            
        }
        
        /* */
        $scope.popFinanceSummary = function(wip) {
            
            ModalService.showModal({
                templateUrl: "public/templates/modal/finance-summary-pop.html",
                controller: "WipsFinancePopController",
                inputs: {
                    wip: wip,
                }
                }).then(function(modal) {
                    modal.element.modal();
                    modal.close.then(function(result) {
               });
            });
            
        }
        
        $scope.popVersionHistory = function() {
            
            ModalService.showModal({
                templateUrl: "public/templates/modal/version-history.html",
                controller: "WipsVersionPopController",
                }).then(function(modal) {
                    modal.element.modal();
                    modal.close.then(function(result) {
               });
            });
            
            
        }
        
        $scope.createPdfPop = function() {
            
            ModalService.showModal({
                templateUrl: "public/templates/modal/create-pdf-pop.html",
                controller: "WipsCreatePdfPop",
                inputs: {
                   viewTitle: $scope.viewTitle,
                   user: $scope.user,
                   colsLabels: $scope.colsLabels,
                   showCols: $scope.showCols,
                   wipsList: $scope.wipsList,
                }
                }).then(function(modal) {
                    modal.element.modal();
                    modal.close.then(function(result) {
               });
            });
        
        }

        
        $scope.generateCSV = function() {
            
            function generateFieldsAndHeaders(fieldsObject, fields, header) {
                angular.forEach(fieldsObject, function(field, key) {
                    if (!field || !key) {
                        throw new Error("error json report fields");
                    }
                    fields.push(key);
                    header.push(field);
                });
                return {
                    fields: fields,
                    header: header
                };
            }
            var fieldsAndHeader = generateFieldsAndHeaders($scope.exportFieldList, [], []);
            var fields = fieldsAndHeader.fields;
            var header = fieldsAndHeader.header;
          
            function _convertToExcel(body, header) {
                return header + "\n" + body;
            }

            function _objectToString(object) {
                var output = "";
                angular.forEach(object, function(value, key) {
                    output += key + ":" + value + " ";
                });
                return "'" + output + "'";
            }

            function generateFieldValues(list, rowItems, dataItem) {
                angular.forEach(list, function(field) {
                    var data = "",
                    fieldValue = "",
                    curItem = null;
                    if (field.indexOf(".")) {
                        field = field.split(".");
                        curItem = dataItem;
                        // deep access to obect property
                        angular.forEach(field, function(prop) {
                        if (curItem !== null && curItem !== undefined) {
                            curItem = curItem[prop];
                        }
                    });
                    data = curItem;
                    } else {
                        data = dataItem[field];
                    }
                    fieldValue = data !== null ? data : " ";
                    if (fieldValue !== undefined && angular.isObject(fieldValue)) {
                        fieldValue = _objectToString(fieldValue);
                    }
              
                    else if (fieldValue !== undefined && fieldValue.indexOf(',') > -1) {
                        fieldValue = fieldValue.replace(/,/g, ' ');
                    }
                    
                    rowItems.push(fieldValue.replace(/\n|\r/g, " "));
                });
                return rowItems;
            }

            function _bodyData() {
                var body = "";
                var fieldsAndHeader = generateFieldsAndHeaders($scope.exportFieldList, [], []);
                var fields = fieldsAndHeader.fields,
                    header = fieldsAndHeader.header;
                
                angular.forEach($scope.wipsList, function(dataItem) {
                    var rowItems = [];
                    var nestedBody = "";
                    rowItems = generateFieldValues(fields, rowItems, dataItem);
                    //Nested Json body generation start 
                   
                        body += rowItems.toString() + "\n";
                });
                return body;
            }

            var bodyData = _bodyData();
            var strData = _convertToExcel(bodyData, header);
            var blob = new Blob([strData], {
               type: "text/plain;charset=utf-8"
            });
            return saveAs(blob, [$scope.viewTitle + ".csv"]);
           
        }
        
    }
        
])
;
