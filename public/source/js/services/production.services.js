angular.module("wips.services").factory("ProdService", [
    "$http",
    "$location",
    function($http,
        $location) {

        var ProdService = {};
        //var APIEndpoint = 'https://capi.langland-development.co.uk//proxy-dev/Resource.php';
        var APIEndpoint = 'https://capi.langland-live.co.uk/proxy-dev/Resource.php';
        
        ProdService.searchJobs_____ = function(searchString) {
             


            return $http({
                method: 'POST',
                url: '/production/search_jobs',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    searchstring: searchString,
                })
            })
        }
        
        /* CAPI-fied */
        ProdService.searchJobs = function(searchString, token) {

            var search = [
                { 'jobs.job_name': {term: searchString} },
                { 'job_numbers.number': {term: searchString,or:true}},
            ];
            var filter = [{'jobs.active': '1'}, {'jobs.archive': '0'}];
        // filter = null;
            let order = [{name:'id',direction: 'asc'}];

            return $http({
                method: 'POST',
                url: APIEndpoint,
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    'graph': 'getJobs',
                    'category': 'LanglandToolsService',
                    'access_token': token,
                    'filter': filter ? JSON.stringify(filter) : null, 
                    'limit': 50,
                    'offset': 0,
                    'order': JSON.stringify(order),
                    'search': JSON.stringify(search),
                })                
            })            

        }


        /* CAPI-fied */
        ProdService.getCapiJob = function(id, token) {

            let filter = [{table:'jobs',id: id}];

            return $http({
                method: 'POST',
                url: APIEndpoint,
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    'graph': 'getJobs',
                    'category': 'LanglandToolsService',
                    'access_token': token,
                    'filter': JSON.stringify(filter), 
                    'limit': 50,
                    'offset': 0,
                    'order': null,
                    'search': null,
                })                
            })            
        }

        /* new for CAPI-fied update */
        ProdService.setParentJobsRecord = function($job) {
            return $http({
                method: 'POST',
                url: '/production/set_parent_jobs_record',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    job: $job,
                })
            })


        }


        ProdService.buildProductionSearchList = function($jobs) {
            
            return $http({
                method: 'POST',
                url: '/production/build_production_job_search_list',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    jobs: $jobs,
                })
            })


        }


        ProdService.getProductionRecord = function(pjid, pr_pid) {
            
            return $http({
                method: 'POST',
                url: '/production/get_production_record',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    pjid: pjid,
                    pr_pid: pr_pid
                })
            })
        }
        
        ProdService.getProductionStages = function() {
            
            return $http({
                method: 'POST',
                url: '/production/get_production_stages',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({})
            })
        }
        
        ProdService.getProductionTypes = function() {
            
            return $http({
                method: 'POST',
                url: '/production/get_production_types',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({})
            })
        }
        
        ProdService.getProductionPartners = function() {
            
            return $http({
                method: 'POST',
                url: '/production/get_production_partners',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({})
            })
        }
        
        ProdService.saveProductionRecord = function(pjid, pr_pid, record) {
            
            return $http({
                method: 'POST',
                url: '/production/save_production_record',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    pjid: pjid,
                    pr_pid: pr_pid,
                    record: record,
                })
            })
        }
        
        ProdService.getProductionRecords = function(action, filters) {
            
             return $http({
                method: 'POST',
                url: '/production/get_production_records',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    action: action,
                    filters: filters,
                })
            })
        }
        
        ProdService.getReportOneStats = function() {
            
            return $http({
                method: 'POST',
                url: '/production/get_report_one_stats',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({})
            })
        }
        
        ProdService.getReportTwoStats = function() {
            
            return $http({
                method: 'POST',
                url: '/production/get_report_two_stats',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({})
            })
        }
        
        ProdService.getReportThreeStats = function() {
            
            return $http({
                method: 'POST',
                url: '/production/get_report_three_stats',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({})
            })
        }
        
        
        return ProdService;
    }
])
;

