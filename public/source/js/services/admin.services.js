angular.module("wips.services").factory("AdminService", [
    "$http",
    function($http) {

        var AdminService = {};

        AdminService.getAllUsers = function(type) {
            
            return $http({
                method: 'POST',
                url: '/admin/get_all_users',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({})
                
            })
        }
        
        AdminService.getUser = function(urid) {
            
            
            return $http({
                method: 'POST',
                url: '/admin/get_user',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    urid : urid,
                    
                })
                
            })
        }
        
        AdminService.saveUser = function(submission) {
            
             return $http({
                method: 'POST',
                url: '/admin/save_user',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    urid: submission.urid,
                    full_name: submission.full_name,
                    email_address: submission.email_address,
                    job_title: submission.job_title,
                    department: submission.department,
                    location: submission.location,
                    password: submission.password,
                    user_type: submission.user_type,
                    status: submission.status,
                    perms: submission.perms
                })
                
            })
            
        }

        AdminService.generatePassword = function() {
            
            return $http({
                method: 'POST',
                url: '/admin/generate_password',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({})
            })
        } 


        AdminService.saveUserPassword = function(submission) {
            
             return $http({
                method: 'POST',
                url: '/admin/save_user_password',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    urid: submission.urid,
                    password: submission.password,
                   
                })
                
            })
        }
        
         AdminService.getLockedWips = function() {
            
            return $http({
                method: 'POST',
                url: '/admin/get_locked_wips',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({})
                
            })
        }
        
         AdminService.unlockWipsRecord = function(pjid) {
         
            return $http({
                method: 'POST',
                url: '/admin/unlock_wips_record',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    pjid: pjid
                })
            })
        }
        
        AdminService.unlockAllWipsRecords = function() {
         
            return $http({
                method: 'POST',
                url: '/admin/unlock_all_wips_records',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({})
            })
        }
        
        AdminService.getPeopleList = function() {
            
            return $http({
                method: 'POST',
                url: '/admin/get_people_list',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({})
                
            })
        }
        
        AdminService.savePeopleData = function(name, type, active) {
            
            return $http({
                method: 'POST',
                url: '/admin/save_people_data',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    name: name,
                    type: type,
                    active: active
                   
                })
                
            })
        }
        
        
        AdminService.togglePeopleActive = function(id, active) {
            
            return $http({
                method: 'POST',
                url: '/admin/toggle_people_active',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    id: id,
                    active: active
                })
                
            })
        }
        
        
        
        
        
        return AdminService;
    }
    
])
;


