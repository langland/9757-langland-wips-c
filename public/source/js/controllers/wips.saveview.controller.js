/**
 *  WipsSaveviewController
 *  
 *  Controller to the save/update view popup form and functionality 
 */
angular.module("wips.controllers").controller("WipsSaveviewController", [
    '$scope',
    '$element',
    'displayView',
    'close',
    'showCols',
    'dataFilters',
    'orderColumn',
    'orderOrder',
    'viewTitle',
    'viewDescription',
    'viewPublicPrivate',
    'viewSetDefault',
    'viewSql',
    'WipsService',
    '$log',
    function ($scope,
            $element,
            displayView,
            close,
            showCols,
            dataFilters,
            orderColumn,
            orderOrder,
            viewTitle,
            viewDescription,
            viewPublicPrivate,
            viewSetDefault,
            viewSql,
            WipsService,
            $log) {

        // set the 'local' scope variables
        $scope.displayView = displayView;
        $scope.showCols = showCols;
        $scope.dataFilters = dataFilters;
        $scope.orderColumn = orderColumn;
        $scope.orderOrder = orderOrder;
        $scope.viewTitle = displayView == 0 ? '' : viewTitle;
        $scope.viewDescription = displayView == 0 ? '' : viewDescription;
        $scope.viewSql = viewSql;

        // set the form data
        $scope.saveViewForm = [];
        $scope.saveViewForm.displayView = displayView;
        $scope.saveViewForm.viewTitle = $scope.viewTitle;
        $scope.saveViewForm.viewDescription = $scope.viewDescription;
        $scope.saveViewForm.publicPrivate = $scope.displayView == 0 ? 'private' : viewPublicPrivate; 
        $scope.saveViewForm.setDefault = $scope.displayView == 0 ? false : viewSetDefault; // default setting

        $scope.close = function (result) {
            $element.modal('hide');
            close(result, 500); // close, but give 500ms for bootstrap to animate
        };

        $scope.cancel = function (result) {
            $element.modal('hide');
            close(result, 500); // close, but give 500ms for bootstrap to animate
        };


        $scope.updateView = function () {
            WipsService.updateViewData(
                $scope.displayView,
                $scope.saveViewForm.viewTitle,
                $scope.saveViewForm.viewDescription,
                $scope.saveViewForm.publicPrivate,
                $scope.saveViewForm.setDefault,
                $scope.showCols,
                $scope.dataFilters,
                $scope.orderColumn,
                $scope.orderOrder,
                $scope.viewSql).then(
                function successCallback(response) {
                   
                    $scope.saveViewForm.viewTitle = '';
                    $scope.saveViewForm.viewDescription = '';
                    $scope.close(response.data.status);
                },
                function errorCallback(response) {
                    $log.error('error~controller:WipsSaveviewController|method:updateView');
                    $log.error(response);
                }
            );
        }

        $scope.saveView = function () {
            WipsService.saveViewData(
                $scope.saveViewForm.viewTitle,
                $scope.saveViewForm.viewDescription,
                $scope.saveViewForm.publicPrivate,
                $scope.saveViewForm.setDefault,
                $scope.showCols,
                $scope.dataFilters,
                $scope.orderColumn,
                $scope.orderOrder,
                $scope.viewSql).then(
                function successCallback(response) {
                   
                    $scope.saveViewForm.viewTitle = '';
                    $scope.saveViewForm.viewDescription = '';
                    $scope.close(response.data.status);
                },
                function errorCallback(response) {
                    $log.error('error~controller:WipsSaveviewController|method:saveView');
                    $log.error(response);
                }
            );
        }

    }
])
.controller("WipsPromptviewController", [
    '$scope',
    '$element',
    'displayView',
    'close',
    'showCols',
    'dataFilters',
    'orderColumn',
    'orderOrder',
    'viewSql',
    'viewTitle',
    'WipsService',
    '$log',
    function ($scope,
            $element,
            displayView,
            close,
            showCols,
            dataFilters,
            orderColumn,
            orderOrder,
            viewSql,
            viewTitle,
            WipsService,
            $log) {

        // set the 'local' scope variables
        $scope.displayView = displayView;
        $scope.showCols = showCols;
        $scope.dataFilters = dataFilters;
        $scope.orderColumn = orderColumn;
        $scope.orderOrder = orderOrder;
        $scope.viewSql = viewSql;
        $scope.viewTitle = viewTitle;

        $scope.close = function (result) {
            $element.modal('hide');
            close(result, 500); // close, but give 500ms for bootstrap to animate
        };

        $scope.cancel = function (result) {
            $element.modal('hide');
            close(result, 500); // close, but give 500ms for bootstrap to animate
        };

        $scope.promtUpdateView = function () {
            WipsService.promptUpdateViewData(
                $scope.displayView,
                $scope.showCols,
                $scope.dataFilters,
                $scope.orderColumn,
                $scope.orderOrder,
                $scope.viewSql).then(
                function successCallback(response) {
                    $scope.close(response.data.status);
                },
                function errorCallback(response) {
                    $log.error('error~controller:WipsSaveviewController|method:updateView');
                    $log.error(response);
                }
            );
        };

    }
])
;