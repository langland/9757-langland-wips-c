angular.module("wips.controllers").controller("ProductionController", [            
    "$scope",
    "$http",
    "$location",
    "ProdService",
    "ModalService",
    "$log",
    function (
        $scope,
        $http,
        $location,
        ProdService,
        ModalService,
        $log) {
            
            // search bits... 
            $scope.prodSearchString = "";
            /*
            
            $scope.$watch('prodSearchString', function(newValue, oldValue) {
                if (newValue !== oldValue) {
                    if (newValue == '') {   
                        //$scope.getSpread();
                        console.log('clear');
                    }
                    else if (newValue.length > 2) {
                        console.log('search production');
                       
                        //$scope.adminSearchJobBags();
                    }
                }
            });
          
            
            */
            $scope.prodJobsList = [];
 
            $scope.prodRecordsList = [];
           
            $scope.prodDisplayControlPanelClosed = true;
            
            $scope.prdColsLabels = {
                'client' : 'Client',
                'job_sector' : 'Sector',
                'number' : 'Job no.',
                'job_type' : 'Job type',
                'type_names_string': 'Production Type',
                'title' : 'Project',
                'priority_job' : 'Priority Job',
                'lead_producer_name' : 'Lead Producer',
                'support_producer_name' : 'Support Producer',
                'deadline_raw' : 'Deadline',
                'stage_name' : 'Stage',
                'creative_partner' : 'Creative Partner',
                'creative_partner_name' : 'Creative Partner',
                'creative_partner_id' : 'Creative Partner',
                'talent' : 'Talent',
                'lead_account_handler_name' : 'Lead Account Handler',
                'lead_creative_name' : 'Lead Creative',
                'created_on' : 'Last Updated'
            }
            
            $scope.prodFilters = {
                'lead_producers' : [],
                'support_producers' : [],
                'production_types' : [],
                'priority_jobs' : false,
            }
            
            $scope.getCookie = function(name) {
                var match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
                if (match) return match[2];
            }

            $scope.toggleProdDisplayControlPanelClosed = function() {
                $scope.prodDisplayControlPanelClosed == true ? $scope.prodDisplayControlPanelClosed = false : $scope.prodDisplayControlPanelClosed = true;
            }            
                        
            $scope.prodFilterTabOpen = '';
                
            $scope.resetProdFilters = function() {
                $scope.prodFilters = {
                    'lead_producers' : [],
                    'support_producers' : [],
                    'production_types' : [],
                    'priority_jobs' : false,
                }
                getProductionRecords();
            }
            
            $scope.getCookie = function(name) {
                var match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
                if (match) return match[2];
            }

            function resetCurrentData() {
                $scope.currentAction = '';
                $scope.current_pjid = 0;
                $scope.current_pr_pid = 0;
                $scope.current_pr_id = 0;
            }
            resetCurrentData();
            
            $scope.toggleProdFilterTabOpen = function(tab) {
                if ($scope.prodFilterTabOpen == tab) {
                    $scope.prodFilterTabOpen = false;
                }
                else {
                    $scope.prodFilterTabOpen = tab;
                } 
            }
            
            $scope.closeProdFilterTab = function(tab) {
                if ($scope.prodFilterTabOpen == tab) {
                    $scope.prodFilterTabOpen = false;
                }
            }
            
            /* function to toggle a filter value */
            $scope.toggleProdFilters = function(filter, key) {
            
                var thisfilter = $scope.prodFilters[filter];
                var elemindex = thisfilter.indexOf(key);
                if (elemindex > -1) {
                    thisfilter.splice(elemindex, 1);
                }
                else {
                    thisfilter.push(key);
                } 
               
                getProductionRecords();
            }
            
            $scope.$watch('productionControl.action', function() {
                 resetCurrentData();
                if ($scope.productionControl.action == 'update') {
                    $scope.currentAction = 'update';
                    $scope.current_pr_pid = $scope.productionControl.pr_pid;
                    getProductionRecord();
                    
                }
                else if ($scope.productionControl.action == 'add') {
                    $scope.currentAction = 'add';
                    $scope.current_pjid = $scope.productionControl.pjid;
                    $scope.current_capi_jid = $scope.productionControl.capi_jid;
                    if($scope.current_pjid == 0) {
                        // get the job from CAPI based on $scope.current_capi_jid
                        let token = $scope.getCookie('cw_access_token');
                
                        ProdService.getCapiJob($scope.current_capi_jid, token).then(
                            function successCallback(response) {
                               // buildCapiJobInfo(response.data.data.data[0]);
                                setParentJobsRecord(response.data.data.data[0]);
                                return;
                            },
                            function errorCallback(response) {
                                $log.error('error~controller:WipsJobsearchController|method:searchJobs');
                                $log.error(response);
                                
                            }
        
                       );

                        // call service to save in parent_jobs and _pjid_capi


                        // return parent_jobs.id into $scope.current_pjid and call getProductionRecord()



                    }
                    else {
                        getProductionRecord();
                    }
                    
                }
                else if ($scope.productionControl.action == 'active') {
                    $scope.currentAction = 'active';
                    // get all active records....
                    getProductionRecords();
                }
                else if ($scope.productionControl.action == 'closed') {
                    $scope.currentAction = 'closed';
                    // get all closed records....
                     getProductionRecords();
                }
            });


            /* new for CAPI-fied update */
            function setParentJobsRecord(job) {
                
                ProdService.setParentJobsRecord(job).then(
                    function successCallback(response) { 
                        $scope.current_pjid = response.data.pjid;
                        getProductionRecord();
                        
                    },  
                    function errorCallback(response) {
                        $log.error('error~controller:ProductionController|method:getProductionRecords');
                        $log.error(response);
                    }
                );

            }


            
            $scope.toggleProductionType = function(key) {
                var idx =  $scope.productionForm.type.indexOf(key); 
                if (idx > -1) {
                     $scope.productionForm.type.splice(idx, 1);
                }
                else {
                     $scope.productionForm.type.push(key);
                }
            };
            
            $scope.toggleFilterPriorityJobs = function() {
                if ($scope.prodFilters.priority_jobs == true) {
                    $scope.prodFilters.priority_jobs = false;
                }
                else {
                     $scope.prodFilters.priority_jobs = true;
                } 
                getProductionRecords();
            }
            
            



            function getProductionRecords() {
               
                ProdService.getProductionRecords($scope.currentAction, JSON.stringify($scope.prodFilters)).then(
                    function successCallback(response) { 
                        $scope.prodRecordsList = response.data.records;
                    },  
                    function errorCallback(response) {
                        $log.error('error~controller:ProductionController|method:getProductionRecords');
                        $log.error(response);
                    }
                );
            }
            
            function getProductionRecord() {
                resetProductionFormData();
                ProdService.getProductionRecord($scope.current_pjid, $scope.current_pr_pid).then(
                    function successCallback(response) {    
                        
                        var prodrec = response.data.prodrec;
                        $scope.productionForm.jobNumber = prodrec.jobinfo.number;
                        $scope.productionForm.job_number_display_inline = prodrec.jobinfo.job_number_display_inline;

                        $scope.productionForm.legacyData = prodrec.jobinfo.legacy_data;
                        $scope.productionForm.legacyJobName = prodrec.jobinfo.job_title_leg;
                        $scope.productionForm.legacyJobNumberDisplay = prodrec.jobinfo.job_number_leg_display_inline;
                        $scope.productionForm.currentJobNumberDisplay = prodrec.jobinfo.job_number_curr_display_inline;

                        $scope.productionForm.jobNumberShorthand = prodrec.jobinfo.job_number_shorthand;
                        $scope.productionForm.projectTeam = prodrec.jobinfo.project_team;
                        $scope.productionForm.sectorCell = prodrec.jobinfo.sector_cell;
                        
                        $scope.productionForm.parentJobNumber = prodrec.jobinfo.parent_number;
                        $scope.productionForm.projectTitle = prodrec.jobinfo.title;
                        $scope.productionForm.projectType = prodrec.jobinfo.job_type;
                        $scope.productionForm.clientCode = prodrec.jobinfo.client;
                        $scope.productionForm.clientSectorRaw = prodrec.jobinfo.job_sector_raw;
                        $scope.productionForm.clientSector = prodrec.jobinfo.job_sector;
                        $scope.productionForm.clientBrand = prodrec.jobinfo.brand;
                        $scope.productionForm.projectDigitalRaw = prodrec.jobinfo.digital;
                        $scope.productionForm.leadAccountHandler = prodrec.jobinfo.account_handler;
                        
                        $scope.productionForm.isParent = prodrec.jobinfo.is_parent;
                        $scope.productionForm.childJobs = prodrec.jobinfo.child_jobs;
                        $scope.productionForm.showChildJobsshow = false;
                        
                        $scope.productionForm.records = prodrec.records.length;
                        
                        if (prodrec.records.length > 0) {
                            $scope.productionForm.lastUpdated = prodrec.records[0].created_on_formatted;
                            $scope.productionForm.lastUpdatedBy = prodrec.records[0].created_by_name;
                            $scope.productionForm.description = prodrec.records[0].description;
                   
                            $scope.productionForm.leadProducer = prodrec.records[0].lead_producer_id;
                            $scope.productionForm.supportProducer = prodrec.records[0].support_producer_id;
                            $scope.productionForm.type = prodrec.records[0].types;
                            
                            $scope.productionForm.types_name_string = prodrec.records[0].types_names_string;
                            $scope.productionForm.stage = prodrec.records[0].stage_id;
                            $scope.productionForm.stageNotes = prodrec.records[0].status_notes;
                            
                            $scope.productionForm.deadline = prodrec.records[0].deadline;
                            $scope.productionForm.agreedBudget = parseInt(prodrec.records[0].agreed_budget) == 0 ? '' : parseInt(prodrec.records[0].agreed_budget);
                            $scope.productionForm.negotiatedCosts = parseInt(prodrec.records[0].negotiated_costs) == 0 ? '' : parseInt(prodrec.records[0].negotiated_costs);
                            
                            $scope.productionForm.producerHours = parseInt(prodrec.records[0].producer_hours) == 0 ? '' : parseInt(prodrec.records[0].producer_hours);
                           // $scope.productionForm.creativePartner = prodrec.records[0].creative_partner;
                            
                            $scope.productionForm.creativePartnerID = prodrec.records[0].creative_partner_id;
                            
                            $scope.productionForm.talent = prodrec.records[0].talent;
                            
                            $scope.productionForm.nda = prodrec.records[0].nda;
                            $scope.productionForm.usageForm = prodrec.records[0].usage_form;
                            $scope.productionForm.leadAccountHandlerID = prodrec.records[0].lead_account_handler_id;
                            
                            $scope.productionForm.leadCreativeID = prodrec.records[0].lead_creative_id;
                            
                            $scope.productionForm.fileDirectoryPath = prodrec.records[0].file_directory_path;
                            
                            $scope.productionForm.priorityJob = prodrec.records[0].priority_job;
                        }
                        else {
                            // if a new record, pre-populate lead account handler and lead creative with WIPS record data if it exists.
                            if (typeof prodrec.jobinfo.wips[0] != 'undefined') {
                                if (typeof prodrec.jobinfo.wips[0].account_handler1_id != 'undefined') {
                                    $scope.productionForm.leadAccountHandlerID = prodrec.jobinfo.wips[0].account_handler1_id;
                                }
                                if (typeof prodrec.jobinfo.wips[0].lead_creative1_id != 'undefined') {
                                    $scope.productionForm.leadCreativeID = prodrec.jobinfo.wips[0].lead_creative1_id;
                                }
                            }
                        }
                    },
                    function errorCallback(response) {
                        $log.error('error~controller:ProductionController|method:getProductionRecord');
                        $log.error(response);
                    }
                );
                
            }



            $scope.$watch('prodSearchString', function(newValue, oldValue) {
                if (newValue.length > 3 && newValue != oldValue) {
                    $scope.prodJobsList = [];
                    $scope.prodSearchJobs();
                }
            });

            
            $scope.prodSearchJobs = function() {
                // reset job list
                $scope.prodJobsList = [];

                let token = $scope.getCookie('cw_access_token');
                // wips service to get jobs.....
                ProdService.searchJobs($scope.prodSearchString, token).then(
                    function successCallback(response) {
                        $scope.buildProductionSearchList(response.data.data.data);
                       // $scope.prodJobsList = response.data.jobs;


                    },
                    function errorCallback(response) {
                        $log.error('error~controller:ProductionController|method:prodSearchJobs');
                        $log.error(response);
                    }
                );
            }


            // run through search results and get production jobs/reocrds - do this via a new service based on the old search object...
            $scope.buildProductionSearchList = function(res) {

                $scope.jobsList = [];
                if(res.length) {

                // call new service to find associated WIPS records 
                ProdService.buildProductionSearchList(res).then(
                    function successCallback(response) {
                        $scope.prodJobsList = response.data.jobs;
                    
                    },
                    function errorCallback(response) {
                        $log.error('error~controller:WipsJobsearchController|method:buildJobSearchList');
                        $log.error(response);
                        
                    }
                );
                } 


            }

            
            $scope.isProductionFormDisabled = function() {
                
                if ($scope.productionForm.leadProducer == 0 || 
                    //$scope.productionForm.agreedBudget == '' ||
                    $scope.productionForm.stage == 0 ||
                    $scope.productionForm.type.length == 0 || 
                    ($scope.productionForm.creativePartnerID == 106 && $scope.productionForm.creativePartnerOther == '')) {
                        return true;
                }
                else {
                    return false;
                }
            }
            
            $scope.prodResetJobList = function() {
                $scope.prodJobsList = [];
            }
            
        
            function getProductionTypes() {
                $scope.productionTypes = []
                ProdService.getProductionTypes().then(
                    function successCallback(response) {
                        $scope.productionTypes = response.data.types;
                    },
                    function errorCallback(response) {
                        $log.error('error~controller:ProductionController|method:getProductionTypes');
                        $log.error(response);
                    }
                );
            }
            getProductionTypes();
           
           getProductionPartners
            function getProductionPartners() {
                $scope.productionTypes = []
                ProdService.getProductionPartners().then(
                    function successCallback(response) {
                        $scope.productionPartners = response.data.partners;
                    },
                    function errorCallback(response) {
                        $log.error('error~controller:ProductionController|method:getProductionPartners');
                        $log.error(response);
                    }
                );
            }
            getProductionPartners();
           
            function getProductionStages() {
                $scope.productionStages = []
                ProdService.getProductionStages().then(
                    function successCallback(response) {
                        $scope.productionStages = response.data.stages;
                    },
                    function errorCallback(response) {
                        $log.error('error~controller:ProductionController|method:getProductionStages');
                        $log.error(response);
                    }
                );
            }
            getProductionStages();
            
            function resetProductionFormData() {
                $scope.productionForm.jobNumber = '';
                $scope.productionForm.parentJobNumber = '';
                $scope.productionForm.projectTitle = '';
         
                $scope.productionForm.projectType = '';
                $scope.productionForm.clientCode = '';
                $scope.productionForm.clientSectorRaw = '';
                
                $scope.productionForm.clientSector = '';
                $scope.productionForm.clientBrand = '';
                $scope.productionForm.projectDigitalRaw = '';
                
                $scope.productionForm.leadAccountHandler = '';
                $scope.productionForm.isParent = false;
                $scope.productionForm.childJobs = [];
                
                $scope.productionForm.showChildJobsshow = false;
                $scope.productionForm.records = 0;
                $scope.productionForm.lastUpdated = '';
                
                $scope.productionForm.lastUpdatedBy = '';
                $scope.productionForm.description = '';
                $scope.productionForm.leadProducer = '0';
                
                $scope.productionForm.supportProducer = '0';
                $scope.productionForm.type = [];
                $scope.productionForm.types_name_string = ''
                
                $scope.productionForm.stage = '0';
                $scope.productionForm.stageNotes = '';
                $scope.productionForm.deadline = '';
                
                $scope.productionForm.agreedBudget = '';
                $scope.productionForm.negotiatedCosts = '';
                $scope.productionForm.producerHours = '';
                
                $scope.productionForm.creativePartnerID = '0';
                $scope.productionForm.creativePartnerOther = '';
                $scope.productionForm.talent = '';
                $scope.productionForm.nda = '0';
                
                $scope.productionForm.usageForm = '0';
                $scope.productionForm.leadAccountHandlerID = '0';
                $scope.productionForm.leadCreativeID = '0';
                
                $scope.productionForm.fileDirectoryPath = '';
                $scope.productionForm.priorityJob = '0';

                $scope.productionForm.legacyData = false;
                $scope.productionForm.legacyJobName = '';
                $scope.productionForm.legacyJobNumberDisplay = '';
                $scope.productionForm.currentJobNumberDisplay = '';
            }
            
            $scope.productionForm = {};
            resetProductionFormData();
            
            $scope.saveProductionRecord = function() {
                ProdService.saveProductionRecord($scope.current_pjid, $scope.current_pr_pid,$scope.productionForm).then(
                    function successCallback(response) {
                        confirmProductionRecordSaved(response.data.message);
                    },
                    function errorCallback(response) {
                        $log.error('error~controller:ProductionController|method:saveProductionRecord');
                        $log.error(response);
                    }
                );
            }
            
            $scope.prodOrderColumn = 'client';
            $scope.prodOrderOrder = 'asc';
            
            // sort the data accordingly
            $scope.sortProdData = function(column, ord, integer) {
                
                if ($scope.prodOrderColumn != column || $scope.prodOrderOrder != ord) {
                    $scope.prodOrderColumn = column;
                    $scope.prodOrderOrder = ord;

                    if (integer) {
                        $scope.prodRecordsList.sort($scope.compareProdInt);
                    }
                    else {
                        $scope.prodRecordsList.sort($scope.compareProdString);
                    }
                }
            }
            
            // function to compare strings - used for ordering 
            $scope.compareProdString = function(a, b) {
                if (a[$scope.prodOrderColumn] < b[$scope.prodOrderColumn])
                    return ($scope.prodOrderOrder == 'asc' ? -1 : 1);
                if (a[$scope.prodOrderColumn] > b[$scope.prodOrderColumn])
                    return ($scope.prodOrderOrder == 'asc' ? 1 : -1);
                return 0;
            }
            
            // function to compare integers - used for ordering 
            $scope.compareProdInt = function(a, b) {
                if (parseInt(a[$scope.prodOrderColumn]) < parseInt(b[$scope.prodOrderColumn]))
                    return ($scope.prodOrderOrder == 'asc' ? -1 : 1);
                if (parseInt(a[$scope.prodOrderColumn]) > parseInt(b[$scope.prodOrderColumn]))
                    return ($scope.prodOrderOrder == 'asc' ? 1 : -1);
                return 0;
            }
            
            function confirmProductionRecordSaved(message) {
                ModalService.showModal({
                    templateUrl: "public/templates/modal/confirm-production-record-saved.html",
                    controller: "ProductionRecordConfirmSaveController",
                    inputs: {}
                }).then(function(modal) {
                    modal.element.modal();
                    modal.close.then(function(result) {
                        $scope.setLocationPath('production');
                    });
                });
            }
            
            $scope.generateProdPDF = function() {
                
                var fontsize = 7;
                var body = []
                var header = [];
                
                header.push({text: 'Client', alignment: 'center', fontSize: fontsize+1, bold: true, color: '#005487', fillColor: '#F5F5F5'});
                header.push({text: 'Sector', alignment: 'center', fontSize: fontsize+1, bold: true, color: '#005487', fillColor: '#F5F5F5'});
                header.push({text: 'Job No.', alignment: 'center', fontSize: fontsize+1, bold: true, color: '#005487', fillColor: '#F5F5F5'});
                header.push({text: 'Job Type', alignment: 'center', fontSize: fontsize+1, bold: true, color: '#005487', fillColor: '#F5F5F5'});
                header.push({text: 'Project', alignment: 'center', fontSize: fontsize+1, bold: true, color: '#005487', fillColor: '#F5F5F5'});
                header.push({text: 'Production Type', alignment: 'center', fontSize: fontsize+1, bold: true, color: '#005487', fillColor: '#F5F5F5'});
                header.push({text: 'Priority Job', alignment: 'center', fontSize: fontsize+1, bold: true, color: '#005487', fillColor: '#F5F5F5'});
                header.push({text: 'Description/Requirements', alignment: 'center', fontSize: fontsize+1, bold: true, color: '#005487', fillColor: '#F5F5F5'});
                header.push({text: 'Lead Producer', alignment: 'center', fontSize: fontsize+1, bold: true, color: '#005487', fillColor: '#F5F5F5'});
                header.push({text: 'Support Producer', alignment: 'center', fontSize: fontsize+1, bold: true, color: '#005487', fillColor: '#F5F5F5'});
                header.push({text: 'Deadline', alignment: 'center', fontSize: fontsize+1, bold: true, color: '#005487', fillColor: '#F5F5F5'});
                header.push({text: 'Stage', alignment: 'center', fontSize: fontsize+1, bold: true, color: '#005487', fillColor: '#F5F5F5'});
                header.push({text: 'Stage/Status Notes', alignment: 'center', fontSize: fontsize+1, bold: true, color: '#005487', fillColor: '#F5F5F5'});
                header.push({text: 'Agreed Budget (before markup)', alignment: 'center', fontSize: fontsize+1, bold: true, color: '#005487', fillColor: '#F5F5F5'});
                header.push({text: 'Negotiated Costs', alignment: 'center', fontSize: fontsize+1, bold: true, color: '#005487', fillColor: '#F5F5F5'});
                header.push({text: 'Producer Hours', alignment: 'center', fontSize: fontsize+1, bold: true, color: '#005487', fillColor: '#F5F5F5'});
                header.push({text: 'Creative Partner', alignment: 'center', fontSize: fontsize+1, bold: true, color: '#005487', fillColor: '#F5F5F5'});
                header.push({text: 'Talent', alignment: 'center', fontSize: fontsize+1, bold: true, color: '#005487', fillColor: '#F5F5F5'});
                header.push({text: 'NDA Signed?', alignment: 'center', fontSize: fontsize+1, bold: true, color: '#005487', fillColor: '#F5F5F5'});
                header.push({text: 'Usage form completed', alignment: 'center', fontSize: fontsize+1, bold: true, color: '#005487', fillColor: '#F5F5F5'});
                header.push({text: 'Lead Account Handler', alignment: 'center', fontSize: fontsize+1, bold: true, color: '#005487', fillColor: '#F5F5F5'});
                header.push({text: 'Lead Creative', alignment: 'center', fontSize: fontsize+1, bold: true, color: '#005487', fillColor: '#F5F5F5'});
                header.push({text: 'Last Updated', alignment: 'center', fontSize: fontsize+1, bold: true, color: '#005487', fillColor: '#F5F5F5'});
                body.push(header);
                
                // generate the table body looping through the wipsList...
                for (var key in $scope.prodRecordsList) {
                    var data = $scope.prodRecordsList[key];
                    var row = [];
                    row.push({text: data.client, fontSize: fontsize});
                    row.push({text: data.job_sector, fontSize: fontsize});
                    row.push({text: data.number, fontSize: fontsize});
                    row.push({text: data.job_type, fontSize: fontsize});
                      
                    if (data.priority_job == '1') {
                        row.push({text: data.title, fontSize: fontsize, fillColor: '#ffff99'});
                        row.push({text: data.types_names_string, fontSize: fontsize, fillColor: '#ffff99'});
                        row.push({text: 'YES', bold: true, fontSize: fontsize, fillColor: '#ffff99'});
                    }
                    else {
                        row.push({text: data.title, fontSize: fontsize});
                        row.push({text: data.types_names_string, fontSize: fontsize});
                        row.push({text: '', fontSize: fontsize});
                    }
                        
                    row.push({text: data.description, fontSize: fontsize});
                    row.push({text: data.lead_producer_name, fontSize: fontsize});
                    row.push({text: data.support_producer_name, fontSize: fontsize});
                    row.push({text: data.deadline, fontSize: fontsize});
                    
                    row.push({text: data.stage_name.toUpperCase(), bold: true, fontSize: fontsize});
                    row.push({text: data.status_notes, fontSize: fontsize});
                    row.push({text: data.agreed_budget_formatted, fontSize: fontsize});
                    row.push({text: data.negotiated_costs_formatted, fontSize: fontsize});
                    
                    row.push({text: data.producer_hours, fontSize: fontsize});
                    row.push({text: data.creative_partner_name, fontSize: fontsize});
                    row.push({text: data.talent, fontSize: fontsize});
                    row.push({text: data.nda == '1' ? 'YES' : '', fontSize: fontsize});
                    
                    row.push({text: data.usage_form == '1' ? 'YES' : '', fontSize: fontsize});
                    row.push({text: data.lead_account_handler_name, fontSize: fontsize});
                    row.push({text: data.lead_creative_name, fontSize: fontsize});
                    row.push({text: data.created_on_formatted + ' by ' + data.created_by_name, fontSize: fontsize});
                    
                    body.push(row);
                }
                
                var content  = [];
                content.push({text: 'WIPS System - Active Production Records', bold: true, color: '#005487', fontSize: fontsize+6});
                content.push({text: $scope.prodRecordsList.length + ' records in total', fontSize: fontsize+2, margin: [0, 6]});
                
                //content.push({text: 'TESTING', bold: true, color: '#005487', fontSize: fontsize+4});
                content.push({
                    table: {
                        headerRows: 1,
                        body: body
                    },
                    layout: {
                        fillColor: function (i, node) {
                            return (i % 2 === 0) ? '#ffffff' : null;
                        }
                    }
                });
          
                var docDefinition = {
                    footer: function (currentPage, pageCount) {
                        var footerText =  'Page ' + currentPage + ' of ' + pageCount;
                        var newDate = new Date();
                    
                        var dateString = "";

                        dateString += zeroPad(newDate.getDate(), 2) + ".";
                        dateString += zeroPad((newDate.getMonth() + 1), 2) + ".";
                        dateString += newDate.getFullYear();
                        dateString += ' ';
                        dateString += zeroPad(newDate.getHours(), 2) + ':';
                        dateString += zeroPad(newDate.getMinutes(), 2);
                    
                        footerText += '\n\nGenerated by ' + $scope.user.full_name + ' on ' + dateString;
                        return {text: footerText, fontSize: fontsize + 1, alignment: 'center', margin: [0, 5]};

                    },
                    content: content,
                    pageSize: 'A3',
                    pageOrientation: 'landscape'
                };
                pdfMake.createPdf(docDefinition).open();
            }
            
            
            $scope.viewProd = function(pjid, pr_pid) {
                
                ModalService.showModal({
                    templateUrl: "public/templates/production/modal/view-production-record.html",
                    controller: "ViewProductionRecordController",
                    inputs: {
                       pjid: pjid, 
                       pr_pid: pr_pid
                    } 
                }).then(function(modal) {
                    modal.element.modal();
                    modal.close.then(function(result) {});
                });
            };
            
            $scope.popProductionCellLong = function(prod, type) {
                ModalService.showModal({
                    templateUrl: "public/templates/production/modal/cell-long-pop.html",
                    controller: "ProductionCellLongPopController",
                    inputs: {
                        prod: prod,
                        type: type,
                    }
                }).then(function(modal) {
                    modal.element.modal();
                    modal.close.then(function(result) {});
                }); 
            }
            
            $scope.report1 = function() {
       
                ModalService.showModal({
                    templateUrl: "public/templates/production/modal/report-1.html",
                    controller: "Report1Controller",
                    inputs: {
                    } 
                }).then(function(modal) {
                    modal.element.modal();
                    modal.close.then(function(result) {});
                });
            };
            
            $scope.report2 = function() {
       
                ModalService.showModal({
                    templateUrl: "public/templates/production/modal/report-2.html",
                    controller: "Report2Controller",
                    inputs: {
                    } 
                }).then(function(modal) {
                    modal.element.modal();
                    modal.close.then(function(result) {});
                });
            };
            
            $scope.report3 = function() {
       
                ModalService.showModal({
                    templateUrl: "public/templates/production/modal/report-3.html",
                    controller: "Report3Controller",
                    inputs: {
                    } 
                }).then(function(modal) {
                    modal.element.modal();
                    modal.close.then(function(result) {});
                });
            };
        }
])

.controller('ProductionRecordConfirmSaveController', [
    '$scope',
    '$element',
    'close',
    function ($scope,
        $element,
        close) {

        $scope.close = function (result) {
             $element.modal('hide');
            close(result, 500); // close, but give 500ms for bootstrap to animate
        };

        $scope.cancel = function (result) {
            $element.modal('hide');
            close(result, 500); // close, but give 500ms for bootstrap to animate
        };
    }
])
.controller("Report1Controller", [
    '$scope',
    '$element',
    'close',
    'ProdService',
    function ($scope,
        $element,
        close,
        ProdService) {
        
        ProdService.getReportOneStats().then(
            function successCallback(response) {
                $scope.stats = response.data.stats;
            },
            function errorCallback(response) {
                $log.error('error~controller:ReportOneController|method:getReportOneStats');
                $log.error(response);
            }
        );        

        $scope.close = function (result) {
           
            $element.modal('hide');
            close(result, 500); // close, but give 500ms for bootstrap to animate
        };

        $scope.cancel = function (result) {
            $element.modal('hide');
            close(result, 500); // close, but give 500ms for bootstrap to animate
        };
    }
])
.controller("Report2Controller", [
    '$scope',
    '$element',
    'close',
    'ProdService',
    function ($scope,
        $element,
        close,
        ProdService) {
                
        $scope.stats = {}        
        
        ProdService.getReportTwoStats().then(
            function successCallback(response) {
                $scope.stats = response.data.stats;
            },
            function errorCallback(response) {
                $log.error('error~controller:ReportOneController|method:getReportOneStats');
                $log.error(response);
            }
        );        
        $scope.close = function (result) {
           
            $element.modal('hide');
            close(result, 500); // close, but give 500ms for bootstrap to animate
        };

        $scope.cancel = function (result) {
            $element.modal('hide');
            close(result, 500); // close, but give 500ms for bootstrap to animate
        };
    }
])
.controller("Report3Controller", [
    '$scope',
    '$element',
    'close',
    'ProdService',
    function ($scope,
        $element,
        close,
        ProdService) {
                
        $scope.stats = {}      
        
        ProdService.getReportThreeStats().then(
            function successCallback(response) {
                $scope.stats = response.data.stats;
            },
            function errorCallback(response) {
                $log.error('error~controller:ReportOneController|method:getReportOneStats');
                $log.error(response);
            }
        );        

        $scope.close = function (result) {
           
            $element.modal('hide');
            close(result, 500); // close, but give 500ms for bootstrap to animate
        };

        $scope.cancel = function (result) {
            $element.modal('hide');
            close(result, 500); // close, but give 500ms for bootstrap to animate
        };

    }
])

.controller("ViewProductionRecordController", [
    '$scope',
    '$element',
    'close',
    'ProdService',
    'pjid',
    'pr_pid',
    function ($scope,
        $element,
        close,
        ProdService,
        pjid,
        pr_pid) {
                
        $scope.prod = {}        
                
        ProdService.getProductionRecord(pjid, pr_pid).then(
            function successCallback(response) {
                $scope.prodRec = response.data.prodrec;
                $scope.prodRec.showChildJobsshow = false;    
            },
            function errorCallback(response) {
                $log.error('error~controller:ViewjobController|method:getJobFull');
                $log.error(response);
            }
        );        

        // $scope.job = job;
        $scope.prodVersionHistoryShow = false;

        $scope.toggleProdVersionHistoryShow = function () {
            $scope.prodVersionHistoryShow = ($scope.prodVersionHistoryShow == true) ? false : true;
        }

        $scope.close = function (result) {
           
            $element.modal('hide');
            close(result, 500); // close, but give 500ms for bootstrap to animate
        };

        $scope.cancel = function (result) {
            $element.modal('hide');
            close(result, 500); // close, but give 500ms for bootstrap to animate
        };

    }
])

.controller("ProductionCellLongPopController", [
    '$scope',
    '$element',
    'prod',
    'type',
    function ($scope,
        $element,
        prod,
        type) {
           
        $scope.prod = prod;
        $scope.type = type;
            
        $scope.close = function (result) {
            $element.modal('hide');
            close(result, 500); // close, but give 500ms for bootstrap to animate
        };  
    }
])

;