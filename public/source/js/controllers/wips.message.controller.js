/**
 *  MessageController - generic
 *  WipsLockMessageController - locked WIPS notification
 *  WipsAppIntroController - into/intructions message
 *  CloseModal  
 *   
 *  Controllers that handle the various pop ups and modal messages 
 */
angular.module("wips.controllers").controller('MessageController', [
    '$scope',
    '$element',
    'type',
    'text',
    'title',
    'close',
    function ($scope,
        $element,
        type,
        text,
        title,
        close) {

        $scope.type = type;
        $scope.text = text;
        $scope.title = title;
 
        $scope.close = function (result) {
             $element.modal('hide');
            close(result, 500); // close, but give 500ms for bootstrap to animate
        };

        $scope.cancel = function () {
            $element.modal('hide');
            close(result, 500); // close, but give 500ms for bootstrap to animate
        };
    }
])
.controller('WipsConfirmSaveController', [
    '$scope',
    '$element',
    'close',
    function ($scope,
        $element,
        close) {

        $scope.close = function (result) {
             $element.modal('hide');
            close(result, 500); // close, but give 500ms for bootstrap to animate
        };

        $scope.cancel = function () {
            $element.modal('hide');
            close(result, 500); // close, but give 500ms for bootstrap to animate
        };
    }
])
.controller("WipsRemoveviewController", [
    '$scope',
    '$element',
    'close',
    'activeViewID', 
    'activeViewTitle',
    'activeViewDescription', 
    'WipsService',
    '$log',
    function ($scope,
        $element,
        close,
        activeViewID, 
        activeViewTitle,
        activeViewDescription,
        WipsService,
        $log) {
         
        $scope.viewID = activeViewID;
        $scope.viewTitle = activeViewTitle;
        $scope.viewDescription = activeViewDescription;
       
        $scope.removeView = function() {
            
            WipsService.removeView(
                $scope.viewID).then(
                function successCallback(response) {
                    $scope.close('success');
                },
                function errorCallback(response) {
                    $log.error('error~controller:WipsRemoveviewController|method:removeView');
                    $log.error(response);
                }
            );
            
        }
        
        $scope.close = function (result) {
           
            $element.modal('hide');
            close(result, 500); // close, but give 500ms for bootstrap to animate
        };     
    }
])
.controller("WipsResourcePopController", [
    '$scope',
    '$element',
    'wip',
    function ($scope,
        $element,
        wip) {
           
        $scope.wip = wip;
            
        $scope.close = function (result) {
            $element.modal('hide');
            close(result, 500); // close, but give 500ms for bootstrap to animate
        };  
    }
])
.controller("WipsLockMessageController", [
    '$scope',
    '$element',
    'lockdata',
    function ($scope,
        $element,
        lockdata) {
           
        $scope.lockdata = lockdata;
            
        $scope.close = function (result) {
            $element.modal('hide');
            close(result, 500); // close, but give 500ms for bootstrap to animate
        };  
    }
])
.controller("WipsAppIntroController", [
    '$scope',
    '$element',
    function ($scope,
        $element) {
        
        $scope.close = function (result) {
            $element.modal('hide');
            close(result, 500); // close, but give 500ms for bootstrap to animate
        };
    }
])
.controller('CloseModal', [
    '$scope', 
    'close', 
    function($scope, 
        close) {

        $scope.close = function(result) {
            
              close(result, 500); // close, but give 500ms for bootstrap to animate
        };
    }
])
.controller("WipsFinancePopController", [
    '$scope',
    '$element',
    'wip',
    function ($scope,
        $element,
        wip) {
           
        $scope.wip = wip;
            
        $scope.close = function (result) {
            $element.modal('hide');
            close(result, 500); // close, but give 500ms for bootstrap to animate
        };  
    }
])
.controller("WipsVersionPopController", [
    '$scope',
    '$element',
    function ($scope,
        $element) {
            
        $scope.close = function (result) {
            $element.modal('hide');
            close(result, 500); // close, but give 500ms for bootstrap to animate
        };  
    }
])
;