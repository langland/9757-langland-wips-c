/**
 *  ViewTabsController
 * 
 *  Handle user view tabs etc...
 */
angular.module("wips.controllers").controller("ViewTabsController", [
    '$scope',
    '$element',
    'WipsService',
    '$log',
    function ($scope,
        $element,
        WipsService,
        $log) {

            $scope.userViews = [];
            $scope.displayControlPanelClosed = false;
        
            $scope.getUserViews = function() {
                
                $scope.userViews = [];
                 
                WipsService.getUserViews().then(
                        
                    function successCallback(response) {
                        $scope.userViews = response.data.views;
                        $scope.defaultUserViewId = 48; // needs to be resolved from the service...
                    },
                    function errorCallback(response) {
                        $log.error('error~controller:ViewTabsController|method:getUserViews');
                        $log.error(response);
                    } 
                );
            }
            $scope.getUserViews();
            
            $scope.showmyviews = function() {
                $scope.switchViewPath($scope.defaultUserViewId);
            }    
            
    }
])


