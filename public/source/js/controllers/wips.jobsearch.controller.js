/**
 *  WipsJobsearchController
 *  
 *  Controller to handle job search 
 */
angular.module("wips.controllers").controller("WipsJobsearchController", [            
    "$scope",
    "$http",
    "$location",
    "WipsService",
    "$log",
    function (
        $scope,
        $http,
        $location,
        WipsService,
        $log) {
            


          
            
            $scope.searchString = "";
            $scope.jobsList = [];
            $scope.jobsCount = 0;

            $scope.getCookie = function(name) {
                var match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
                if (match) return match[2];
            }


         
            $scope.$watch('searchString', function(newValue, oldValue) {
                if (newValue.length > 3 && newValue != oldValue) {
                    $scope.jobsList = [];
                    $scope.jobsCount = 0;
                    $scope.searchJobs();
                }
            });



            /* CAPI-fied */
            $scope.searchJobs = function() {

                let token = $scope.getCookie('cw_access_token');
                // reset job list
                $scope.jobsList = [];
                // wips service to get jobs.....
                WipsService.searchJobs($scope.searchString, token).then(
                    function successCallback(response) {
                        $scope.buildJobSearchList(response.data.data.data);
                        $scope.jobsCount = response.data.data.total;
                    },
                    
                    function errorCallback(response) {
                        $log.error('error~controller:WipsJobsearchController|method:searchJobs');
                        $log.error(response);
                        
                    }
                );
            }






            $scope.buildJobSearchList = function(res) {

               $scope.jobsList = [];
                if(res.length) {

                    // call new service to find associated WIPS records 
                    WipsService.buildJobSearchList(res).then(
                        function successCallback(response) {
                            $scope.jobsList = response.data.jobs;
                           
                        },
                        function errorCallback(response) {
                            $log.error('error~controller:WipsJobsearchController|method:buildJobSearchList');
                            $log.error(response);
                            
                        }
                    );
                } 
            }
            
            $scope.resetJobList = function() {
                $scope.jobsList = [];
            }
           
        }
])
;