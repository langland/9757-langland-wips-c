/** 
 *  WipsTesting
 *  
 *  Controller for testing various bits n bobs. 
 *  Will remove on production version.
 *   
 */
angular.module("wips.controllers").controller('WipsTesting', [
    '$scope',
    "WipsService",
    "$log",
    function ($scope,
        WipsService,
        $log) {

        $scope.TestData = {};
        $scope.testid = 0;
        
        $scope.test_string = '';
        $scope.ret_test_string = ''
        $scope.ret_string_encrypted = '';
        $scope.ret_string_encrypted_decrypt = '';
        
        $scope.color_corporate = "abe7ab";
        $scope.color_consumer = "f5c1c1";
        $scope.color_ctr = "dfbae8";
        $scope.color_ctr_uk = "dfbae8";
        $scope.color_ctr_us = "f5f4c1";
        $scope.color_ethical = "acdbf7";
        $scope.color_other = "e3e3e3";
        

        $scope.encryptTest = function() {
            
            WipsService.testEncryptString($scope.test_string).then(
                function successCallback(response) {
                    
                    $scope.ret_test_string = response.data.string;
                    $scope.ret_string_encrypted = response.data.encrypted;
                    $scope.ret_string_encrypted_decrypt = response.data.encrypted_decrypt;
                    
                },
                function errorCallback(response) {
                    $log.error('error~controller:WipsTesting|method:saveTestData');
                    $log.error(response);
                }
            );
            
            
        }



        $scope.saveTestData = function () {
            var title = 'Test Name ';

            WipsService.saveTestData(title, $scope.showCols, $scope.dataFilters, $scope.orderColumn, $scope.orderOrder).then(
                function successCallback(response) {
                    alert(response.data.status);
                },
                function errorCallback(response) {
                    $log.error('error~controller:WipsTesting|method:saveTestData');
                    $log.error(response);
                }
            );
        }

        $scope.getTestData = function () {

            WipsService.getTestData($scope.testid).then(
                function successCallback(response) {
                    $scope.TestData = response.data.testdata;
                    $scope.dataFilters = response.data.testdata.filters;
                    $scope.showCols = response.data.testdata.columns;
                },
                function errorCallback(response) {
                    $log.error('error~controller:WipsTesting|method:getTestData');
                    $log.error(response);
                }
            );
        }
    }
])
;