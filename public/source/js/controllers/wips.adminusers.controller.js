angular.module("wips.controllers").controller("AdminUsers", [
    "$scope",
    "AdminService",
    "ModalService",
    "$location",
    "$log",
   "$window",
    "$route",
    function ($scope,
            AdminService,
            ModalService,
            $location,
            $log,
            $window,
            $route) {
                
                
           
                
            $scope.generatePassword = function() {
                
                AdminService.generatePassword().then(
                    function successCallback(response) {
                        $scope.adminCurrentUser.password = response.data.password;
                    },
                    function errorCallback(response) {
                        console.log('error');
                        console.log(response);
                    }
                );
            }
            
            $scope.submitUserForm = function() {
                // first check that email address does not already exist. this is the uniue identifier.
              
                AdminService.saveUser($scope.adminCurrentUser).then(
                    function successCallback(response) {
                        // redirect to user list...
                        // with a message....
                        $scope.currentMessage.type = 'notice';
                        $scope.currentMessage.text = 'User Account successfully saved';
                        $scope.currentMessage.show = true;
                        $location.path("admin/users");
                        
//                        $scope.setLocationPath('admin/users');
                        
                    },
                    function errorCallback(response) {
                        console.log('error');
                        console.log(response);
                    }
                );
                    
            }
            
            $scope.submitUserPasswordForm = function() {
                // first check that email address does not already exist. this is the uniue identifier.
              
                AdminService.saveUserPassword($scope.adminCurrentUser).then(
                    function successCallback(response) {
                        // redirect to user list...
                        // with a message....
                        $scope.currentMessage.type = 'notice';
                        $scope.currentMessage.text = 'User Password successfully changed';
                        $scope.currentMessage.show = true;
                         $location.path("admin/users");
                        
                    },
                    function errorCallback(response) {
                        console.log('error');
                        console.log(response);
                    }
                );
                    
            }
                
                
            $scope.deleteUser = function(urid) {
                 AdminService.deleteUser(urid).then(
                    function successCallback(response) {
                        $window.location.reload();
                        $scope.currentMessage.type = response.data.message.type;
                        $scope.currentMessage.text = response.data.message.text;
                        $scope.currentMessage.show = true;
                    },
                    function errorCallback(response) {
                        console.log('error');
                        console.log(response);
                    }
                );
                
            }
            
            $scope.confirmDeleteUser = function(urid) {
                    ModalService.showModal({
                        templateUrl: "public/templates/modal/confirm-delete-user.html",
                        controller: "CloseModal"
                    }).then(function (modal) {
                        modal.element.modal();
                        modal.close.then(function (result) {
                            if (result) {
                                $scope.deleteUser(urid);
                            }
                        });
                    });
                };
                
                
                
           
    
                
                
                
    }
])

.controller("AdminLockedWips", [
    "$scope",
    "AdminService",
    "ModalService",
    "$location",
    "$log",
   "$window",
    "$route",
    function ($scope,
            AdminService,
            ModalService,
            $location,
            $log,
            $window,
            $route) {
    
        $scope.lockedWipsList = [];
        
        $scope.getLockedWips = function() {
            
            AdminService.getLockedWips().then(
                    function successCallback(response) {
                        $scope.lockedWipsList = response.data.wips;
                    },
                    function errorCallback(response) {
                        console.log('error');
                        console.log(response);
                    }
                );
            
        }
        
        $scope.lockedWipsList = $scope.getLockedWips();
        

        $scope.unlockWipsRecord = function(pjid) {
            
            AdminService.unlockWipsRecord(pjid).then(
                    function successCallback(response) {
                        $scope.lockedWipsList = $scope.getLockedWips();
                    },
                    function errorCallback(response) {
                        console.log('error');
                        console.log(response);
                    }
                );
           
        }

        $scope.unlockAllWipsRecords = function() {
             AdminService.unlockAllWipsRecords().then(
                    function successCallback(response) {
                        $scope.lockedWipsList = $scope.getLockedWips();
                    },
                    function errorCallback(response) {
                        console.log('error');
                        console.log(response);
                    }
                );
        }



    }
])
.controller("AdminPeopleSelects", [
    "$scope",
    "AdminService",
    "ModalService",
    "$location",
    "$log",
    "$window",
    "$route",
    function ($scope,
            AdminService,
            ModalService,
            $location,
            $log,
            $window,
            $route) {
    

        $scope.peopleList = [];
        $scope.peopleForm = {};
        $scope.peopleFormMessage = '';
         
       
        function getPeopleList() {
            
            AdminService.getPeopleList().then(
                    function successCallback(response) {
                        $scope.peopleList = response.data.people;
                    },
                    function errorCallback(response) {
                        console.log('error');
                        console.log(response);
                    }
                );
            
        }
        
        getPeopleList();
        
        function resetPeopleForm() {
            $scope.peopleForm = {
                name: '',
                type: '1',
                active: 1
            };
        }
        
        resetPeopleForm();
        
        $scope.submitPeopleForm = function() {
            AdminService.savePeopleData(
                $scope.peopleForm.name,
                $scope.peopleForm.type,
                $scope.peopleForm.active).then(
                function successCallback(response) {
                    $scope.peopleFormMessage = $scope.peopleForm.name + ' has been added.'
                    resetPeopleForm();
                    getPeopleList();
                },
                function errorCallback(response) {
                    $log.error('error~controller:WipsSaveviewController|method:saveView');
                    $log.error(response);
                }
            );
            
        }
        
        $scope.togglePeopleActive = function(id, active) {
            
            AdminService.togglePeopleActive(id, active).then(
                function successCallback(response) {
                    $scope.peopleFormMessage = '';
                    resetPeopleForm();
                    getPeopleList();
                },
                function errorCallback(response) {
                    $log.error('error~controller:WipsSaveviewController|method:saveView');
                    $log.error(response);
                }
            ); 
            
             
             
             
        }
        
    
    
    
    }
])
;