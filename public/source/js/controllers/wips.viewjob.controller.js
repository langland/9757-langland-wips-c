/**
 *  ViewjobController
 * 
 *  Controller to the handle the view job popup and related functionality
 */
angular.module("wips.controllers").controller("ViewjobController", [
    '$scope',
    '$element',
   // 'job',
    'close',
    'WipsService',
    'pjid',
    'capi_job',
    function ($scope,
            $element,
           // job,
            close,
            WipsService,
            pjid,
            capi_job) {
                
        $scope.job = {}        
       
        
        $scope.getCookie = function(name) {
            var match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
            if (match) return match[2];
        }

        


        if (capi_job) {

            let token = $scope.getCookie('cw_access_token');
            WipsService.getCapiJob(pjid, token).then(
                function successCallback(response) {
                    let job = response.data.data.data[0];

                    job.job_number_shorthand = job.numbers[0].number;
                    job.job_number_curr_display_inline = job.numbers[0].number + (job.numbers[1] ? ' / ' + job.numbers[1].number : '');

                    job.title = job.job_name;
                    job.client = job.client_name;
                    job.job_type = job.location_name;
                    job.brand = job.brand_name;
                    job.job_sector = job.discipline_name ? job.discipline_name : job.sector_name;
                    job.account_handler = job.lead_account_handler_name;
                    job.wips_count = 0;
                    $scope.job = job;
                    $scope.job.showChildJobsshow = false;   
                },
                function errorCallback(response) {
                    $log.error('error~controller:ViewjobController|method:getJobFull');
                    $log.error(response);
                }
            );        


        }   
        else {      

            WipsService.getJob(pjid).then(
                function successCallback(response) {
                    $scope.job = response.data.job;
                    $scope.job.showChildJobsshow = false;    
                },
                function errorCallback(response) {
                    $log.error('error~controller:ViewjobController|method:getJobFull');
                    $log.error(response);
                }
            );        
        }

       

        // $scope.job = job;
        $scope.versionHistoryShow = false;

        $scope.toggleVersionHistoryShow = function () {
            $scope.versionHistoryShow = ($scope.versionHistoryShow == true) ? false : true;
        }

        $scope.close = function (result) {
           
            $element.modal('hide');
            close(result, 500); // close, but give 500ms for bootstrap to animate
        };

        $scope.cancel = function (result) {
            $element.modal('hide');
            close(result, 500); // close, but give 500ms for bootstrap to animate
        };

    }
])
;
