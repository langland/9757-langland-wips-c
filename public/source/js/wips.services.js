angular.module("wips.services", [])

.factory("WipsService", [
    "$http",
    "$location",
    function($http,
        $location) {

            

        var WipsService = {};
       // var APIEndpoint = 'https://capi.langland-development.co.uk//proxy-dev/Resource.php';
        var APIEndpoint = 'https://capi.langland-live.co.uk/proxy-dev/Resource.php';
        
        WipsService.getJob = function(jid) {
            
            return $http({
                method: 'POST',
                url: '/service/get_job',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    jid: jid,
                })
            })
            
        }
        
        WipsService.saveWip = function(jid, wip) {
            
            return $http({
                method: 'POST',
                url: '/service/save_wip',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    jid: jid,
                    wip: wip,
                })
            })
            
        }

        /* ADDED FOR CAPI */
        WipsService.saveCapiJobAndWip = function(capi_job, wip) {

            return $http({
                method: 'POST',
                url: '/service/save_capi_job_and_wip',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    capi_job: capi_job,
                    wip: wip,
                })
            })





        }




        
        /* CAPI-fied */
        WipsService.searchJobs = function(searchString, token) {
             
            /*
            return $http({
                method: 'POST',
                url: '/service/search_jobs',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    searchstring: searchString,
                })
            })
            */

            // hard coded token for now but this will be a session var later on when loging integrated
            //let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpZCI6ImY3MGVmOWU4ZjE2Mzg0NTUyNzk0MjQwN2IwNDE0ZGY1NDljMTE5OGMiLCJqdGkiOiJmNzBlZjllOGYxNjM4NDU1Mjc5NDI0MDdiMDQxNGRmNTQ5YzExOThjIiwiaXNzIjoiIiwiYXVkIjoiZ29DYXBpQ2xpZW50Iiwic3ViIjoicGF1bC5zaWx2ZXN0ZXJAbGFuZ2xhbmQuY28udWsiLCJleHAiOjE2MDgxMzg2MDIsImlhdCI6MTYwODEwNjIwMiwidG9rZW5fdHlwZSI6ImJlYXJlciIsInNjb3BlIjoicm9vdCB1c2UgamIifQ.HwgncdFl2n8qxqUlF7JZU0iq29b1jN5TuO_wgMb_IL2Xnm4BOxxyJN2mi1VhfUEKgk9PRrB6OS0v6w9PUF1XLmmUsJZTmlE4dAXzIObm4VGR6j4LaWh5qelBE7mLKQQoNiAxToL-wnzXvsgUygQNicHDEdwfmJpLKmsHZNzLyyiRfgPrPmUU4C04znDn-mYFpvTbHuEKsjs6sVYxaJbiniqWz8sMlQUODOdR0wp2_1zIlSQJJ5AymTtyE6gEQ8aUMu1q-ev3GjTCLrxZrV48PG_nQEK2zScHm-Ds9MHfRq1C1eK-o_cM51iFKqwHGfiJnoWdQIzqvSWex8vpLBVY1Q";
              
            var search = [
                { 'jobs.job_name': {term: searchString} },
                { 'job_numbers.number': {term: searchString,or:true}},
            ];
            var filter = [{'jobs.active': '1'}, {'jobs.archive': '0'}];
           // filter = null;
            let order = [{name:'id',direction: 'asc'}];

            return $http({
                method: 'POST',
                url: APIEndpoint,
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    'graph': 'getJobs',
                    'category': 'LanglandToolsService',
                    'access_token': token,
                    'filter': filter ? JSON.stringify(filter) : null, 
                    'limit': 50,
                    'offset': 0,
                    'order': JSON.stringify(order),
                    'search': JSON.stringify(search),
                })                
            })            
        }

        /* new service to integrate CAPI search results with 'local' WIP data */
        WipsService.buildJobSearchList = function($jobs) {

            return $http({
                method: 'POST',
                url: '/service/build_job_search_list',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    jobs: $jobs,
                })
            })

        }

        /* ADDED FOR CAPI */
        WipsService.getCapiJob = function(id, token) {

            // hard coded token for now but this will be a session var later on when loging integrated
            ///let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpZCI6ImY3MGVmOWU4ZjE2Mzg0NTUyNzk0MjQwN2IwNDE0ZGY1NDljMTE5OGMiLCJqdGkiOiJmNzBlZjllOGYxNjM4NDU1Mjc5NDI0MDdiMDQxNGRmNTQ5YzExOThjIiwiaXNzIjoiIiwiYXVkIjoiZ29DYXBpQ2xpZW50Iiwic3ViIjoicGF1bC5zaWx2ZXN0ZXJAbGFuZ2xhbmQuY28udWsiLCJleHAiOjE2MDgxMzg2MDIsImlhdCI6MTYwODEwNjIwMiwidG9rZW5fdHlwZSI6ImJlYXJlciIsInNjb3BlIjoicm9vdCB1c2UgamIifQ.HwgncdFl2n8qxqUlF7JZU0iq29b1jN5TuO_wgMb_IL2Xnm4BOxxyJN2mi1VhfUEKgk9PRrB6OS0v6w9PUF1XLmmUsJZTmlE4dAXzIObm4VGR6j4LaWh5qelBE7mLKQQoNiAxToL-wnzXvsgUygQNicHDEdwfmJpLKmsHZNzLyyiRfgPrPmUU4C04znDn-mYFpvTbHuEKsjs6sVYxaJbiniqWz8sMlQUODOdR0wp2_1zIlSQJJ5AymTtyE6gEQ8aUMu1q-ev3GjTCLrxZrV48PG_nQEK2zScHm-Ds9MHfRq1C1eK-o_cM51iFKqwHGfiJnoWdQIzqvSWex8vpLBVY1Q";

            let filter = [{table:'jobs',id: id}];

            return $http({
                method: 'POST',
                url: APIEndpoint,
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    'graph': 'getJobs',
                    'category': 'LanglandToolsService',
                    'access_token': token,
                    'filter': JSON.stringify(filter), 
                    'limit': 50,
                    'offset': 0,
                    'order': null,
                    'search': null,
                })                
            })            
        }


         
        WipsService.getWips = function(filters) {

            return $http({
                method: 'POST',
                url: '/service/get_wips',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    filters: filters
                })
            })
            
        }
         
        WipsService.getAllActivePersons = function(type) {
            
            return $http({
                method: 'POST',
                url: '/service/get_all_active_persons',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({})
            })
            
        }
        
        WipsService.getWipsStatuses = function() {
            
            return $http({
                method: 'POST',
                url: '/service/get_wips_statuses',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({})
            })
            
        }
        
        WipsService.getWipsStages = function() {
            
            return $http({
                method: 'POST',
                url: '/service/get_wips_stages',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({})
            })
            
        }
        
        WipsService.getWipsSectors = function() {
            
            return $http({
                method: 'POST',
                url: '/service/get_wips_sectors',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({})
            })
            
        }
        
         WipsService.getWipsJobTypes = function() {
            
            return $http({
                method: 'POST',
                url: '/service/get_wips_job_types',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({})
            })
            
        }
        
        WipsService.getActiveWipsClients = function() {
            
            return $http({
                method: 'POST',
                url: '/service/get_active_wips_clients',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({})
            })
            
        }
        
        WipsService.saveViewData = function(title, description, public_private, set_default, columns, filters, orderCol, orderOrder, viewSql) {
            
            return $http({
                method: 'POST',
                url: '/service/save_view_data',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    title: title,
                    description: description, 
                    public_private: public_private,
                    set_default: set_default,
                    columns: columns,
                    filters: filters,
                    ordercol: orderCol,
                    orderorder: orderOrder,
                    viewsql: viewSql
                })
            })
            
        }
        
        WipsService.updateViewData = function(vid, title, description, public_private, set_default, columns, filters, orderCol, orderOrder, viewSql) {
            
            return $http({
                method: 'POST',
                url: '/service/update_view_data',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    vid: vid,
                    title: title,
                    description: description, 
                    public_private: public_private,
                    set_default: set_default,
                    columns: columns,
                    filters: filters,
                    ordercol: orderCol,
                    orderorder: orderOrder,
                    viewsql: viewSql
                })
            })

        }
        
        WipsService.promptUpdateViewData = function(vid, columns, filters, orderCol, orderOrder, viewSql) {
            
            return $http({
                method: 'POST',
                url: '/service/prompt_update_view_data',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    vid: vid,
                    columns: columns,
                    filters: filters,
                    ordercol: orderCol,
                    orderorder: orderOrder,
                    viewsql: viewSql
                })
            })

        }
        
        WipsService.removeView = function(vid) {
            
            return $http({
                method: 'POST',
                url: '/service/remove_view',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    vid: vid
                })
            })
            
        }
        
        WipsService.getViewData = function(vid) {
        
            return $http({
                method: 'POST',
                url: '/service/get_view_data',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    vid: vid
                })
            })
            
        }
   
        WipsService.getUserViews = function() {

            return $http({
                method: 'POST',
                url: '/service/get_user_views',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({})
            })
            
        }
        
        
        WipsService.csvExport = function(filters, showcols) {

            return $http({
                method: 'POST',
                url: '/service/csv_export',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    filters: filters, 
                    showcols: showcols
                })
            })
            
        }
        
        
        WipsService.getUserSession = function() {
            
            return $http({
                method: 'POST',
                url: '/service/get_user_session',
                headers: {'Content-Type': 'application/json'},
                    data: JSON.stringify({
                        
                })
            })
                       
        }
        
        /*
        WipsService.checkUserSession = function() {
            
            return $http({
                method: 'POST',
                url: '/service/check_user_session',
                headers: {'Content-Type': 'application/json'},
                    data: JSON.stringify({
                        
                })
            })
        }
        */
        
        WipsService.lockWip = function(pjid) {
         
            $http({
                method: 'POST',
                url: '/service/lock_wip',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    pjid: pjid
                })
            })
            
        }
        
        WipsService.unlockWip = function(pjid) {
         
            $http({
                method: 'POST',
                url: '/service/unlock_wip',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    pjid: pjid
                })
            })
            
        }
        
        WipsService.isWipLocked = function(pjid) {
            
            return $http({
                method: 'POST',
                url: '/service/is_wip_locked',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    pjid: pjid, 
                })
            })
            
        }
        
        /**** Testing encryption ****/
         WipsService.testEncryptString = function(string) {
             
            return $http({
                method: 'POST',
                url: '/service/test_encrypt_string',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    string: string, 
                })
            })
        }
        
        
        
        return WipsService;
    }
])

/* Will need to add Admin services here in later phase */
/*
.factory("AdminService", [
    "$http",
    function($http) {

        var AdminService = {};

        AdminService.getAllUsers = function(type) {
            
            return $http({
                method: 'POST',
                url: '/admin/get_all_users',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({})
                
            })
        }
        
        AdminService.getUser = function(urid) {
            
            
            return $http({
                method: 'POST',
                url: '/admin/get_user',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    urid : urid,
                    
                })
                
            })
        }
        
        AdminService.saveUser = function(submission) {
            
             return $http({
                method: 'POST',
                url: '/admin/save_user',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    urid: submission.urid,
                    full_name: submission.full_name,
                    email_address: submission.email_address,
                    job_title: submission.job_title,
                    department: submission.department,
                    location: submission.location,
                    password: submission.password,
                    user_type: submission.user_type,
                    status: submission.status,
                })
                
            })
            
        }

        AdminService.generatePassword = function() {
            
            return $http({
                method: 'POST',
                url: '/admin/generate_password',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({})
            })
        } 


        AdminService.saveUserPassword = function(submission) {
            
             return $http({
                method: 'POST',
                url: '/admin/save_user_password',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    urid: submission.urid,
                    password: submission.password,
                   
                })
                
            })
        }
        
         AdminService.getLockedWips = function() {
            
            return $http({
                method: 'POST',
                url: '/admin/get_locked_wips',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({})
                
            })
        }
        
         AdminService.unlockWipsRecord = function(pjid) {
         
            return $http({
                method: 'POST',
                url: '/admin/unlock_wips_record',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    pjid: pjid
                })
            })
        }
        
        AdminService.unlockAllWipsRecords = function() {
         
            return $http({
                method: 'POST',
                url: '/admin/unlock_all_wips_records',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({})
            })
        }
        
        AdminService.getPeopleList = function() {
            
            return $http({
                method: 'POST',
                url: '/admin/get_people_list',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({})
                
            })
        }
        
        AdminService.savePeopleData = function(name, type, active) {
            
            return $http({
                method: 'POST',
                url: '/admin/save_people_data',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    name: name,
                    type: type,
                    active: active
                   
                })
                
            })
        }
        
        
        AdminService.togglePeopleActive = function(id, active) {
            
            return $http({
                method: 'POST',
                url: '/admin/toggle_people_active',
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify({
                    id: id,
                    active: active
                })
                
            })
        }
        
        
        
        return AdminService;
    }
    
])

*/

;
