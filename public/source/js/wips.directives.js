angular.module("wips.directives", [])

.directive('editCreateWipsRecord', function() {
  return {
        restrict: 'AE',
        replace: 'true',
        templateUrl: '/public/templates/edit-create-wips-form.html',
        link: function(scope, element, attrs) {
            (function ( $ ) { 
                $('[data-toggle="datepicker"]').datepicker({
                    filter: function(date) {
                        if (date.getDay() === 0 || date.getDay() === 6) {
                            return false; // Disable all Sundays & Saturdays
                        }
                    },
                    weekStart: 0, inline: false, format: 'dd-mm-yyyy', autoHide: true});
            }(jQuery));
        }
  };
})

.directive('jobSearchContainer', function() {
    return {
        restrict: 'AE',
        replace: 'true',
        templateUrl: '/public/templates/job-search-container.html',
    };
})

.directive('wipsViewsMain', function() {
    return {
        restrict: 'AE',
        replace: 'true',
        templateUrl: '/public/templates/wips-views-main.html',
    };
})

/*
.directive('colorTestPage', function() {
    return {
        restrict: 'AE',
        replace: 'true',
        templateUrl: '/public/templates/testing/color-test.html',
    };
})

.directive('functionTestPage', function() {
    return {
        restrict: 'AE',
        replace: 'true',
        templateUrl: '/public/templates/testing/function-test.html',
    };
})
*/
.directive('displayControlPanelFilters', function() {
    return {
        restrict: 'AE',
        replace: 'true',
        templateUrl: '/public/templates/display-control-panel-filters.html',
    };
})

.directive('userViewsTabs', function() {
    return {
        restrict: 'AE',
        replace: 'true',
        templateUrl: '/public/templates/user-views-tabs.html',
    };
})

.directive('tooltip', function(){
    return {
        restrict: 'A',
        link: function(scope, element, attrs){
            $(element).hover(function(){
                // on mouseenter
                $(element).tooltip('show');
            }, function(){
                // on mouseleave
                $(element).tooltip('hide');
            });
        }
    };
})

.directive('ngJsonExportExcel', function() {
    return {
        //restrict: 'AE',
        scope: {
            data: '=',
            reportFields: '=',
            myFilename: '=',
            nestedReportFields: '=',
            nestedDataProperty: "@"
        }, 
        link: function(scope, element) {
            scope.myFilename = !!scope.myFilename ? scope.myFilename : "wips-export";
      
            function generateFieldsAndHeaders(fieldsObject, fields, header) {
                angular.forEach(fieldsObject, function(field, key) {
                    if (!field || !key) {
                        throw new Error("error json report fields");
                    }
                    fields.push(key);
                    header.push(field);
                });
                return {
                    fields: fields,
                    header: header
                };
            }
            var fieldsAndHeader = generateFieldsAndHeaders(scope.reportFields, [], []);
            var fields = fieldsAndHeader.fields;
            var header = fieldsAndHeader.header;
            var nestedFieldsAndHeader = generateFieldsAndHeaders(scope.nestedReportFields, [], [""]);
            var nestedFields = nestedFieldsAndHeader.fields;
            var nestedHeader = nestedFieldsAndHeader.header;

            scope.$watch('reportFields', function() {
                fieldsAndHeader = generateFieldsAndHeaders(scope.reportFields, [], []);
                fields = fieldsAndHeader.fields;
                header = fieldsAndHeader.header;
            });

            function _convertToExcel(body, header) {
                return header + "\n" + body;
            }

            function _objectToString(object) {
                var output = "";
                angular.forEach(object, function(value, key) {
                    output += key + ":" + value + " ";
                });
                return "'" + output + "'";
            }

            function generateFieldValues(list, rowItems, dataItem) {
                angular.forEach(list, function(field) {
                    var data = "",
                    fieldValue = "",
                    curItem = null;
                    if (field.indexOf(".")) {
                        field = field.split(".");
                        curItem = dataItem;
                        // deep access to obect property
                        angular.forEach(field, function(prop) {
                        if (curItem !== null && curItem !== undefined) {
                            curItem = curItem[prop];
                        }
                    });
                    data = curItem;
                    } else {
                        data = dataItem[field];
                    }
                    fieldValue = data !== null ? data : " ";
                    if (fieldValue !== undefined && angular.isObject(fieldValue)) {
                        fieldValue = _objectToString(fieldValue);
                    }
              
                    else if (fieldValue !== undefined && fieldValue.indexOf(',') > -1) {
                        fieldValue = fieldValue.replace(/,/g, ' ');
                    }
                    
                    rowItems.push(fieldValue.replace(/\n|\r/g, " "));
                });
                return rowItems;
            }

            function _bodyData() {
                var body = "";
                var fieldsAndHeader = generateFieldsAndHeaders(scope.reportFields, [], []);
                var fields = fieldsAndHeader.fields,
                    header = fieldsAndHeader.header;
                var nestedFieldsAndHeader = generateFieldsAndHeaders(scope.nestedReportFields, [], [""]);
                var nestedFields = nestedFieldsAndHeader.fields,
                    nestedHeader = nestedFieldsAndHeader.header;
            
                angular.forEach(scope.data, function(dataItem) {
                    var rowItems = [];
                    var nestedBody = "";
                    rowItems = generateFieldValues(fields, rowItems, dataItem);
                    //Nested Json body generation start 
                    if (scope.nestedDataProperty && dataItem[scope.nestedDataProperty].length) {
                        angular.forEach(dataItem[scope.nestedDataProperty], function(nestedDataItem) {
                            var nestedRowItems = [""];
                            nestedRowItems = generateFieldValues(nestedFields, nestedRowItems, nestedDataItem);
                            nestedBody += nestedRowItems.toString() + "\n";
                        });
                        var strData = _convertToExcel(nestedBody, nestedHeader);
                        body += rowItems.toString() + "\n" + strData;
                        ////Nested Json body generation end 
                    } else {
                        body += rowItems.toString() + "\n";
                    }
                });
                return body;
            }

            // $timeout(function() {
            element.bind("click", function() {
                var bodyData = _bodyData();
                var strData = _convertToExcel(bodyData, header);
                var blob = new Blob([strData], {
                    type: "text/plain;charset=utf-8"
                });
                return saveAs(blob, [scope.myFilename + ".csv"]);
            });
         // }, 1000);

        }
    };
})

// ** ADMIN SCREENS **
/*.directive('adminOptions', function() {
  return {
      restrict: 'AE',
      replace: 'true',
      templateUrl: '/public/templates/admin/admin-options.html',
      
  };
})*/

.directive('adminMenu', function() {
  return {
      restrict: 'AE',
      replace: 'true',
      templateUrl: '/public/templates/admin/admin-menu.html',
      
  };
})
/*
.directive('adminFinanceImport', function() {
  return {
      restrict: 'AE',
      replace: 'true',
      templateUrl: '/public/templates/admin/admin-finance-import.html',
      
  };
})

// users
.directive('adminUsersList', function() {
  return {
      restrict: 'AE',
      replace: 'true',
      templateUrl: '/public/templates/admin/users/admin-users-list.html',
      
  };
})

.directive('adminUsersForm', function() {
  return {
      restrict: 'AE',
      replace: 'true',
      templateUrl: '/public/templates/admin/users/admin-users-form.html',
      
  };
})

.directive('adminUsersView', function() {
  return {
      restrict: 'AE',
      replace: 'true',
      templateUrl: '/public/templates/admin/users/admin-users-view.html',
      
  };
})

.directive('adminUsersPassword', function() {
  return {
      restrict: 'AE',
      replace: 'true',
      templateUrl: '/public/templates/admin/users/admin-users-password.html',
      
  };
})
*/
.directive('adminLockedWips', function() {
  return {
      restrict: 'AE',
      replace: 'true',
      templateUrl: '/public/templates/admin/locked/admin-locked-wips.html',
      
  };
})

.directive('adminPeopleSelects', function() {
  return {
      restrict: 'AE',
      replace: 'true',
      templateUrl: '/public/templates/admin/data/admin-people-selects.html',
      
  };
})

// production
.directive('productionMenu', function() {
  return {
      restrict: 'AE',
      replace: 'true',
      templateUrl: '/public/templates/production/production-menu.html',
      
  };
})

.directive('productionActive', function() {
  return {
      restrict: 'AE',
      replace: 'true',
      templateUrl: '/public/templates/production/production-table.html',
      
  };
})

.directive('productionClosed', function() {
  return {
      restrict: 'AE',
      replace: 'true',
      templateUrl: '/public/templates/production/production-table.html',
      
  };
})

.directive('productionAdd', function() {
  return {
      restrict: 'AE',
      replace: 'true',
      templateUrl: '/public/templates/production/production-add.html',
      
  };
})


.directive('productionAddRecord', function() {
    return {
        restrict: 'AE',
        replace: 'true',
        templateUrl: '/public/templates/production/production-add-record.html',
        link: function(scope, element, attrs) {
              (function ( $ ) { 
                  $('[data-toggle="datepicker"]').datepicker({
                      filter: function(date) {
                          if (date.getDay() === 0 || date.getDay() === 6) {
                              return false; // Disable all Sundays & Saturdays
                          }
                      },
                      weekStart: 0, inline: false, format: 'dd-mm-yyyy', autoHide: true});
              }(jQuery));
          }

      };
})

.directive('productionFilters', function() {
  return {
      restrict: 'AE',
      replace: 'true',
      templateUrl: '/public/templates/production/production-filters.html',
      
  };
})     

        
;