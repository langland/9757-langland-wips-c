/* 
 * file inluding any useful js functions to use globally 
 */



// function to pad strings with zeroes - useful for date/tima manipulation...        
function zeroPad(num, places) {
    var zero = places - num.toString().length + 1;
    return Array(+(zero > 0 && zero)).join("0") + num;
   
}


function getPrefixedViews() {
    
    var prefixedViews = array();
    
    prefixedViews['ctr_uk'] = array();
    prefixedViews['ctr_uk']['title'] = 'CTR UK';
    prefixedViews['ctr_uk']['description'] = 'Active CTR UK WIPs records';
    prefixedViews['ctr_uk']['columns'] = {
        'client' : true,
        'sector' : false,
        'jnumber' : true,
        'jtype' : false,
        'project' : true,
        'digital' : true,
        'countries' : true,
        'items' : true,
        'status' : true,
        'status_notes' : true,
        'stages' : true,
        'resources' : true,
        'livedate' : true,
        'account_handlers' : true,
        'lead_delivery' : true,
        'creatives' : true, 
        'last_updated' : true,
        'actions' : true,
    };
    
    prefixedViews['ctr_uk']['filters'] = {
        'sectors' : ['3'],
        'clients' : [],
        'types' : ['1'],
        'digital' : [],
        'status' : [],
        'show_completed' : false,
        'show_cancelled' : false,
        'only_completed' : false,
        'only_cancelled' : false,
        'only_mine' : false,
        'stages' : [],
        'resource_requests' : false,
        'account_handlers' : [],
        'lead_delivery' : [],
        'lead_creatives' : []
    };
    
    prefixedViews['ctr_uk']['ordercol'] = 'client';
    prefixedViews['ctr_uk']['orderorder'] = 'asc';
    prefixedViews['ctr_uk']['sql'] = 'SELECT wj.id AS wid, wj.pjid AS pjid, pj.client AS pjclient FROM wips_jobs wj LEFT JOIN parent_jobs pj ON pj.id = wj.pjid LEFT JOIN wips_jobs_status wj_status ON wj_status.wj_id = wj.id WHERE wj.current = 1 AND pj.sector IN (3) AND pj.type IN (1) AND wj_status.ws_id IN (1,2,3,4) ORDER BY pj.client ASC';
    
    
}