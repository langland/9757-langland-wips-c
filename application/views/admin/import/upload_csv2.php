<!doctype html>
<html lang="en"  ng-app="wips.controllers">

    <?php print $head; ?>

    <body class="admin-page <?php print $vserver; ?>" id="data-import">
        
        <?php print $header; ?>
        
        <div class="container-fluid">

            <div class="content">
                
                <div id="admin-menu">
    
                    <ul class="nav nav-tabs main-tabs">
                        <li>
                            <a href="/#!/admin/users">Manage Users</a>
                        </li> 
                       <li>
                            <a href="/admin/access_import">Active Jobs Import</a>
                        </li> 
                        <li  class="active">
                            <a href="/admin/access_import2">Finance Data Import</a>
                        </li> 
                        <li>
                            <a href="/#!/admin/locked-wips">Manage Locked WIPs</a>
                        </li> 
                        <li>
                            <a href="/#!/admin/people-selects">WIPs selection data - people</a>
                        </li> 
                        
                    </ul>
    
                </div>
                
                
                <div class="row" id="admin-data-import">
                

                    <div class="col-xs-0 col-lg-0 col-xl-1"></div>

                    <div class="col-xs-12 col-lg-12 col-xl-10"> 
                    
                        <h1>Finance Data Import</h1>

                        <div class='data-container'> 

                            <div class="data-upload-container">

                            <h3>Select Processing option and Finance data CSV and click Upload</h3>

                            <form action="/admin/access_import2/upload_data" method="post" enctype="multipart/form-data" name="form1" id="form1"> 

                                <div style='padding: 10px;'>
                                   
                                    <input type="radio" id="processBoth" name="process" value="both" checked="checked"> <label for="processBoth" style="font-weight: normal; font-size: 13px;">Import for WIPS Finance AND Reports Dashboard</label><br>
                                    <input type="radio" id="processWips" name="process" value="wips"> <label for="processWips" style="font-weight: normal; font-size: 13px;">Import for WIPS Finance ONLY</label><br>
                                    <input type="radio" id="processDashboard" name="process" value="dashboard"> <label for="processDashboard" style="font-weight: normal; font-size: 13px;">Import for Reports Dashboard ONLY</label>
                                </div>
                                
                                
                                <input required type="file" class="form-control" name="userfile" id="userfile"  align="center"/>

                                <button type="submit" name="submit" class="btn btn-default">Upload</button>

                            </form>
                              <?php
                                if ($this->session->flashdata('import-message2')) {
                                    
                                    print '<div class="process-message">';
                                    print $this->session->flashdata('import-message2');
                                    print '</div>';
                                    
                                }
                                ?>
                            
                            </div>
                        </div>
                        
                    <div class="col-xs-0 col-lg-0 col-xl-1"></div>

                </div>
                
            </div>

        </div>



    </body>

</html>      


