      <?php if ($vserver == 'localhost') : ?>
            <div class="guide-boxes" ng-show='showDebug'>
                <span class="hidden-sm hidden-md hidden-lg show-xs">XS</span>
                <span class="hidden-xs hidden-md hidden-lg show-sm">SM</span>
                <span class="hidden-sm hidden-xs hidden-lg show-md">MD</span>
                <span class="hidden-sm hidden-md hidden-xs show-lg">LG</span>
            </div>
        <?php endif; ?>
    <span class='version-info'>v. <?php print $version; ?></span>
       
        <header class="header">
            <div class="container-fluid">
                <div class='row'>
                    
                    <div class='mycol col-xs-7 col-md-2 col-lg-2 logo-holder'>
                         <img class='logo' src="/public/Langland-wips-logo-WHITE.png" /><br /> 
                    </div>
                    
                    <div class='mycol col-md-10 col-lg-7'>
       
                        <div class="menu-item first">
                            <a href='/#!/my-views/<?php print $user_data['default_view']; ?>'>Creative WIPs Views</a>
                        </div>
                        
                        <div class="menu-item">
                            <a href='/#!/job-search'>+ Add Creative WIPs</a>
                        </div>
                        
                        <div class="menu-item">
                            <a href='/#!/production'>Production</a>
                        </div>
                        
                        <div class="menu-item active">
                            <a href='/#!/admin'>Administration</a>
                        </div>
                    </div>
                    
            
                    <div class='mycol col-md-12 col-lg-3'>
                        <div class="head-button" style="float: right;" ng-click="appIntroMessage()">HALP!</div>
                        <div class="head-button" style="float: right;" ><a href="/wips/logout">LOGOUT</a></div>
                        <div class="user-info" style="float: right; text-align: left; line-height: 15px; margin-top: 20px;"><?php print $user_data['user']->full_name; ?><br />(<?php print $user_data['user']->user_type_name; ?>)</div>
                    </div>
                    
                </div>
              
                
            </div>
        </header>