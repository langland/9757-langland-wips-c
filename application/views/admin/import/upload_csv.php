<!doctype html>
<html lang="en"  ng-app="wips.controllers">

    <?php print $head; ?>

    <body class="admin-page <?php print $vserver; ?>" id="data-import">

        <?php print $header; ?>
        
        <div class="container-fluid">

            <div class="content">
                
                <div id="admin-menu">
    
                    <ul class="nav nav-tabs main-tabs">
                        <li>
                            <a href="/#!/admin/users">Manage Users</a>
                        </li> 
                        <li class="active">
                            <a href="/admin/access_import">Active Jobs Import</a>
                        </li> 
                        <li>
                            <a href="/admin/access_import2">Finance Data Import</a>
                        </li> 
                        <li>
                            <a href="/#!/admin/locked-wips">Manage Locked WIPs</a>
                        </li> 
                        <li>
                            <a href="/#!/admin/people-selects">WIPs selection data - people</a>
                        </li> 
                        
                    </ul>
    
                </div>
                
                <div class="row" id="admin-data-import">
                

                    <div class="col-xs-0 col-lg-0 col-xl-1"></div>

                    <div class="col-xs-12 col-lg-12 col-xl-10"> 
                    
                        <h1>Active Jobs Import</h1>
                        
<!--                        <div class='data-container' style="display: none;">    

                            <div class="clear-data-testing">

                                <h3>Clear Data (for testing during development)</h3>


                                 <p><a href="/admin/dev_empty_data">Click to empty test data</a></p>

                                <?php /*
                                if ($this->session->flashdata('empty-message')):
                                    print $this->session->flashdata('empty-message');
                                endif; */
                                ?>
                            </div>

                        </div>-->

                        <div class='data-container'> 

                            <div class="data-upload-container">

                            <h3>Select Active Jobs CSV and click Upload</h3>

                            <form action="/admin/access_import/upload_data" method="post" enctype="multipart/form-data" name="form1" id="form1"> 

                                <input required type="file" class="form-control" name="userfile" id="userfile"  align="center"/>

                                <button type="submit" name="submit" class="btn btn-default">Upload</button>

                            </form>
                            
                                <?php
                                if ($this->session->flashdata('import-message')) {
                                    print '<div class="process-message">';
                                    print $this->session->flashdata('import-message');
                                    print '</div>';
                                }
                                ?>
                            
                            </div>
                        </div>
                      
                    <div class="col-xs-0 col-lg-0 col-xl-1"></div>

                </div>
                
            </div>

        </div>



    </body>

</html>      


