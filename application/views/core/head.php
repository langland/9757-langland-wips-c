<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-touch-fullscreen" content="yes">
        <title>Langland WIPs Status</title>

        <!-- Bootstrap -->
        <link rel="stylesheet" type="text/css" href="/public/css/bootstrap-css/css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="/public/css/datepicker.min.css"/>

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i" rel="stylesheet" />
        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="/public/css/main.css?version=<?php print time() ?>"/>
        <link rel="stylesheet" type="text/css" href="/public/css/loading-bar.min.css"/>

        <!-- JS -->
        <script type="text/javascript" src="/public/js/FileSaver.min.js"></script>
        <script type="text/javascript" src="/public/js/pdfmake.min.js"></script>
        <script type="text/javascript" src="/public/js/vfs_fonts.js"></script>
        <script type="text/javascript" src="/public/js/wips.js?version=<?php print time() ?>"></script>
        <script type="text/javascript" src="/public/js/datepicker.js"></script>
    </head>