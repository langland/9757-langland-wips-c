      <?php if ($vserver == 'localhost') : ?>
            <div class="guide-boxes" ng-show='showDebug'>
                <span class="hidden-sm hidden-md hidden-lg show-xs">XS</span>
                <span class="hidden-xs hidden-md hidden-lg show-sm">SM</span>
                <span class="hidden-sm hidden-xs hidden-lg show-md">MD</span>
                <span class="hidden-sm hidden-md hidden-xs show-lg">LG</span>
            </div>
        <?php endif; ?>
  <span class='version-info' ng-click="popVersionHistory()"><?php print $version; ?></span>
       
       
        <header class="header">
            <div class="container-fluid">
                
                
                
                <div class='row'>
                    
                    <div class='mycol logo-holder col-xs-12 col-sm-3 col-md-2 col-lg-2 logo-holder'>
                        <img class='logo' style="width: 150px" src="/public/0005_WIPS-STATUS.png" /> 
                    </div>
                   
                    <div class='mycol menu-items col-xs-12 col-sm-9 col-md-10 col-lg-7'>
      
                        <div ng-if="user.perms.wips.view == 1" class="menu-item first" ng-controller="ViewTabsController" ng-click="goHome()" ng-class="{'active': pageDisplay == 'wips-views-main' }">
                            Creative WIPs Views
                        </div>
                    
                        <div ng-if="user.perms.wips.view == 1" class="menu-item" ng-click="locationChangeCheckViewUpdate('job-search')" ng-class="{'active': pageDisplay == 'job-search' }">
                            + Add Creative WIPs
                        </div>
                        
                        <div class="menu-item" ng-if="user.perms.prod.view == 1" ng-click="locationChangeCheckViewUpdate('production')" ng-class="{'active': activeMenu == 'production' }">
                            Production
                        </div>
                        
                        <div class="menu-item" ng-if="user.perms.admin.users == 1 || user.perms.admin.finance == 1 || user.perms.admin.locked == 1 || user.perms.admin.peoplesel == 1" ng-click="setLocationPath('admin')" ng-class="{'active': activeMenu == 'admin' }">
                            Administration
                        </div>
                    </div>
                    
                    <div class='hidden-sm hidden-md hidden-xs show-lg mycol user-info col-xs-12 col-md-12 col-lg-3'>
                        <div class="head-button" ng-click="appIntroMessage()">HALP!</div>
                        <div class="head-button"  ><a href="/wips/logout">LOGOUT</a></div>
                        <div class="user-name"  style="padding: 5px 0"><?php print $user_data['user']->full_name; ?></div>
          
                    </div>
                    
                </div>
                                
                
                <div class="row show-sm show-md hidden-lg show-xs textright">
               
                    <div class="col-xs-12 user-info">
                    
                <div class="head-button" style="float: right;" ng-click="appIntroMessage()">HALP!</div>
                        <div class="head-button"><a href="/wips/logout">LOGOUT</a></div>
                        <div class="user-name" style="padding: 5px 0"><?php print $user_data['user']->full_name; ?> </div>
          
                    </div>
                </div>
                
                
               
                <!--<div class='row'>
                     <p class="textleft testpara" style="display: block;">
                            <span ng-click="setLocationPath('color-test')" class="nglink">colour tests</span> 
                            <span>|</span> 
                            <span ng-click="setLocationPath('functional-tests')" class="nglink">functional tests</span>
                            <span>|</span>
                            <span ng-click="appIntroMessage()" class="nglink">welcome pop test</span>
                            <span>|</span>
                            <span ng-click="modalMessage('info', 'testing, blah', 'TEST MESSAGE')" class="nglink">modal message</span>
                            <span>{{entryInit}}</span>
                            <span>|</span>
                            <span ng-click="gotoAnchor('141_1635')">ROCHEP</span>
                            
                            <span ng-click="gotoAnchor('150_4')">MC</span>
                            
                          
                        </p>
                    
                </div>-->
              
                
            </div>
        </header>