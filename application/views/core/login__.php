<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-touch-fullscreen" content="yes">
        <title>Langland WIPs Status</title>

        <!-- Bootstrap -->
        <link rel="stylesheet" type="text/css" href="/public/css/bootstrap-css/css/bootstrap.min.css"/>

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i" rel="stylesheet">
        
        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="/public/css/main.css"/>

    </head>
    <body class="login-page <?php print $vserver; ?>">

        <?php if ($vserver == 'localhost') : ?>
            <div class="guide-boxes" ng-show='showDebug'>
                <span class="hidden-sm hidden-md hidden-lg show-xs">XS</span>
                <span class="hidden-xs hidden-md hidden-lg show-sm">SM</span>
                <span class="hidden-sm hidden-xs hidden-lg show-md">MD</span>
                <span class="hidden-sm hidden-md hidden-xs show-lg">LG</span>
            </div>
        <?php endif; ?>

         <span class='version-info'><?php print $version; ?></span>
        
        <header class="header">
            <div class="container-fluid">
                <div class='row'>
                    <div class='mycol col-xs-7 col-md-3 col-lg-2 logo-holder'>
                        <img class='logo' style="width: 150px" src="/public/0005_WIPS-STATUS.png" /> 
                    </div>
                    <div class='col-md-7 col-lg-7 hidden-xs hidden-sm'></div>

                    <div class='col-xs-5 col-md-2 col-lg-3'></div>
                </div>    
            </div>
        </header>

        <div class="container-fluid">

            <div class="content">

                <div class="col-sm-1 col-md-2 col-lg-3"></div>    
                <div class="col-sm-10 col-md-8 col-lg-6">

                    <h1 class="title">Sign in</h2>
                        <div class="form-container">
                            <?php
                            print form_open(
                                            '../wips/login', array('class' => '', 'id' => 'login-form')
                            );
                            ?>

                            <?php
                            print "<p class='inline-message'>" . ($this->session->flashdata('message') ? $this->session->flashdata('message') : '') . "</p>";
                            ?>

                            <?php
                            print "<p class='inline-message'>" . ($this->session->flashdata('login-failed') ? $this->session->flashdata('login-failed'). '<br /><strong>Please remember to use your new @publicislangland.com email address.</strong>' : '') . "</p>";
                            ?>

                            <?php
                            print isset($cmessage) ? "<p class='inline-message'>" . $cmessage . "</p>" : '';
                            ?>

                            <?php
                            print validation_errors() ? "<p class='inline-message'>" . validation_errors() . "</p>" : '';
                            ?>

                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label hidden-xs" for="si_email" >Email Address</label>
                                <div class="col-xs-12 col-sm-9">
                                    <input value="<?php print $this->session->flashdata('email') ? $this->session->flashdata('email') : set_value('si_email'); ?>" type="email" class="form-control" id="si_email" name="si_email"  placeholder="Enter your email address" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label hidden-xs" for="si_password">Password</label>
                                <div class="col-xs-12 col-sm-9">
                                    <input type="password" class="form-control" id="si_password" name="si_password" placeholder="Enter your Password" required>
                                    <!-- <small id="passwordHelp" class="form-text text-muted">forgotten password</small> -->
                                </div>
                            </div>    

                            <div class="form-group row">
                                <div class="col-xs-12 col-right">
                                    <button type="submit" class="btn btn-default">Sign in</button>
                                </div>
                            </div>

                            <?php
                            print form_close();
                            ?>
                        </div>

                </div>
                <div class="col-sm-1 col-md-2 col-lg-3"></div>  

            </div>
            
        </div>
        
    </body>

</html>