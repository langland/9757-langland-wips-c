<!doctype html>
<html lang="en" ng-app="wips.controllers">
   
    <?php print $head; ?>
    <body ng-init="init()" ng-controller="WipsController" class="<?php print $vserver; ?>" ng-class="myView">
  
        <?php print $header; ?>       
                    
        <div class="container-fluid">

            <div class="content">

<!--
                <div ng-if="pageDisplay == 'function-test'" function-test-page></div>

                <div ng-if="pageDisplay == 'color-test'" color-test-page></div>
-->
                <div ng-if="pageDisplay == 'job-search'" job-search-container></div>

                <div ng-if="pageDisplay == 'edit-create-wips'" edit-create-wips-record></div>

                <div ng-if="pageDisplay == 'wips-views-main'" wips-views-main></div>
                
                <div  ng-if="activeMenu == 'production' && user.perms.prod.view == 1" ng-controller="ProductionController">
                
                    <div ng-if="activeMenu == 'production' && user.perms.prod.view == 1" production-menu></div>
                    <div ng-if="pageDisplay == 'production-active' && user.perms.prod.view == 1" production-active></div>
                    <div ng-if="pageDisplay == 'production-closed' && user.perms.prod.view == 1" production-closed></div>
                    <div ng-if="pageDisplay == 'production-add' && user.perms.prod.view == 1" production-add></div>
                    <div ng-if="pageDisplay == 'production-add-record' && user.perms.prod.view == 1" production-add-record></div>

                </div>
                
                
                <div ng-if="(user.perms.admin.users == 1 || user.perms.admin.finance == 1 || user.perms.admin.locked == 1 || user.perms.admin.peoplesel == 1) && activeMenu =='admin'" admin-menu></div>
                
                <!-- moved to CAPI 
                <div ng-if="user.perms.admin.users == 1 && pageDisplay=='admin-users-list'" admin-users-list></div>
                <div ng-if="user.perms.admin.users == 1 && pageDisplay=='admin-users-form'" admin-users-form></div>
                <div ng-if="user.perms.admin.users == 1 && pageDisplay=='admin-users-password'" admin-users-password></div>
                <div ng-if="user.perms.admin.users == 1 && pageDisplay=='admin-users-view'" admin-users-view></div>
                -->
                
                <!-- not required 
                <div ng-if="user.perms.admin.finance == 1 && pageDisplay=='admin-finance-import'" admin-finance-import></div>
                -->
                
                <div ng-if="user.perms.admin.locked == 1 && pageDisplay=='admin-locked-wips'" admin-locked-wips></div>
                
                <div ng-if="user.perms.admin.peoplesel == 1 && pageDisplay=='admin-people-selects'" admin-people-selects></div>
                
            </div>

        </div>

    </body>

</html>