<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service extends CI_Controller {
   
    public function get_user_session() {
        
        if (array_key_exists('user', $this->session->userdata())) {
            $data['user'] = $this->session->userdata('user');
            $data['default_view'] = $this->session->userdata('default_view');
        }
        else {
            $data['user'] = false;
            $this->session->sess_destroy();
            $this->session->set_flashdata('login-failed', "Your session ended. Please log in again.");
        }
        echo json_encode($data);
    }
        
    
    public function get_job() {
        
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        
        $jid = $request->jid;
        $data['job'] = $this->wips->get_job($jid);
        
        echo json_encode($data);
        
    }
    
    public function save_wip() {
        
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        
        $jid = $request->jid;
        $wip = $request->wip;
        
        $data['message'] = $this->wips->save_wip($jid, $wip);
        
        echo json_encode($data);
    }

    /* new servcie for CAPI */
    public function save_capi_job_and_wip() {

        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        
        $capi_job = $request->capi_job;
        $wip = $request->wip;
        
        $data['message'] = $this->wips->save_capi_job_and_wip($capi_job, $wip);
        
        echo json_encode($data);

    }


     /* new service to integrate CAPI search results with 'local' WIP data */
     public function build_job_search_list() {

        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        
        $jobs = $request->jobs;
        
        $data['jobs'] = $this->wips->build_job_search_list($jobs);
        
        echo json_encode($data);
    }
    
    /* REDUNDANT - replace with CAPI
    public function search_jobs() {
        
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        
        $searchstring = $request->searchstring;
        
        $data['jobs'] = $this->wips->search_jobs($searchstring);
        
        echo json_encode($data);
    }
    */ 
    
    public function get_wips() {
        
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        
        $filters = $request->filters;
        
        $return = $this->wips->get_wips($filters);
        
        $data['wips'] = $return['wips'];
        $data['sql'] = $return['sql'];
        
//        $data['wips'] = $this->wips->get_wips($filters);
        
        echo json_encode($data);
    }
    
    public function get_people() {
        
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $type = $request->type;
        $data['people'] = $this->wips->get_people($type);
        echo json_encode($data);
    }
    
     public function get_all_active_persons() {
        
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);

        $data['persons'] = $this->wips->get_all_active_persons();

        echo json_encode($data);
    }
    
    
    public function get_wips_statuses() {
        
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);

        $data['statuses'] = $this->wips->get_wips_statuses();

        echo json_encode($data);
    }
    
    
    public function get_wips_stages() {
        
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);

        $data['stages'] = $this->wips->get_wips_stages();

        echo json_encode($data);
    }
    
    public function get_wips_sectors() {
        
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);

        $data['sectors'] = $this->wips->get_wips_sectors();

        echo json_encode($data);
    }
    
    public function get_wips_job_types() {
        
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);

        $data['jobtypes'] = $this->wips->get_wips_job_types();

        echo json_encode($data);
    }
    
    public function get_active_wips_clients() {
        
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);

        $data['clients'] = $this->wips->get_active_wips_clients();

        echo json_encode($data);
    }
    
    public function save_view_data() {
        
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
    
        $title = $request->title;
        $description = $request->description;
        $public_private = $request->public_private;
        $set_default = $request->set_default;
        $columns = $request->columns;
        $filters = $request->filters;
        $ordercol = $request->ordercol;
        $orderorder = $request->orderorder;
        $viewsql = $request->viewsql;
        
        $data['status'] = $this->wips->save_view_data($title, $description, $public_private, $set_default, $columns, $filters, $ordercol, $orderorder, $viewsql);
        
        echo json_encode($data);
    }
   
    public function update_view_data() {
        
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        
        $vid = $request->vid;
        $title = $request->title;
        $description = $request->description;
        $public_private = $request->public_private;
        $set_default = $request->set_default;
        $columns = $request->columns;
        $filters = $request->filters;
        $ordercol = $request->ordercol;
        $orderorder = $request->orderorder;
        $viewsql = $request->viewsql;
        
        $data['status'] = $this->wips->update_view_data($vid, $title, $description, $public_private, $set_default, $columns, $filters, $ordercol, $orderorder, $viewsql);
        
        echo json_encode($data);
    }
    
    public function prompt_update_view_data() {
        
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        
        $vid = $request->vid;
        $columns = $request->columns;
        $filters = $request->filters;
        $ordercol = $request->ordercol;
        $orderorder = $request->orderorder;
        $viewsql = $request->viewsql;
        
        $data['status'] = $this->wips->prompt_update_view_data($vid, $columns, $filters, $ordercol, $orderorder, $viewsql);
        
        echo json_encode($data);
    }
    
    public function remove_view() {
        
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        
        $vid = $request->vid;
        
        $data['status'] = $this->wips->remove_view($vid);
        
        echo json_encode($data);
        
    }
    
    public function get_view_data() {
        
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        
        $vid = $request->vid;
        
        $data['viewdata'] = $this->wips->get_view_data($vid);
        
        echo json_encode($data);
    }
    
    public function get_user_views() {
        
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        
        $data['views'] = $this->wips->get_user_views();
        
        echo json_encode($data);
    }
    
    public function lock_wip() {
        
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        
        $pjid = $request->pjid;
        
        $this->wips->lock_wip($pjid);
    }
    
    public function unlock_wip() {
        
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        
        $pjid = $request->pjid;
        
        $this->wips->unlock_wip($pjid);
    }
    
    public function is_wip_locked() {
        
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        
        $pjid = $request->pjid;
        
        $data['lockdata'] = $this->wips->is_wip_locked($pjid);
        
        echo json_encode($data);
    }
    
    
    // testing encryption
    public function test_encrypt_string() {
        
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        
        $string = $request->string;
        
        $data['string'] = $string;
        
        $encrypted = rtrim($this->encrypt->encode($string));
        $data['encrypted'] = $encrypted;
        
        $encrypt_decrypt = $this->encrypt->decode($encrypted);
        $data['encrypted_decrypt'] = $encrypt_decrypt;
        
        echo json_encode($data);
    }
    
}