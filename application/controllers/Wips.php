<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wips extends CI_Controller {
   
    /**
     * Index route
     */
    public function index() {
         
        $data = array();
        $content = array();
        
        if ($this->wips->is_in_maintenance()) {
            print 'maintenance mode';
            exit;
        }
        
        
        $content['vserver'] = false;
        $content['version'] = $this->config->item('version') ? $this->config->item('version') : false;
        
        $content['head'] = $this->load->view('core/head.php', '', TRUE);
        
        
        if (base_url() == $this->config->item('url_localhost')) {
            $content['vserver'] = 'localhost';
        }
        else if (base_url() == $this->config->item('url_staging')) {
            $content['vserver'] = 'staging';
        }
        
        if ($this->input->get('sx')) {
            $content['cmessage'] = 'Your session has expired. Please log in again to continue';
        } 
                
        if ($this->session->userdata('logged_in')) {
           
            $content['user_data'] = $this->session->userdata();
            
            $content['header'] = $this->load->view('core/header.php', $content, TRUE);
            
            $this->load->view('core/_layout.php', $content);
        } else {
            $this->load->view('core/login.php', $content);
        }
       
    }

    /**
     * Login 
     */
    public function login() {
        
        $login = $this->user->validate_user_by_credentials_c();

        //print_r($login);

    
        if ($login['status']) {

            $newdata = array(
                'user' => $login['user'],
                'urid' => $login['user']->uid,
                'user_type' => $login['user']->user_type,
                'logged_in' => TRUE,
                'default_view' => $login['default_view']
            );
            
            $this->session->set_userdata($newdata);

            $this->system->update_activity_log('user', 'Login');
            
            if ($newdata['user_type'] == 6) { // Producer
                redirect('../#!/production', 'refresh');
                exit;
            }
            elseif ($login['default_view']) {
                redirect('../#!/my-views/' . $login['default_view'], 'refresh');
                exit;
            }
            else {
                redirect('../', 'refresh');
                exit;
            }
           
        } else {
            $this->session->set_flashdata('email', $login['email']);
            $this->session->set_flashdata('login-failed', $login['reason']);
            redirect('../', 'refresh');
            exit;
        }
    }
    
  


    /**
     * Logout
     */
    public function logout() {
        
        // unlock any locked views...
       // $this->wips->unlock_wip_bu_userid($this->session->userdata('urid'));
        
        $newdata = array(
            'user' => '',
            'urid' => 0,
            'user_type' => '',
            'logged_in' => FALSE,  
        );
        
       // $this->system->update_activity_log('user', 'Logout');
        
        // clear the session and re-direct
        $this->session->unset_userdata($newdata);
        $this->session->sess_destroy();
        redirect('../', 'refresh');
        exit;
    }
    
    /**
     * TEST FUNCTION - for display job/wip object on screen for debug
     */
    public function get_job_test($jid) {
        
        $job = $this->wips->get_job($jid);
        print "<p>JOB RECORD : " . $jid . "</p>";
        print "<pre>";
        print_r($job);
        print "</pre>";
    }
    
    /**
     * TEST FUNCTION - for display a WUPS object 
     */
    public function get_wip_test($wid) {
        
        $wip = $this->wips->get_wip($wid);
        print "<p>JOB RECORD : " . $wid . "</p>";
        print "<pre>";
        print_r($wip);
        print "</pre>";
    }
    
}