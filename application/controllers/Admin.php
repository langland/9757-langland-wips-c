<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
   
    // empty the data during development
    /*
    public function dev_empty_data() {
        print $this->import->dev_empty_data(); 
    }
    */
    
    
    /*
     * Temp function for updating job numbers from import 
     * run on url admin/run_import
     * 
     * once done then comment out...
     *
     */
    /*
    public function run_import() {
        
        print 'Expired URL';
        return; // done with this now.
        //print "<h2>Temporary Job Number Import Process</h2>";
        //print $this->admin->temp_import();
        
    }

    
    public function run_us_import() {
        
        print "<h2>Temporary US Job Number Import Process for bulk import</h2>";
        
        print $this->admin->temp_us_import();
        
    }
    */
    /**
     * access_import 
     * 
     * Job data import function 
     *
     * @stage integer stage of the import process
     */
    /*
    public function altair_import($stage = false) {
   
        $user = $this->session->get_userdata();
        $data = array(
            'user_data' => $user,
        );
        
        if ($user['user']->user_type != 1) {
            $this->wips->logout();
            exit;
        }
        
        $data['vserver'] = false;
        $data['version'] = $this->config->item('version') ? $this->config->item('version') : false;
        
        $data['head'] = $this->load->view('core/head.php', '', TRUE);
        
        if (base_url() == $this->config->item('url_localhost')) {
            $data['vserver'] = 'localhost';
        }
        else if (base_url() == $this->config->item('url_staging')) {
            $data['vserver'] = 'staging';
        }
        
        $data['user_data'] = $this->session->userdata();
            
        $data['header'] = $this->load->view('admin/import/header.php', $data, TRUE);
        
        switch($stage) {
            
            // stage 2
            case 'upload_data':
               
                $data['csv_content'] = $this->altair->upload_csv();
                break;
            
            case 'force_upload': 
                $data['csv_content'] = $this->altair->upload_csv(true);
                break;
            
            // stage 1
            default: 
                
                $this->load->view('admin/import/altair_csv', $data); 
                break;
        }
    }
    */
    
    
    /**
     * access_import 
     * 
     * Job data import function 
     *
     * @stage integer stage of the import process
     */
    /*
    public function access_import($stage = false) {
   
        $user = $this->session->get_userdata();
        $data = array(
            'user_data' => $user,
        );
        
        if ($user['user']->user_type != 1) {
            $this->wips->logout();
            exit;
        }
        
        $data['vserver'] = false;
        $data['version'] = $this->config->item('version') ? $this->config->item('version') : false;
        
        $data['head'] = $this->load->view('core/head.php', '', TRUE);
        
        if (base_url() == $this->config->item('url_localhost')) {
            $data['vserver'] = 'localhost';
        }
        else if (base_url() == $this->config->item('url_staging')) {
            $data['vserver'] = 'staging';
        }
        
        $data['user_data'] = $this->session->userdata();
            
        $data['header'] = $this->load->view('admin/import/header.php', $data, TRUE);
        
        switch($stage) {
            
            // stage 2
            case 'upload_data':
               
                $data['csv_content'] = $this->import->upload_csv();
                break;
            
            // stage 1
            default: 
                
                $this->load->view('admin/import/upload_csv', $data); 
                break;
        }
    }
    */
    /**
     * access_import2 
     * 
     * Finance summary data import function 
     *
     * @stage integer stage of the import process
     */
    /*
    public function access_import2($stage = false) {
   
        $user = $this->session->get_userdata();
        $data = array(
            'user_data' => $user,
        );
        
        if ($user['user']->user_type != 1) {
            $this->wips->logout();
            exit;
        }
        
        $data['vserver'] = false;
        $data['version'] = $this->config->item('version') ? $this->config->item('version') : false;
        
        $data['head'] = $this->load->view('core/head.php', '', TRUE);
        
        if (base_url() == $this->config->item('url_localhost')) {
            $data['vserver'] = 'localhost';
        }
        else if (base_url() == $this->config->item('url_staging')) {
            $data['vserver'] = 'staging';
        }
        
        $data['user_data'] = $this->session->userdata();
            
        $data['header'] = $this->load->view('admin/import/header.php', $data, TRUE);
        
        switch($stage) {
            
            // stage 2
            case 'upload_data':
               
                $data['csv_content'] = $this->import->upload_csv2();
                break;
            
            // stage 1
            default: 
                
                $this->load->view('admin/import/upload_csv2', $data); 
                break;
        }
    }
    */

    /**
     * get_all_users
     * 
     * Return all users to ng service
     */
    /*
    public function get_all_users() {
        
        $data = array();
        
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        
        $data['users'] = $this->user->get_user(); 
        
        echo json_encode($data);
        
    }
    */
    /**
     * get_users
     * 
     * Return a single user to ng service
     */
     /*
    public function get_user() {
        
        $data = array();
        
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
                
        $data['user'] = $this->user->get_user($request->urid);
        
        echo json_encode($data);
    }
    */
    
    /**
     * save_user
     * 
     * Save user account details from ng service call
     */
    /*
    public function save_user() {
        
    //   $this->admin->check_access_allowed();
        
        $data = array();
        
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        
        $urid = $request->urid;
        $full_name = $request->full_name;
        $email_address = $request->email_address;
        $job_title = $request->job_title ? $request->job_title : '';
        
        $department = $request->department;
        $location = $request->location;
        
        $password = isset($request->password) ? md5($request->password) : '';
        $user_type = $request->user_type;
        $status = $request->status;
        
        $perms = $request->perms;
        
        $data['urid'] = $this->user->save_user($urid, 
                $full_name, 
                $email_address, 
                $job_title,
                $department,
                $location,
                $password,
                $user_type,
                $status,
                $perms);
        
        echo json_encode($data);
    }
    */
    /**
     * generate_password
     * 
     * Generate and return a random password to ng service
     */
    /*
    public function generate_password() {
        
    //   $this->admin->check_access_allowed();
         
        $data = array();
        
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        
        $data['password'] = $this->user->create_random_password(10);
        
        echo json_encode($data);
    }
    */
    /**
     * save_user_password
     * 
     * Save the password on a user account via ng service
     */
    /*
    public function save_user_password() {
        
    //  $this->admin->check_access_allowed();
        
        $data = array();
        
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        
        $urid = $request->urid;
        $password = md5($request->password);
        
        $data['status'] = $this->user->save_user_password($urid, $password);
        
        echo json_encode($data);
    }
    */
    /**
     * get_locked_wips
     * 
     * Get and return an array of locked wips records to ng service
     */
    public function get_locked_wips() {
        
    // $this->admin->check_access_allowed();
        
        $data = array();
        
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        
        $data['wips'] = $this->wips->get_locked_wips(); 
        
        echo json_encode($data);
    }
    
    /**
     * unlock_wips_record
     * 
     * Unlock a locked wips record based on id from ng service
     */
    public function unlock_wips_record() {
        
    // $this->admin->check_access_allowed();
        
        $data = array();
        
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        
        $pjid = $request->pjid;
        
        $this->wips->unlock_wip($pjid); 
        
        $data['status'] = 1;
        
        echo json_encode($data);
    }
    
    /**
     * unlock_all_wips_records
     * 
     * Unlock all locked wips records in one hit via ng service
     */
    public function unlock_all_wips_records() {
        
    // $this->admin->check_access_allowed();
        
        $data = array();
        
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        
        $this->wips->unlock_wip(0); 
        
        $data['status'] = 1;
        
        echo json_encode($data);
    }
    
    /**
     * get_people_lists
     * 
     * Get and return array of all 'people' (used for wips dropdowns - 'account handlers',
     * 'lead delivery' and 'lead creatives') to ng service.
     */
    public function get_people_list() {
        
    // $this->admin->check_access_allowed();
        
        $data = array();
        
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        
        $data['people'] = $this->wips->get_people_list(); 
        
        echo json_encode($data);
    }

    /**
     * save_people_data
     * 
     * Save a new dropdown 'person' ('account handlers', 'lead delivery',
     *  'lead creatives') record via ng service
     */
    public function save_people_data() {
        
        $data = array();
        
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        
        $name = $request->name;
        $type = $request->type;
        $active = $request->active;
        
        $data['status'] = $this->wips->save_people_data($name, $type, $active); 
        
        echo json_encode($data);
    }
    
    /**
     * toggle_people_active
     * 
     * Activate/de-activate dropdown 'person' ('account handlers', 'lead delivery',
     *  'lead creatives') record via ng service
     */
    public function toggle_people_active() {
        
        $data = array();
        
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
      
        
        $id = $request->id;
        $active = $request->active;
        
        $data['status'] = $this->wips->toggle_people_active($id, $active); 
        
     
        echo json_encode($data);
    }
    
    
    public function full_backup() {
        
        // $this->admin->check_access_allowed();
    
        $tables = array(
            'activity_log',
            'ci_sessions',
            'import_csv',
            'import_csv2',
            'import_history',
            'job_types',
            'parent_jobs',
            'production_partners',
            'production_record',
            'production_record_partners',
            'production_record_ref',
            'production_record_stage',
            'production_record_type',
            'production_stage',
            'production_type',
            'sectors',
            'users',
            'users_views',
            'user_types',
            'views',
            'views_default',
            'wips_finance_data',
            'wips_jobs',
            'wips_jobs_people_accounts',
            'wips_jobs_people_creatives',
            'wips_jobs_people_delivery',
            'wips_jobs_stages',
            'wips_jobs_status',
            'wips_lock',
            'wips_persons',
            'wips_persons_type',
            'wips_resource_requests',
            'wips_settings', 
            'wips_stages',
            'wips_status'
        );
        
        // back up to file also....
        $mybackup = $this->admin->backup_tables($tables);
        $backupfilename = $_SERVER['DOCUMENT_ROOT'] . '/_bups/' . date('Ymd_His') . '_fullbackup.sql';
        $handle = fopen($backupfilename, 'w' );
        fwrite($handle,$mybackup);
        fclose($handle);
        
        
    }
    
}
?>