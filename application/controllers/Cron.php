<?php
//defined('BASEPATH') OR exit('No direct script access allowed');

define('UNLOCK_KEY', '538a83d8ce347d95d9d0ff87b9176dbd');
define('UNLOCK_PERIOD', '86400'); // 24hrs = 24*60*60 


class Cron extends CI_Controller {
   
    public function unlock_wips($key = NULL) {
        
        // url must include the key for loose authentication
        if (!$key || $key != UNLOCK_KEY) {
            $activity = 'WIPS unlock process failed to authenticate.'; 
            return false; 
        }
        else {
            $deleted = $this->admin->wips_batch_unlock(UNLOCK_PERIOD);
            $activity = 'WIPS unlock process completed. ' . $deleted . ' records unlocked.'; 
            $this->system->update_activity_log('cron', $activity, 999999999);
            return true;
        }
    }
      
    // direct link to backup selection of mysql tables    
    public function backup_tables_file() {
        
        $tables = array(
            'parent_jobs',
            'wips_jobs',
            'wips_jobs_people_accounts',
            'wips_jobs_people_creatives',
            'wips_jobs_people_delivery',
            'wips_jobs_stages',
            'wips_jobs_status',
            'wips_finance_data',
            'wips_resource_requests',
        );
        
        // get backup data
        $mybackup = $this->admin->backup_tables($tables);
        
        $fname = $_SERVER['DOCUMENT_ROOT'] . '/_bups/' . date('Ymd_His') . '.sql';
        
       // print 'done';  
        $handle = fopen($fname, 'w' );
        fwrite($handle,$mybackup);
        fclose($handle);
        
        print "Done!<br />Saved to file: " . $fname;
    }
    
    // 
    public function send_wips_reminder_email($key = NULL) {
        
        if (!$key || $key != UNLOCK_KEY) {
            $activity = 'WIPs reminder email failed to authenticate.'; 
            $this->system->update_activity_log('cron', $activity, 999999999);
            return false; 
        }
        
        $email_status = $this->system->send_wips_reminder();
        
        if ($email_status) {
            $activity = 'WIPs reminder email sent successfully.'; 
        }
        else {
            $activity = 'WIPs reminder email sent failed to send.'; 
        }
            
        $this->system->update_activity_log('cron', $activity, 999999999);
        
        
    }

    function test($key = NULL){
    	
        if (!$key || $key != UNLOCK_KEY) {
           $activity = 'WIPs reminder email failed to authenticate.'; 
           $this->system->update_activity_log('cron', $activity, 999999999);
           return false; 
       }
       
       $email_status = $this->system->send_wips_reminder();
       
       if ($email_status) {
           $activity = 'WIPs reminder email sent successfully.'; 
       }
       else {
           $activity = 'WIPs reminder email sent failed to send.'; 
       }
           
       $this->system->update_activity_log('cron', $activity, 999999999);
       print  $activity;
   }

    public function clear_sessions($key = null) {
        $this->load->model('Cron_model');         
        $this->Cron_model->clear_sessions($key);

    }
    
}
?>