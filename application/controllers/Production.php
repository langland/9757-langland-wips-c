<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Production extends CI_Controller {

    /* will be replaced by CAPI search
    public function search_jobs() {

        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);

        $searchstring = $request->searchstring;

        $data['jobs'] = $this->prod->search_jobs($searchstring);

        echo json_encode($data);
    }
    */

    public function build_production_job_search_list() {


        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);

        $jobs = $request->jobs;
       
        $data['jobs'] = $this->prod->build_production_job_search_list($jobs);

        echo json_encode($data);

    }


    /* new for CAPI-fied update */
    public function set_parent_jobs_record() {

        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);

        $job = $request->job;
       
        $data['pjid'] = $this->wips->save_capi_job_and_wip($job, false);

        echo json_encode($data);

    }


    public function get_production_record() {

        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);

        $pjid = $request->pjid;
        $pr_pid = $request->pr_pid;

        $data['prodrec'] = $this->prod->get_production_data($pjid, $pr_pid);

        echo json_encode($data);
    }

    public function get_production_stages() {

        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);

        $data['stages'] = $this->prod->get_production_stages();

        echo json_encode($data);
    }

    public function get_production_types() {

        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);

        $data['types'] = $this->prod->get_production_types();

        echo json_encode($data);
    }

    public function get_production_partners() {

        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);

        $data['partners'] = $this->prod->get_production_partners();

        echo json_encode($data);
    }
    
    public function save_production_record() {

        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);

        $pjid = $request->pjid;
        $pr_pid = $request->pr_pid;
        $record = $request->record;

        $data['message'] = $this->prod->save_production_record($pjid, $pr_pid, $record);

        echo json_encode($data);
    }

    public function get_production_records() {

        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);

        $action = $request->action;
        $filters = $request->filters;

        $data['records'] = $this->prod->get_production_records_table($action, $filters);

        echo json_encode($data);
    }

    public function get_report_one_stats() {

        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);

        $data['stats'] = $this->prod->get_report_one_stats();

        echo json_encode($data);
    }

    public function get_report_two_stats() {

        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);

        $data['stats'] = $this->prod->get_report_two_stats();

        echo json_encode($data);
    }

    public function get_report_three_stats() {

        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);

        $data['stats'] = $this->prod->get_report_three_stats();

        echo json_encode($data);
    }
    
    public function get_report_one_stats_test() {

    //    $postdata = file_get_contents("php://input");
    //    $request = json_decode($postdata);

        $data['stats'] = $this->prod->get_report_one_stats();
        
        print "<pre>";
        print_r($data['stats']);
        print "</pre>";
        exit;
       
    }

}
?>