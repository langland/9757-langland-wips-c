<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['version'] = 'Beta CAPI v3.0';
$config['test'] = 'Test';

$config['url_localhost'] = 'http://localhost.wips/';
$config['url_staging'] = 'http://wips.langland-development.co.uk/';
$config['url_live'] = 'https://wips.langland-live.co.uk/';

$config['prod_word_limit'] = 10;

$config['import_exclude'] = array(
    'B3236-000314-01',
);

//$config['apiEndPoint'] = "'http://capi.langland-development.co.uk/proxy-dev/Resource.php'";
//$config['apiTokenEndPoint'] = "'http://capi.langland-development.co.uk/proxy-dev/Token.php'";
$config['apiEndPoint'] = "https://capi.langland-live.co.uk/proxy-dev/Resource.php";
$config['apiTokenEndPoint'] = "https://capi.langland-live.co.uk/proxy-dev/Token.php";
