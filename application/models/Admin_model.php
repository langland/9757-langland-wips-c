<?php

class Admin_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    
    
    public function wips_batch_unlock($interval) {
        
        $time_limit = time() - $interval;
        
        $this->db->where('locked_on <', $time_limit);
        $this->db->delete('wips_lock');
       // $this->db->delete()
        return $this->db->affected_rows(); 
        
    }
    
   
    public function backup_tables($tables = false){
        
        $insert_exclude = array(
            'ci_sessions', 
            'import_csv', 
            'import_csv2'
        );
        
        $this->db->save_queries = false;
        
        if (!$tables) {
            $tables = array(
                'parent_jobs',
                'wips_jobs',
                'wips_jobs_people_accounts',
                'wips_jobs_people_creatives',
                'wips_jobs_people_delivery',
                'wips_jobs_stages',
                'wips_jobs_status',
                'wips_finance_data',
                'wips_resource_requests',
            );
        }
        
        $data = "" .
            "\n--  SQL DB BACKUP " . date("d.m.Y H:i") . " ".
            "\n--  DATABASE: " . $this->db->database .
            "\n--  TABLES: " . implode(', ' ,$tables) . 
            "\n";
    
        foreach($tables as $table){
   
            $data.= "\n". 
                    "\n--  TABLE: " . $table .
                    "\n\n";           
            
            $data.= "DROP TABLE IF EXISTS `". $table . "`;\n";
            
            $query = $this->db->query("SHOW CREATE TABLE `". $table . "`");
            
            $row = array_values($query->row_array()); 
    
            $data.= $row[1].";\n";
            
            $vals = Array(); $z=0;
        
            $query = $this->db->query("SELECT * FROM `" . $table . "`");
            
            foreach ($query->result_array() as $items) {
                $items = array_values($items);
                $vals[$z]="(";
            
                for($j=0; $j<count($items); $j++){
                
                    if (isset($items[$j])) { 
                        $vals[$z].= "'". $this->escape($items[$j]) ."'"; 
                    } 
                    else { 
                        $vals[$z].= "NULL"; 
                    }
                    
                    if ($j<(count($items)-1)){ 
                        $vals[$z].= ","; 
                    }
                }
                
                $vals[$z].= ")"; $z++;
            }    
            
            
            
            if (!in_array($table, $insert_exclude)) {
                $data.= "INSERT INTO `" . $table . "` VALUES ";      
                $data .= "  " . implode(";\nINSERT INTO `" . $table . "` VALUES ", $vals).";\n";
            }
            
        }
        
        return $data; 
 
    }
  
    
    private function escape($value) {
        $return = '';
        for($i = 0; $i < strlen($value); ++$i) {
            $char = $value[$i];
            $ord = ord($char);
            if($char !== "'" && $char !== "\"" && $char !== '\\' && $ord >= 32 && $ord <= 126)
                $return .= $char;
            else
                $return .= '\\x' . dechex($ord);
        }
        return $return;
    }
    
    
    
    
    
    /*
     * Temp functions for updating job numbers from import
     */
    
    // get the data...
    /*
    function get_data_import() {
        // build data import array form single import table in WIPs
        $data_import = array(); 
        
        $this->db->select('*');
        $this->db->from('_job_number_import');
        $query = $this->db->get();
        foreach ($query->result() as $row) {
            if ($row->legacy_job_number) {
                $data_import[$row->legacy_job_number] = array(
                    'uk_job_number' => ($row->uk_job_number ? $row->uk_job_number : NULL),
                    'us_job_number' => ($row->us_job_number ? $row->us_job_number : NULL),
                );
            } 
        }
        return $data_import;
    }
    */
    // update the local table...
    /*
    function temp_import() {
        
        $data_import = $this->get_data_import();
        
        if (count($data_import)) {
            // loop through the data import and update...   
            foreach($data_import as $key => $data) {
                $udata = array(
                    'job_number_alt_uk' => $data['uk_job_number'],
                    'job_number_alt_us' => $data['us_job_number'] 
                );
                $this->db->where('job_number', $key);
                $this->db->update('parent_jobs_numbers', $udata);
            }
        }
        return '<p>DONE! ' . count($data_import) . ' import record processed.</p>';
    }
    */
    
    // get the data...
    /*
    function get_us_data_import() {
        // build data import array form single import table in WIPs
        $data_import = array(); 
        
        $this->db->select('*');
        $this->db->from('_us_job_number_import');
        $query = $this->db->get();
        foreach ($query->result() as $row) {
             $data_import[] = array(
                'uk_job_number' => $row->uk_job_number,
                'us_job_number' => $row->us_job_number,
             );
        }
        return $data_import;
    }
    */
    
    
    // update the local table...
    /*
    function temp_us_import() {
        
        $data_import = $this->get_us_data_import();
        
        if (count($data_import)) {
            // loop through the data import and update...   
            foreach($data_import as $key => $data) {
                $udata = array(
                    'job_number_alt_us' => $data['us_job_number'] 
                );
                $this->db->where('job_number_alt_uk', $data['uk_job_number']);
                $this->db->update('parent_jobs_numbers', $udata);
            }
        }
        return '<p>DONE! ' . count($data_import) . ' import record processed.</p>';
    }
    */
    
    
    
    
}  