<?php
class System_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    /**
     *  Function to return the email settings used throughout this model. 
     */
    public function get_email_config() {
        
        $config = array(); 
        $config['smtp_host'] = "localhost";
        $config['smtp_user'] = "root"; 
        $config['smtp_pass'] = "";
        $config['mailtype'] = "html";
        
        return $config; 
    }
    
     /**  
     *  System WIPs reminder email
     */
    public function send_wips_reminder() {
        
        $email_status = 0;
        $email_to = array('clientserviceswipnotifications@langland.co.uk, digitalpm@langland.co.uk');
        //$email_to = array('paul.silvester@langland.co.uk'); // for testing

        $email_cc = array('james.wheeler@langland.co.uk, kevin.booth@langland.co.uk, scarlett.rushton@langland.co.uk');
        //$email_cc = array('laila.hassani@langland.co.uk'); // for testing
        //$email_cc = false;
        
        $subject = 'Weekly WIP Remider';
        $message = 
                '<p>All,</p>' .
                '<p>Please make sure your WIPs are up to date by end of today.</p>' .
                '<p>Thanks,</p>' .
                '<p>Kevin.</p>';
      
        $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
        $this->email->set_header('Content-type', 'text/html');
        $this->email->set_mailtype("html");
        $this->email->from('noreply@langland-live.co.uk', 'Traffic');
        $this->email->to($email_to);
        if ($email_cc)
            $this->email->cc($email_cc);
        $this->email->bcc('paul.silvester@langland.co.uk');
        $this->email->subject($subject);
        $this->email->message($message);

        if ($this->email->send()) {
            $email_status = 1;
        }
        
        return $email_status;      
    
    }
    
    
    public function update_activity_log($tag, $activity, $uid = false) {
        
        $userdata = $this->session->userdata('user');

        // 2. insert data into wips_jobs and make it the current record
        $data = array(
            'uid' => 0, //$uid ? $uid : $this->session->userdata('urid'), 
            'time' => time(),
            'tag' => $tag,
            'activity' => $activity,
            /* CAPI-fied */
            '_uid' => $uid ? $uid : $this->session->userdata('urid'), 
            '_full_name' => $uid ? '' : $userdata->full_name 
        );
        $this->db->insert('activity_log', $data);
        
        return $this->db->insert_id(); 
        
    }
    
    
}