<?php

define('AUTH_KEY', 'VrLpXi3e3IEq67Li');

class Cron_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }


    public function clear_sessions($key) {
        
        if ($key == AUTH_KEY) {
            $this->db->from('ci_sessions');
            $this->db->truncate();

            $activity = date("d.m.Y h:i:sa") .' : Session table cleared';
            $this->system->update_activity_log('cron', $activity, 999999999);

        }
        else {

            $activity = date("d.m.Y h:i:sa") .' : Session table clear failed - incorrect key';
            $this->system->update_activity_log('cron', $activity, 999999999);
        }      
      
    }
    
}