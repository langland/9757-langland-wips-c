<?php

class Altair_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    
    public function get_parent_job_table_name() {
        return 'parent_jobs_test';
    }
    
    
    public function upload_csv($force = false) {
        
       // $data_validate_terminate = false;
        
        $message = [];
        
        if (!$force) {
            // 1. truncate 'helper' table
            $message[] = $this->empty_altair_csv_table();

            $file = $_FILES['userfile']['tmp_name'];
            $filename = $_FILES['userfile']['name'];

            // 2. load csv data into 'helper' table import_csv
            $sql = "LOAD DATA LOCAL INFILE '" . $file ."'
                    INTO TABLE altair_csv
                    FIELDS TERMINATED by ',' OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\n' IGNORE 1 LINES;";
            $query = $this->db->query($sql);

            // 3. upload successful...  
            if (!$query) { // ... no
                $message[] = "<span class='error'>ERROR: There was a problem uploading the CSV.</span>";
                $message[] = "<span class='error'>Import process TERMINATED</span>"; 
                $message[] =  "<span class='error'>Please check the CSV, try again and contact dev team if this problem persists.</span>";  
                $this->return_to_upload_page($message);
            } 
            else {
            /// ... yes
                $message[] = "CSV uploaded successfully to temporary helper table.";
            }
            
        }
        else {
            $message[] = "Upload table cleared";
            $message[] = "CSV uploaded successfully to temporary helper table.";
            
        }
        
       
        // 4. validate:  rows > 0... 
        $query = $this->db->query('SELECT * FROM altair_csv');
        $num_rows = $query->num_rows();
        if ($num_rows == 0) { // ... no
            $message[] = "<span class='error'>ERROR: There were no records uploaded from the CSV.</span>";
            $message[] = "<span class='error'>Import process TERMINATED</span>"; 
            $message[] =  "<span class='error'>Please check the CSV, try again and contact dev team if this problem persists.</span>";  
            $this->return_to_upload_page($message);
        } 
        // ... yes
        
        // 5. validate: values in col 'S' ....
        $query = $this->db->query("SELECT * FROM altair_csv WHERE NOT S =''");
        $num_S_rows = $query->num_rows();
        if ($num_S_rows > 0) { // ... no 
            $message[] = "<span class='error'>ERROR: <strong>" . $num_S_rows . "</strong> records were found in the CSV with extra columns of data.</span>";  
            $message[] = "<span class='error'>Import process TERMINATED</span>"; 
            $message[] =  "<span class='error'>Please check the CSV, try again and contact dev team if this problem persists.</span>";  
            $this->return_to_upload_page($message); 
        }
        // ... yes
        else {
            $message[] = "Validation: <strong>" . $num_rows . "</strong> rows successfully uploaded from CSV to helper table.";
        }    
         // 13. clean up: remove any blank/empty rows
        $blank_rows = $this->clean_altair_csv_table_blank_rows(); 
        if ($blank_rows) {
            $message[] = $blank_rows;
        }
            
         // now exclude any records using config exclusion array
        $exclude_list = $this->config->item('import_exclude');
        if (isset($exclude_list) && count($exclude_list)) {
            $this->db->where_in('A', $exclude_list);
            $this->db->delete('altair_csv');
            $message[]  = "Clean Data: <strong>" . $this->db->affected_rows() . "</strong> records removed based on exclusion list: " . implode('; ', $exclude_list);
        }
        
        $this->db->query("UPDATE  altair_csv SET  V = 1");
        
//        if ($data_validate) {
        
        // 6. validate: job numbers in "A' .... TO DO
            $query = $this->db->query("SELECT * FROM altair_csv WHERE A NOT LIKE 'B3236%' AND A NOT LIKE 'B1133%' "
                    . "AND A NOT LIKE 'X3236%' AND A NOT LIKE 'X1133%' "
                    . "AND A NOT LIKE 'N3236%' AND A NOT LIKE 'N1133%' "
                  //  . "AND A NOT LIKE 'G3236%' AND A NOT LIKE 'G1133%' "
                  //  . "AND A NOT LIKE 'T3236%' AND A NOT LIKE 'T1133%'"
                    . "AND A NOT LIKE 'C3236%' AND A NOT LIKE 'C1133%' "
                    . "AND A NOT LIKE 'H3236%' AND A NOT LIKE 'H1133%'"
                    );
            $num_A_rows = $query->num_rows();
            if ($num_A_rows > 0) { // ... no 
                foreach ($query->result() as $row) {
                    $vmess_a[] = $row->A . ($row->B ? ' - ' . $row->B : '');
                }
                $message[] = "<span class='error'>Validation ERROR: <strong>" .  $num_A_rows . "</strong> CSV records were found to have invalid job numbers in column 'Job/Sub-Job ID':" 
                    . '<strong><ul><li>' .  implode('</li><li>', $vmess_a) . '</li></ul></strong></span>'   
                    . "<span class='error'>Import process TERMINATED</span><br />"    
                    . "<span class='error'>Please check the CSV, try again and contact dev team if this problem persists.</span>";  
                $this->return_to_upload_page($message); 
            }
            else {
            // ... valid
                $message[] = "Validation: Job numbers in 'Job/Sub-Job ID' all validated as correctly formatted.";
            }

            // 7. validate: job names in 'B' .... TO DO
            $query = $this->db->query("SELECT * FROM altair_csv WHERE B = '0' OR LENGTH(B) = 0");
            $num_B_rows = $query->num_rows();
            if ($num_B_rows > 0) { // ... no 
                $vmess_a = array();
                $vmess = "<span class='error'>Validation ERROR: <strong>" .  $num_B_rows . "</strong> CSV records were found with invalid job names in column 'Job/Sub-Job Name': ";
                foreach ($query->result() as $row) {
                    $vmess_a[] = $row->A . ' - Job name: ' . (trim($row->B) ? $row->B : '<i>blank</i>');
                    $this->db->query("UPDATE  altair_csv SET  V = 0 WHERE A = '" . $row->A . "'");
                }
                $message[] = $vmess . '<strong><ul><li>' .  implode('</li><li>', $vmess_a) . '</li></ul></strong></span>';
              //  $message[] = "Import process TERMINATED";      
              //  $message[] =  "Please check the CSV, try again and contact dev team if this problem persists.";  
              //  $this->return_to_upload_page($message); 
            }
            else {
            // ... valid
                $message[] = "Validation: job names in 'Job/Sub-Job Name' all validated as correctly formatted.";
            }
           
            // 8. validate: leagacy job numbers or blanks in 'C' ....
           // $query = $this->db->query("SELECT * FROM altair_csv WHERE LENGTH(C) != 0 AND LENGTH(C) != 5 AND LENGTH(C) != 6 AND C != ' '");
            $query = $this->db->query("SELECT * FROM altair_csv WHERE TRIM(C) != '' AND (LENGTH(TRIM(C)) > 6 OR LENGTH(TRIM(C)) < 4)");
            $num_C_rows = $query->num_rows();
            if ($num_C_rows > 0) { // ... no 
                $vmess_a = array();
                $vmess = "<span class='error'>Validation ERROR: <strong>" .  $num_C_rows . "</strong> CSV records were found with invalid legacy job numbers in column 'Legacy Job': ";
                foreach ($query->result() as $row) {
                    $vmess_a[] = $row->A . ' - Legacy number: ' . (trim($row->C) ? $row->C : '<i>blank</i>');
                    $this->db->query("UPDATE  altair_csv SET  V = 0 WHERE A = '" . $row->A . "'");
                }
                $message[] = $vmess . '<strong><ul><li>' .  implode('</li><li>', $vmess_a) . '</li></ul></strong></span>';
               // $message[] = "Import process TERMINATED";      
                //$message[] =  "Please check the CSV, try again and contact dev team if this problem persists.";  
               // $this->return_to_upload_page($message); 
            }
            else {
                // ... valid
                $message[] = "Validation: legacy job numbers in 'Legacy Job' all validated as correctly formatted (or blank).";
            }
            
            // 9. validate: 'active' or 'inactive' status in 'D' .... 
            $query = $this->db->query("SELECT * FROM altair_csv WHERE D != 'Active' AND D != 'Inactive'");
            $num_D_rows = $query->num_rows();
            if ($num_D_rows > 0) { // ... no 
                $vmess_a = array();
                $vmess = "<span class='error'>Validation ERROR: <strong>" .  $num_D_rows . "</strong> CSV records were found with invalid status strings in column 'Status': "; 
                foreach ($query->result() as $row) {
                    $vmess_a[] = $row->A . ' - Status: ' . (trim($row->D) ? $row->D : '<i>blank</i>');
                    $this->db->query("UPDATE  altair_csv SET  V = 0 WHERE A = '" . $row->A . "'");
                }
                $message[] = $vmess . '<strong><ul><li>' .  implode('</li><li>', $vmess_a) . '</li></ul></strong></span>';
            // $message[] = "Import process TERMINATED";      
              //  $message[] =  "Please check the CSV, try again and contact dev team if this problem persists.";  
               // $this->return_to_upload_page($message); 
            }
            else {
            // ... valid
                $message[] = "Validation: status strings in 'Status' all validated as correct.";
            }

            // 10. validate: 'CTR', 'ETH', 'COR' or 'CON' in 'E' .... TO DO
            $query = $this->db->query("SELECT * FROM altair_csv WHERE E != 'CTR' AND E != 'COR' AND E != 'CON' AND E != 'ETH' AND E != 'INT'");
            $num_E_rows = $query->num_rows();
            if ($num_E_rows > 0) { // ... no 
                $vmess_a = array();
                $vmess = "<span class='error'>Validation ERROR: <strong>" .  $num_E_rows . "</strong> CSV records were found with with invalid division values in column 'Division': ";
                foreach ($query->result() as $row) {
                    $vmess_a[] = $row->A . ' - Division: ' . (trim($row->E) ? $row->E : '<i>blank</i>');
                    $this->db->query("UPDATE  altair_csv SET  V = 0 WHERE A = '" . $row->A . "'");
                }
                $message[] = $vmess . '<strong><ul><li>' .  implode('</li><li>', $vmess_a) . '</li></ul></strong></span>';
//                $message[] = "Import process TERMINATED";      
  //              $message[] =  "Please check the CSV, try again and contact dev team if this problem persists.";  
               // $this->return_to_upload_page($message); 
            }
            else {
            // ... valid
                $message[] = "Validation: division values in 'Division' all validated as correct.";
            }
            
            // 11. validate: strings in 'F', 'G' and 'H' .... TO DO
            $query = $this->db->query("SELECT * FROM altair_csv WHERE LENGTH(F) = 0 OR LENGTH(G) = 0 OR LENGTH(H) = 0");
            $num_F_rows = $query->num_rows();
            if ($num_F_rows > 0) { // ... no 
                $vmess_a = array();
                $vmess = "<span class='error'>Validation ERROR: <strong>" . $num_F_rows . "</strong> CSV records found where 'Client', 'Brand' or 'Job Responsible' columns are blank: ";  
                foreach ($query->result() as $row) {
                    $vmess_a[] = $row->A . ' - Client: ' . (trim($row->F) ? $row->F : '<i>blank</i>')
                            . ' - Brand: ' . (trim($row->G) ? $row->G : '<i>blank</i>')
                            . ' - Responsible: ' . (trim($row->H) ? $row->H : '<i>blank</i>');
                    $this->db->query("UPDATE  altair_csv SET  V = 0 WHERE A = '" . $row->A . "'");
                }
                $message[] = $vmess . '<strong><ul><li>' .  implode('</li><li>', $vmess_a) . '</li></ul></strong></span>';
//                $message[] = "Import process TERMINATED";      
  //              $message[] =  "Please check the CSV, try again and contact dev team if this problem persists.";  
               // $this->return_to_upload_page($message); 
            } else {
            // ... valid
                $message[] = "Validation: 'Client', 'Brand' or 'Job Responsible' values all validated as correct.";
            }
            
            // 12. validate: dates formatted 'dd/mm/yyyy' in  I and J .... TO DO
            $query = $this->db->query("SELECT * FROM altair_csv WHERE LENGTH(I) != 10 OR I = '00/01/1900'");
            $num_I_rows = $query->num_rows();
            if ($num_I_rows > 0) { // ... no 
                $vmess_a = array();
                $vmess = "<span class='error'>Validation ERROR: <strong>" . $num_I_rows . "</strong> CSV records found where 'Created At' value is not a valid date. ";
                
                foreach ($query->result() as $row) {
                    $vmess_a[] = $row->A . ' - Created At: ' . (trim($row->I) ? $row->I : '<i>blank</i>');
                    $this->db->query("UPDATE  altair_csv SET  V = 0 WHERE A = '" . $row->A . "'");
                }
                $message[] = $vmess . '<strong><ul><li>' .  implode('</li><li>', $vmess_a) . '</li></ul></strong></span>';
                
                //$message[] = "Import process TERMINATED";      
                //$message[] =  "Please check the CSV, try again and contact dev team if this problem persists.";  
              //  $this->return_to_upload_page($message); 
            }
            else {
                // ... valid
                $message[] = "Validation: 'Created At' date values validated.";
            }

            $query = $this->db->query("SELECT * FROM altair_csv WHERE LENGTH(J) != 10 OR J = '00/01/1900'");
            $num_J_rows = $query->num_rows();
            if ($num_J_rows > 0) { // ... no 
                $vmess_a = array();
                $vmess = "<span class='error'>Validation ERROR: <strong>" .  $num_J_rows . "</strong> CSV records found where 'Changed At' value is not a valid date. ";
                
                foreach ($query->result() as $row) {
                    $vmess_a[] = $row->A . ' - Changed At: ' . (trim($row->J) ? $row->J : '<i>blank</i>');
                    $this->db->query("UPDATE  altair_csv SET  V = 0 WHERE A = '" . $row->A . "'");
                }
                $message[] = $vmess . '<strong><ul><li>' .  implode('</li><li>', $vmess_a) . '</li></ul></strong></span>';
                
                //$message[] = "Import process TERMINATED";      
                //$message[] =  "Please check the CSV, try again and contact dev team if this problem persists.";  
              //  $this->return_to_upload_page($message); 
            } else {
              // ... valid
                $message[] = "Validation: 'Changed At' date values validated.";
            }
 //       }
       
        // check invalid records....
        $query = $this->db->query("SELECT * FROM altair_csv WHERE V = 0");  
        $num_V_rows = $query->num_rows();   
        
        if ($num_V_rows > 0 && !$force) {

            $message[] = "<span class='error'><strong>Validation FAILED: a total of " . $num_V_rows . " CSV records have failed validation.</strong></span>"
                    . "<br />-------------------------------------------------------------------------------------------------------------------------------------<br />"
                    . "<strong>If you choose to proceed then these records will be excluded from the import/update process. <br/><a href='/admin/altair_import/force_upload'>Proceed with Import</a></strong>"
                    . "<br />-------------------------------------------------------------------------------------------------------------------------------------<br />"
                    . "Alternatively fix these issues within the CSV and re-upload the file to try again. <a href='/admin/altair_import'>Reset and try again</a><br /> ";

            // log process...
            $log_data = array(
                'datetime' => time(),
                'method' => 'manual upload',
                'type' => 'altair import',
                'summary' => '<ol><li>' . implode('</li><li>', $message) . '</li></ul>',
            );
            $this->db->insert('import_history', $log_data);
            
            $this->return_to_upload_page($message);    
        }
        else {
            
            if ($num_V_rows > 0) {
                $message[] = "<span class='error'>Validation FAILED: a total of <strong>" . $num_V_rows . "</strong> CSV records have failed validation. These records will be ignored during processing.</span>";
            }
            
            $message2 = $this->upload_csv_step2();
            
            $full_message = array_merge($message, $message2);
            
            // log process...
            $log_data = array(
                'datetime' => time(),
                'method' => 'manual upload',
                'type' => 'altair import',
                'summary' => '<ol><li>' . implode('</li><li>', $full_message) . '</li></ul>',
            );
            $this->db->insert('import_history', $log_data);
            
            $this->return_to_upload_page($full_message);    
            
        }
        
        
        
    }
    
    
    public function upload_csv_step2() {
        
        $pjtable = $this->get_parent_job_table_name();
        
        $message = array();
            // 14. clean up: clean finance data columns 
        // - remove commas in values
        // - replace '-' with value 0
        // - remove any pound signs
        $message[] = $this->clean_altair_csv_table_finance();
        //return $message;
        // 15. derive unix timestamps for created (I) and updated (J) date fields and save in
        // 'S' and 'T' to aid in processing later
        $message[] = $this->set_altair_csv_unix_dates();
        
        // 16. validate: check thereare still rows remaining after clean up ... 
        $query = $this->db->query('SELECT * FROM altair_csv WHERE V = 1');
        $num_rows = $query->num_rows();
        if ($num_rows == 0) { // ... no
            $message[] = "<span class='error'>ERROR: no valid records remaining following data clean up and formatting process.</span>"; 
            $message[] = "<span class='error'>Import process TERMINATED</span>";      
            $message[] =  "<span class='error'>Please check the CSV, try again and contact dev team if this problem persists.</span>";  
            $this->return_to_upload_page($message);
        } 
        $message[] = "Validation: <strong>" . $num_rows . "</strong> valid, clean and formatted workable rows ready to be processed.";
        
        
        // back up before we do anything else....
        $mybackup = $this->backup_table($pjtable);
        $backupfilenameJobName =  date('Ymd_His') . '_' . $pjtable . '.sql';
        $backupfilenameJob = $_SERVER['DOCUMENT_ROOT'] . '/_bups/' . $backupfilenameJobName;
        $handle = fopen($backupfilenameJob, 'w' );
        fwrite($handle,$mybackup);
        fclose($handle);

        $mybackup = $this->backup_table('parent_jobs_numbers');
        $backupfilenameNumbersName = date('Ymd_His') . '_parent_jobs_numbers.sql';
        $backupfilenameNumber = $_SERVER['DOCUMENT_ROOT'] . '/_bups/' . $backupfilenameNumbersName;
        $handle = fopen($backupfilenameNumber, 'w' );
        fwrite($handle,$mybackup);
        fclose($handle);
        
        $mybackup = $this->backup_table('wips_finance_data_altair');
        $backupfilenameFinanceName = date('Ymd_His') . '_wips_finance_data_altair.sql';
        $backupfilenameFinance = $_SERVER['DOCUMENT_ROOT'] . '/_bups/' . $backupfilenameFinanceName;
        $handle = fopen($backupfilenameFinance, 'w' );
        fwrite($handle,$mybackup);
        fclose($handle);

        $this->db->insert('import_baks', array(
            'datetime' => time(),
            'uid' => $this->session->userdata('urid'),
            'jobs' => $backupfilenameJobName,
            'numbers' => $backupfilenameNumbersName,
            'finance' => $backupfilenameFinanceName,
        ));
        
        $message[] = "Back Up: Jobs and Finance tables successfully backed up.";
       
        
        // 17. get the related WIPS record Job ID based on job numbers, and insert into column 'U' in helper table
        // 18. this function should insert any that don't have job numbers, and log/report this...
        $message[] = $this->get_jobs_pjid(false);
        
        // 19. back up tables before processing begins
        
        // 20 - 28. process the data....
        // this will now do the job data and the financials...
        $message[] = $this->process_alt_data();
        
        // 29. empty the 'helper' table ready for next import
        // $message[] = $this->empty_altair_csv_table();
        
        
        
        
        
        return $message;
//        $this->return_to_upload_page($message);
        
        
        
    }
    
    
    
    public function process_alt_data() {
        
        $message = "";
        
        $jobs_updated = 0;
        $finance_updated = 0;
        $finance_inserted = 0;
        
        // loop through each row in altair_csv
        
        // Job data based on pjid see if record needs updating...  IF altair.csv.T != parent_jobs.alt_updated
        
        // if does not need updating then leave it
        
        // if does need updating then update the relevant fields...
        
        // based on parent_jobs.id = altair_csv.U
        // parent_jobs.number = altair_csv.C
        // parent_jobs.title = altair_csv.B
        // parent_jobs.sector = altair_csv.E (derived value)
        // parent_jobs.job_open = altair_csv.D (derived value)
        // parent_jobs.alt_client = altair_csv.F
        // parent_jobs.alt_brand = altair_csv.G
        // parent_jobs.alt_ah = altair_csv.H
        // parent_jobs.alt_created = altair_csv.S
        // parent_jobs.alt_updated = altair_csv.T
        
        // based on parent_jobs_number.pjid = altair_csv.U
        // parent_jobs_number.search_title = altair_csv.B
        // parent_jobs_number.job_number = altair_csv.C
        // parent_jobs_number.search_title = altair_csv.B
        
        
        // Finace data based on pjid see if record needs updating...  
        
        // IF no record OR altair.csv.T != wips_finance_data_altair.updated
        
        // based on wips_finance_data_altair.pjid = altair_csv.U
        // wips_finance_data_altair.enc_total_costs = altair_csv.M (encrypted)
        // wips_finance_data_altair.enc_agency_time_costs = altair_csv.K (encrypted)
        // wips_finance_data_altair.enc_third_party_bought_in = altair_csv.L (encrypted)
        // wips_finance_data_altair.third_party_committed = altair_csv.N (encrypted)
        // wips_finance_data_altair.enc_invoiced = altair_csv.Q (encrypted)
        // wips_finance_data_altair.updated = altair_csv.T
        
        
        // just worry about updates for now....
        $this->db->select('
                ac.A as alt_jn_uk,
                ac.B as alt_job_name, 
                ac.C as alt_legacy_jn,
                ac.D as alt_status,
                ac.E as alt_division,
                ac.F as alt_client,
                ac.G as alt_brand,
                ac.H as alt_ah,
                ac.S as alt_created, 
                ac.T as alt_updated, 
                ac.U as alt_pjid, 
                
                ac.K as alt_agency_time,
                ac.L as alt_third_party_in,
                ac.N as alt_third_party_committed,
                ac.M as alt_costs_to_date,
                ac.Q as alt_invoiced, 

                pj.title as pj_title,
                pj.number as pj_number,
                pj.sector as pj_sector,
                pj.job_open as pj_job_open,
                pj.alt_client as pj_alt_client,
                pj.alt_brand as pj_alt_brand,
                pj.alt_ah as pj_alt_ah,
                pj.alt_created as pj_alt_created,
                pj.alt_updated as pj_alt_updated
        ');
        $this->db->from('altair_csv ac');
        $this->db->join($this->get_parent_job_table_name() . ' pj', 'pj.id = ac.U');
        $this->db->where('ac.U !=', 0);
        $this->db->where('ac.U !=', '');
        $this->db->order_by('ac.U', 'ASC');
        
        $query = $this->db->get();
        
//        $sql = "SELECT * FROM altair_csv WHERE U != 0 && U != ''";
//        $query = $this->db->query($sql);

        foreach ($query->result() as $row) {
      
            $pjid = $row->alt_pjid;
            
            $alt_jn_uk = $row->alt_jn_uk;
            $alt_job_name = $row->alt_job_name;
            $alt_legacy_jn = $row->alt_legacy_jn;
            $alt_opened = 1; //$row->alt_status == 'Active' ? 1 : 0;
            $alt_division = $this->get_division_id($row->alt_division);
            $alt_client = $row->alt_client;
            $alt_brand = $row->alt_brand;
            $alt_ah = $row->alt_ah;
            $alt_created = $row->alt_created;
            $alt_updated = $row->alt_updated ? $row->alt_updated : 0;
            
            $alt_agency_time = $row->alt_agency_time;
            $alt_third_party_in = $row->alt_third_party_in;
            $alt_third_party_committed = $row->alt_third_party_committed;
            $alt_costs_to_date = $row->alt_costs_to_date;
            $alt_invoiced = $row->alt_invoiced;
            
            $pj_title = $row->pj_title;
            $pj_number = $row->pj_number;
            $pj_sector = $row->pj_sector;
            $pj_job_open = $row->pj_job_open;
            
            $pj_alt_client = $row->pj_alt_client;
            $pj_alt_brand = $row->pj_alt_brand;
            $pj_alt_ah = $row->pj_alt_ah;
            $pj_alt_created = $row->pj_alt_created;
            $pj_alt_updated = $row->pj_alt_updated;
            
          //  $wf_updated = $row->wf_updated;
            
            if ($pj_alt_updated < $alt_updated) { 
                // do an update to parent_jobs table...
                //print $pjid . ' - au:' . $alt_updated . ' - pjau:' . $pj_alt_updated . ' - title:' . $pj_title . '<br />';
                
                // update the parent_jobs_table...
                $udata = array(
                    'number' => $alt_legacy_jn,
                    'title' => $alt_job_name, 
                    'sector' => $alt_division,
                   // 'job_open' => $alt_opened,
                    'alt_client' => $alt_client,
                    'alt_brand' => $alt_brand,
                    'alt_ah' => $alt_ah,
                    'alt_created' => $alt_created,
                    'alt_updated' => $alt_updated
                );
                
                $this->db->where('id', $pjid);
                $this->db->update($this->get_parent_job_table_name(), $udata);
                
             //   $message .= $pjid . ' '; 
                $jobs_updated++;
                
                
                
                
                 // check for record in finance table and update if required
                
                
            }
              
            $finance_update_insert = $this->check_update_insert_finance_record($pjid, $alt_updated);
            
            if ($finance_update_insert) {
            
                $fdata = array(
                    'pj_id' =>$pjid, 
                    'number' => $alt_legacy_jn,
                    'enc_total_costs' => $this->wips->wips_encrypt($alt_costs_to_date),
                    'enc_agency_time_cost'  => $this->wips->wips_encrypt($alt_agency_time),
                    'enc_third_party_bought_in' => $this->wips->wips_encrypt($alt_third_party_in),
                    'enc_third_party_committed' => $this->wips->wips_encrypt($alt_third_party_committed),
                    'enc_invoiced' => $this->wips->wips_encrypt($alt_invoiced),
                    'enc_estimated' => 0,
                    'updated' => $alt_updated,
                    'total_costs' => $alt_costs_to_date,
                    'agency_time_cost' => $alt_agency_time,
                    'third_party_bought_in' => $alt_third_party_in,
                    'third_party_committed' => $alt_third_party_committed,
                    'invoiced' => $alt_invoiced,
                    'estimated' => 0,
                    'reftotal' => 0
                );
                
                if ($finance_update_insert == 'insert') {

                    $this->db->insert('wips_finance_data_altair', $fdata);
                    
                    $finance_inserted++;

                }
                else if ($finance_update_insert == 'update') {

                    unset($fdata['pj_id']);    
                    $this->db->where('pj_id', $pjid);
                    $this->db->update('wips_finance_data_altair', $fdata);
                    
                    $finance_updated++;
                }
                
            }
              
                
              
            
           // $message .= $pjid . ' - ' . $alt_updated . '<br />';
         //   $message .= $pjid.$alt_updated . '<br />';
            
            
            
    
            
        }
        
  /*      
        $this->db->select('pj.id, pj.number, pj.title');
        $this->db->from('parent_jobs pj');
        $this->db->where('pj.parent_number', $pjnum);
        $this->db->where('pj.number !=', $pjnum);
        $this->db->order_by('pj.number', 'ASC');
        
        $query = $this->db->get();
        
        foreach ($query->result() as $row) {
            
            $child_jobs[] = array(
                'id' => $row->id,
                'number' => $row->number,
                'title' => $row->title,
            );
         }
         
         return $child_jobs;
        */
        
        $message .= "Processing summary: <ul>"; 
        $message .= "<li><strong>" . $jobs_updated . "</strong> job records updated.</li>";
        $message .= "<li><strong>" . $finance_inserted . "</strong> finance records inserted.</li> ";
        $message .= "<li><strong>" . $finance_updated . "</strong> finance records updated.</li>";
        $message .= "</ul>----------------------------<br />";
        //$message .= "<a href='/admin/altair_import'>Reset and run another import</a><br />";
        
        return $message;
        
    }
    
    
    public function check_update_insert_finance_record($pjid, $alt_updated) {
        
        $this->db->select('*');
        $this->db->from('wips_finance_data_altair');
        $this->db->where('pj_id', $pjid);
        
        $query = $this->db->get();
        $row = $query->row(); 
        
        
        if ($row) {
            
            if ($row->updated < $alt_updated) {
                return 'update';
            }
            else 
                return false;
            
        }
        else 
            return 'insert';
        
        
        
        
    }
    
        
    public function get_division_id($sector) {
        
        $sid = 5; // default to OTHER...

        switch ($sector) {
            
            case 'COR':
                $sid = 1;
                break;
            
            case 'CON':
                $sid = 2;
                break;
            
            case 'CTR':
                $sid = 3;
                break;
            
            case 'ETH':
                $sid = 4;
                break;
            
            default:
                $sid = 5;
                break;
            
        }
        
        return $sid;
    }
    
    
    public function get_jobs_pjid($insert = true) {
        
        $message = "";
        $rejected = array();
        $inserted = array();
        $found = array();
        
        
        
        // find the records...
        //$sql = "SELECT * FROM altair_csv ac";
       
        $this->db->select('
                ac.A as alt_jn_uk,
                ac.B as alt_job_name, 
                ac.C as alt_legacy_jn,
                ac.D as alt_status,
                ac.E as alt_division,
                ac.F as alt_client,
                ac.G as alt_brand,
                ac.H as alt_ah,
                ac.S as alt_created, 
                ac.T as alt_updated, 
                ac.U as alt_pjid, 
                
                ac.K as alt_agency_time,
                ac.L as alt_third_party_in,
                ac.N as alt_third_party_committed,
                ac.M as alt_costs_to_date,
                ac.Q as alt_invoiced 
        ');
         $this->db->from('altair_csv ac');
         $this->db->where('ac.V', 1);
         $query = $this->db->get();

        // loop through and update...
        foreach ($query->result() as $row) {

            $alt_jn_uk = $row->alt_jn_uk;
            $alt_full_desc = $row->alt_jn_uk . ' - ' . $row->alt_job_name; // . ' - ' . $row->alt_division . ' - ' . $row->alt_client . ' - ' . $row->alt_brand;
            $alt_legacy_jn = $row->alt_legacy_jn;
            
            $pjid = $this->get_pjid($alt_jn_uk, $alt_legacy_jn);
            
            if ($pjid) {
                $sql2 = "UPDATE altair_csv SET U = " . $pjid . " WHERE A = '" . $alt_jn_uk . "'";
                $query2 = $this->db->query($sql2);
                $found[] = $alt_jn_uk;
            }
            else { // 
                // we need to insert here and get the pjid....
                // 
                // insert into parent_jobs AND parents_jobs_numbers, retrieve the 
                // 
                // if active...
                if ($insert && $row->alt_job_name != '0') {
                    
                    $now = time();
                    // insert into parent_jobs 
                    $idata = array(
                        'number' => $row->alt_legacy_jn ? $row->alt_legacy_jn : '',
                        'type' => 1,
                        'title' => $row->alt_job_name,
                        'client' => '',
                        'account_handler_id' => 0,
                        'digital' => 0,
                        'sector' => $this->get_division_id($row->alt_division),
                        'access_created' => '',
                        'access_modified' => '',
                        'record_added' => $now,
                        'record_added' => $now,
                        'job_open' => 1, //$row->alt_status == 'Active' ? 1 : 0,
                        'enc_brand' => $this->wips->wips_encrypt($row->alt_brand),
                        'enc_account_handler' => $this->wips->wips_encrypt($row->alt_ah),
                        'alt_client' => $row->alt_client,
                        'alt_brand' => $row->alt_brand,
                        'alt_ah' => $row->alt_ah,
                        'alt_created' => $row->alt_created,
                        'alt_updated' => $row->alt_updated
                    );
                    $this->db->insert($this->get_parent_job_table_name(), $idata);
                    $pjid = $this->db->insert_id(); 
                    
                    // Insert into parents_jobs_numbers
                    $idata2 = array(
                        'pjid' => $pjid,
                        'search_title' => $row->alt_job_name,
                        'job_number' => $row->alt_legacy_jn ? $row->alt_legacy_jn : '',
                        'job_number_alt' => NULL,
                        'job_number_alt_uk' => $alt_jn_uk,
                        'job_number_alt_us' => NULL,
                        'jbjid' => 0,
                    ); 
                    $this->db->insert('parent_jobs_numbers', $idata2);
                    
                    // update altair_csv
                    $sqlu = "UPDATE altair_csv SET U = " . $pjid . " WHERE A = '" . $alt_jn_uk . "'";
                    $queryu = $this->db->query($sql2);
                    
                    $inserted[] = $alt_jn_uk . ' [pjid: ' . $pjid . '] ';                    
                    
                }
                else {
                    $rejected[] = $alt_full_desc; //$alt_jn_uk;
                }
            }
        }
        
        $message =  "Parent Job (WIPS) ID search matched <strong>" . count($found) . "</strong> records.";
        
        if (count($rejected) > 0) {
            $message .= "<br /><span class='error'>CSV records which could not be matched in WIPS so ignores: <strong>" . count($rejected) . "</strong><strong><ul><li>";
            $message .= implode('</li><li>', $rejected) . '</li></ul></strong></span>';
        }

        if (count($inserted) > 0) {
            $message .= "<br />Could not be found so Inserted: <strong>" . count($inserted) . "</strong><ul><li>";
            $message .=  implode('</li><li>', $inserted) . '</li></ul>';
        }

        
        return $message;
        
        
    }
    
    
    public function get_pjid($uk_jn, $leg_jn) {
        
        if ($uk_jn) {
            $sql = "SELECT pjid FROM parent_jobs_numbers WHERE job_number_alt_uk = '" . $uk_jn . "'";  
        }
        else if ($leg_jn) {
            $sql = "SELECT pjid FROM parent_jobs_numbers WHERE job_number = '" . $leg_jn . "'"; 
        }
        else {
            return false;
        }
        
        $query = $this->db->query($sql);
        $result = $query->row();
       
        if ($result) {
            return $result->pjid;
        }
        else {
            return false;
        }
        
        
    }
    
    
    
    public function set_altair_csv_unix_dates() {
        
        $sql = "UPDATE altair_csv SET S = UNIX_TIMESTAMP(STR_TO_DATE(I, '%d.%m.%Y')), T = UNIX_TIMESTAMP(STR_TO_DATE(J, '%d.%m.%Y'))";
        
        $query = $this->db->query($sql);
        
        return "Formatting: dates converted to UNIX timestamp.";
    }
    
    
    public function clean_altair_csv_table_finance() {
        
        $sql = "UPDATE altair_csv SET K = REPLACE(K, '-', 0), "
                . "L = REPLACE(L, ' - ', 0),"
                . "M = REPLACE(M, ' - ', 0),"
                . "N = REPLACE(N, ' - ', 0),"
                . "O = REPLACE(O, ' - ', 0),"
                . "P = REPLACE(P, ' - ', 0),"
                . "Q = REPLACE(Q, ' - ', 0),"
                . "R = REPLACE(R, ' - ', 0)";
              
        $query = $this->db->query($sql);
        
        $sql = "UPDATE altair_csv SET K = REPLACE(K, '£', ''), "
                . "L = REPLACE(L, '£', ''),"
                . "M = REPLACE(M, '£', ''),"
                . "N = REPLACE(N, '£', ''),"
                . "O = REPLACE(O, '£', ''),"
                . "P = REPLACE(P, '£', ''),"
                . "Q = REPLACE(Q, '£', ''),"
                . "R = REPLACE(R, '£', '')";
              
        $query = $this->db->query($sql);
        
         $sql = "UPDATE altair_csv SET K = REPLACE(K, ',', ''), "
                . "L = REPLACE(L, ',', ''),"
                . "M = REPLACE(M, ',', ''),"
                . "N = REPLACE(N, ',', ''),"
                . "O = REPLACE(O, ',', ''),"
                . "P = REPLACE(P, ',', ''),"
                . "Q = REPLACE(Q, ',', ''),"
                . "R = REPLACE(R, ',', '')";
              
        $query = $this->db->query($sql);
        
        return 'Formatting: dashes replaced with zeroes and commas removed.';
        
    }
    
    
    
    public function clean_altair_csv_table_blank_rows() {
        
        
        $query = $this->db->query("SELECT * FROM altair_csv WHERE A ='0'");
        $num_D_rows = $query->num_rows();
        
        if ($num_D_rows) {
            $sql = "DELETE FROM altair_csv WHERE A = '0'";
            $query = $this->db->query($sql);
            return "Clean Data: <strong>" . $num_D_rows . "</strong> blank row deleted"; 
            
        }
        
        return false;
        
    }
    
    
    public function return_to_upload_page($message) {
        
        $import_message = "";
        
      //  for($i=0; $i<count($message); $i++) {
       //     $import_message .= ($i+1). ". " . $message[$i] . '<br />';
       // }
        
        $import_message .= "<ol><li>" . implode("</li><li>", $message) . "</li></ol>";
        
        
        $this->session->set_flashdata('import-message', $import_message);
        redirect('../admin/altair_import', 'refresh');
        
        
    }
    
     
    /**
     * empty_import_csv_table
     * 
     * Empty import 'helper' table
     */
    public function empty_altair_csv_table() {
        
        $this->db->truncate('altair_csv');
        
        return 'Upload table cleared'; 
        
    }
    
    
    
    
    public function backup_table($table){
        
        $this->db->save_queries = false;
        
        $data = "" .
            "\n--  SQL DB BACKUP " . date("d.m.Y H:i") . " ".
            "\n--  DATABASE: " . $this->db->database .
            "\n--  TABLES: " . $table . 
            "\n";
    
    //    foreach($tables as $table){
   
            $data.= "\n". 
                    "\n--  TABLE: " . $table .
                    "\n\n";           
            
            $data.= "DROP TABLE IF EXISTS `". $table . "`;\n";
            
            $query = $this->db->query("SHOW CREATE TABLE `". $table . "`");
            
            $row = array_values($query->row_array()); 
    
            $data.= $row[1].";\n";
            
            $vals = Array(); $z=0;
        
            $query = $this->db->query("SELECT * FROM `" . $table . "`");
            
            foreach ($query->result_array() as $items) {
                $items = array_values($items);
                $vals[$z]="(";
            
                for($j=0; $j<count($items); $j++){
                
                    if (isset($items[$j])) { 
                        $vals[$z].= "'". $this->escape($items[$j]) ."'"; 
                    } 
                    else { 
                        $vals[$z].= "NULL"; 
                    }
                    
                    if ($j<(count($items)-1)){ 
                        $vals[$z].= ","; 
                    }
                }
                
                $vals[$z].= ")"; $z++;
            }    
            
            
            
          //  if (!in_array($table, $insert_exclude)) {
                $data.= "INSERT INTO `" . $table . "` VALUES ";      
                $data .= "  " . implode(";\nINSERT INTO `" . $table . "` VALUES ", $vals).";\n";
          //  }
            
    //    }
        
        return $data; 
 
    }
  
    
    private function escape($value) {
        $return = '';
        for($i = 0; $i < strlen($value); ++$i) {
            $char = $value[$i];
            $ord = ord($char);
            if($char !== "'" && $char !== "\"" && $char !== '\\' && $ord >= 32 && $ord <= 126)
                $return .= $char;
            else
                $return .= '\\x' . dechex($ord);
        }
        return $return;
    }
    
    
    
    
}
       
?>