<?php

class Wips_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    
    /**
     * get_job 
     * 
     * Returns a job object including WIPS records
     *
     * @param integer $jid The job record id
     * @return object $job
     */
    public function get_job($jid, $include_wips = true) {
        
        $job = (object)array(); 
       
        // first get parent job data...
        $this->db->select("pj.*, 
                jt.type as job_type, 
                s.id as job_sector_raw, 
                s.name as job_sector,   
                pjn.id as pjnid,
                pjn.pjid as pjnpjid,
                pjn.job_number as job_number_old,
                pjn.job_number_alt_uk as job_number_uk,
                pjn.job_number_alt_us as job_number_us,
                pjn.job_number_alt_uk_leg as job_number_uk_leg,
                pjn.job_number_alt_us_leg as job_number_us_leg");
        $this->db->from('parent_jobs pj');
        $this->db->where('pj.id', $jid);
        $this->db->join('job_types jt', 'jt.id = pj.type');
        $this->db->join('sectors s', 's.id = pj.sector');
        $this->db->join('parent_jobs_numbers pjn', 'pjn.pjid = pj.id');
        
        $query = $this->db->get();
        $row = $query->row(); 
        
        $job = $row;
        $job->brand = $row->enc_brand != '' ? $this->wips_decrypt($row->enc_brand) : '';
        $job->account_handler = $row->enc_account_handler != '' ? $this->wips_decrypt($row->enc_account_handler) : '';
        unset($job->enc_brand);
        unset($job->enc_account_handler);
        
        $job->digital_formatted = $job->digital ? 'YES' : 'NO';
        $job->record_added_formatted = $this->format_date($row->record_added);
        $job->record_modified_formatted = $this->format_date($row->record_modified);
        
        $job->default_account_handler_id = $job->account_handler != '' ? $this->get_account_handler_id_by_name($job->account_handler) : 0;
        
        $job->title_leg = false;
        $job->is_parent = false;
        $job->is_parent_jid = 0;
        
        $job_number_display_array = array();
        $job_number_curr_display_array = array();
        $job_number_display_raw_array = array();
        $job_number_hover_display_array = array();
        
        if ($job->job_number_uk) {
            $job_number_display_array[] = "<span class='jnuk'><strong>" . substr($job->job_number_uk, 0, 15) . "</strong>" . substr($job->job_number_uk, 15) . " [UK]<span>";
            $job_number_curr_display_array[] = "<span class='jnuk'><strong>" . substr($job->job_number_uk, 0, 15) . "</strong>" . substr($job->job_number_uk, 15) . " [UK]<span>";
            $job_number_hover_display_array[] = "<span class='jnuk'>[UK]<br /><strong>" . substr($job->job_number_uk, 0, 15) . "</strong>" . substr($job->job_number_uk, 15) . "<span>";
            $job_number_display_raw_array[] = $job->job_number_uk;
        }
        if ($job->job_number_us) {
            $job_number_display_array[] = "<span class='jnus'><strong>" . substr($job->job_number_us, 0, 15) . "</strong>" . substr($job->job_number_us, 15) . " [US]<span>";
            $job_number_curr_display_array[] = "<span class='jnus'><strong>" . substr($job->job_number_us, 0, 15) . "</strong>" . substr($job->job_number_us, 15) . " [US]<span>";
            $job_number_hover_display_array[] = "<span class='jnus'>[US]<br /><strong>" . substr($job->job_number_us, 0, 15) . "</strong>" . substr($job->job_number_us, 15) . "<span>";
            $job_number_display_raw_array[] = $job->job_number_us;
        }
        
        $job->job_number_display = implode('<br />', $job_number_display_array );   
        $job->job_number_display_inline = implode(' / ', $job_number_display_array );
        $job->job_number_curr_display_inline = implode(' / ', $job_number_curr_display_array );
        $job->job_number_display_raw_inline = implode(',', $job_number_display_raw_array );
        $job->job_number_display_raw_inline2 = implode(' | ', $job_number_display_raw_array );
        $job->job_number_hover_display = implode('<br />', $job_number_hover_display_array );
        
        if ($job->job_number_uk) {
            $job->job_number_shorthand = $this->shorthand_alt_job_number_main($job->job_number_uk, $job->job_sector);
        }
        else if ($job->job_number_us) {
            $job->job_number_shorthand = $this->shorthand_alt_job_number_main($job->job_number_us, $job->job_sector);
        }
        else {
            $job->job_number_shorthand = $job->job_number_old;
        }
        
        if (strtolower($job->job_sector) == 'ctr') {
           $job->sector_cell = 'sector-ctr-' . ($job->job_type == 'US' ? 'us' : 'uk'); 
        }    
        else {
           $job->sector_cell = 'sector-' . strtolower($job->job_sector); 
        }
        

        $job->wips_count = $this->get_wips_records_count($jid);
        
        //  $job->finance_data = $this->get_job_finance_data($jid);
        // get estimates - those set in job bag system
        // $job->job_estimates = $this->get_job_estimates($jid, strtolower($job->job_type));
        //   $job->project_team = $this->get_job_project_team($jid, strtolower($job->job_type));

        if ($include_wips && $job->wips_count) 
            // ...then get any associated WIPS records.    
            $job->wips = $this->get_wips_records($jid); 
     
        $job->job_title_leg = false;
     
        if (substr($job->title, 0, 2) == 'C,') {
            $title_explode = explode(',', $job->title);
            $job->client = $title_explode[1];
        }

        // override client with new business based on job number
        if (substr($job->job_number_uk_leg, 0, 1) == 'N') {
            $job->client = 'NEW BUSINESS';
        }

        $job->legacy_data = false; 

        if (($job->job_number_uk_leg && $job->job_number_uk_leg != $job->job_number_uk) ||
            ($job->job_number_us_leg && $job->job_number_us_leg != $job->job_number_us) || $job->job_title_leg) {
            $job->legacy_data = true;
        } 
        
        // SEPT 20 changes - handling closing of 01/02/03/04 records
        $job->mldm = false;
        if($job->job_sector == 'CTR' && substr($job->job_number_uk, 13,2) != '00') {
            $job->mldm = true;
        }
       
        $job->cancelled = false;
        if($job->mldm == true) {
            $job->cancelled = true;
        }

     
        $job->capi_jid = $this->get_capi_jid_from_pjid_capi($job->id);

        return $job;
        
    } 
    
    /*
    public function get_job_estimates($pj_id, $type) {
        
        $estimates = (object)array();
        
        $this->db->select('*');
        $this->db->from('wips_jobs_estimates');
        $this->db->where('pjid', $pj_id);
        $this->db->where('type', $type);
        $this->db->where('current', 1);
        
        $query = $this->db->get();
        $row = $query->row(); 
        
        if ($row) {
            $estimates = $row;
            
            $estimates->hrs_ah = $this->format_hours($estimates->hrs_ah);
            $estimates->hrs_media = $this->format_hours($estimates->hrs_media);
            $estimates->hrs_production = $this->format_hours($estimates->hrs_production);
            $estimates->hrs_creative = $this->format_hours($estimates->hrs_creative);
            $estimates->hrs_ps = $this->format_hours($estimates->hrs_ps);
            
        //    $estimates->data_saved = true;   
            $estimates->date_added_f = $this->format_date($estimates->date_added, 2);
            
            $estimates->gbp_fee = $this->format_finance_data($estimates->gbp_fee);
            $estimates->gbp_fee_discount = $this->format_finance_data($estimates->gbp_fee_discount);
            $estimates->gbp_direct_costs = $this->format_finance_data($estimates->gbp_direct_costs);
            $estimates->gbp_total = $this->format_finance_data($estimates->gbp_total);
            
            $estimates->set = true;
        }
        else { // set defaults....
            
            $estimates->hrs_ah = 0;
            $estimates->hrs_media = 0;
            $estimates->hrs_production = 0;
            $estimates->hrs_creative = 0;
            $estimates->hrs_ps = 0;
                
            $estimates->gbp_fee = 0.00;
            $estimates->gbp_fee_discount = 0.00;
            $estimates->gbp_direct_costs = 0.00;
            $estimates->gbp_total = 0.00;
                
            $estimates->current = 0;
            $estimates->date_added = 0;
            $estimates->date_added_f = 0;
            $estimates->added_by_id = 0;
            $estimates->added_by_name = '';
            
            $estimates->set = false;
            
        }
                
        
        return $estimates;       
     
           
    }
    */
    
    /*
    function get_job_project_team($pj_id, $type) {
        
        $project_team = (object)array();
        
        $this->db->select('*');
        $this->db->from('wips_jobs_project_team');
        $this->db->where('pjid', $pj_id);
        $this->db->where('type', $type);
        $this->db->where('current', 1);
        
        $query = $this->db->get();
        $row = $query->row(); 
        
        if ($row) {
            $project_team = $row;
            $project_team->date_added_f = $this->format_date($project_team->date_added);
            $project_team->set = true;
        }
        else { // set defaults....
            
            $project_team->pm_id = 0;
            $project_team->pm_name = "";
           
            $project_team->ah_id = 0;
            $project_team->ah_name = "";
            
            $project_team->strategy_id = 0;
            $project_team->strategy_name = "";
            
            $project_team->creative1_id = 0;
            $project_team->creative1_name = "";
            
            $project_team->creative2_id = 0;
            $project_team->creative2_name = "";
            
            $project_team->current = 0;
            $project_team->date_added = 0;
            $project_team->added_by_id = 0;
            $project_team->added_by_name = '';
            
            $project_team->set = false;

        }
   
        return $project_team;      
        
    }
    */
    
    /**
     * get_job_finance_data
     * 
     * Returns an object of finance summary data for a particular job if it exists.
     * If they can't be found then returns false.
     *
     * @param int $pj_id Parent Job (internal) ID
     * @return object or false 
     */
    /*
    public function get_job_finance_data($pj_id) {
        
        
        $finance_data = array(
            'dimensions' => $this->get_finance_data($pj_id, 'wips_finance_data'),
            'altair' => $this->get_finance_data($pj_id, 'wips_finance_data_altair')
        );
    
        
        $total_total_costs_r = $finance_data['dimensions']->total_costs_r + $finance_data['altair']->total_costs_r; 
        $total_agency_time_cost_r = $finance_data['dimensions']->agency_time_cost_r + $finance_data['altair']->agency_time_cost_r; 
        $total_third_party_bought_in_r = $finance_data['dimensions']->third_party_bought_in_r + $finance_data['altair']->third_party_bought_in_r; 
        $total_third_party_committed_r = $finance_data['dimensions']->third_party_committed_r + $finance_data['altair']->third_party_committed_r; 
        $total_invoiced_r = $finance_data['dimensions']->invoiced_r + $finance_data['altair']->invoiced_r; 
         
        $finance_data['totals'] = (object)array(
            'total_costs_r' =>  $total_total_costs_r,
            'agency_time_cost_r' => $total_agency_time_cost_r,
            'third_party_bought_in_r' => $total_third_party_bought_in_r,
            'third_party_committed_r' => $total_third_party_committed_r,
            'invoiced_r' => $total_invoiced_r,
                
            'total_costs' => $this->format_finance_data($total_total_costs_r),
            'agency_time_cost' =>  $this->format_finance_data($total_agency_time_cost_r),
            'third_party_bought_in' =>  $this->format_finance_data($total_third_party_bought_in_r),
            'third_party_committed' =>  $this->format_finance_data($total_third_party_committed_r),
            'invoiced' => $this->format_finance_data($total_invoiced_r),
      
        );
        
        return $finance_data;
       
    }
    */
    
    /*
    public function get_finance_data($pj_id, $table) {
        
        $this->db->from($table);
        $this->db->where('pj_id', $pj_id);
        
        $query = $this->db->get();
        $row = $query->row(); 
        
        if($row) {
            
            $set = true;
            $total_costs_r = $row->enc_total_costs ? $this->wips_decrypt($row->enc_total_costs) : 0;
            $agency_time_cost_r = $row->enc_agency_time_cost ? $this->wips_decrypt($row->enc_agency_time_cost) : 0;
            $third_party_bought_in_r = $row->enc_third_party_bought_in ? $this->wips_decrypt($row->enc_third_party_bought_in) : 0;
            $third_party_committed_r = $row->enc_third_party_committed ? $this->wips_decrypt($row->enc_third_party_committed) : 0;
            $invoiced_r = $row->enc_invoiced ? $this->wips_decrypt($row->enc_invoiced) : 0;
            $estimate_r = $row->enc_estimated ? $this->wips_decrypt($row->enc_estimated) : 0;
            $updated_r = $row->updated ? $row->updated : 0;
            
            if ($total_costs_r + $agency_time_cost_r + $third_party_bought_in_r + $third_party_committed_r + $invoiced_r == 0)
                $set = false;
            
        }
        else {
            
            $set = false;
            $total_costs_r = 0;
            $agency_time_cost_r = 0;
            $third_party_bought_in_r = 0;
            $third_party_committed_r =  0;
            $invoiced_r = 0;
            $estimate_r = 0;
            $updated_r = 0;
            
        }
            
        return (object)array(
            'total_costs_r' => $total_costs_r,
            'agency_time_cost_r' => $agency_time_cost_r,
            'third_party_bought_in_r' => $third_party_bought_in_r,
            'third_party_committed_r' => $third_party_committed_r,
            'invoiced_r' => $invoiced_r,
            'estimate_r' => $estimate_r,
                
            'total_costs' => $this->format_finance_data($total_costs_r),
            'agency_time_cost' =>  $this->format_finance_data($agency_time_cost_r),
            'third_party_bought_in' =>  $this->format_finance_data($third_party_bought_in_r),
            'third_party_committed' =>  $this->format_finance_data($third_party_committed_r),
            'invoiced' => $this->format_finance_data($invoiced_r),
            'estimated' => $estimate_r ? $this->format_finance_data($estimate_r) : 'n/a',
            
            'updated' => $updated_r ? $this->format_date($updated_r, 2) : false,
            'set' => $set,
        );
        
    }
    */
/*
    public function format_finance_data($value) {
        
        if ($value == 0) {
            return '£0.00';
        }
        else if ($value < 0) {
            return '- £' . number_format((float)$value*-1, 2);
        }
        else {
            return '£'. number_format((float)$value, 2);
        }
        
//        return $value > 0 ? '£'. number_format((float)$value, 2) : '£0.00';
    }
    */
    
    /**
     * get_account_handler_id_by_name
     * 
     * Returns a person record id of an account handler based on name provided.
     * If they can't be found then returns 0.
     *
     * @param string $name Account handler full name
     * @return integer 
     */
    public function get_account_handler_id_by_name($name) {
        
        $this->db->select('id');
        $this->db->from('wips_persons');
        $this->db->like('name', $name, 'both'); 
        $this->db->where('tid', 1);
        
        $query = $this->db->get();
        $row = $query->row(); 

        if($row) {
            return $row->id;
        }
        else {
            return 0;
        }
    }
    
    /**
     * get_child_jobs
     * 
     * Returns any child jobs of a parent job
     *
     * @param integer $pjnum Parent Job Number
     * @return array 
     */
    /*
    public function get_child_jobs($pjnum) {
        
        $child_jobs = array();
        
        $this->db->select('pj.id, pj.number, pj.title');
        $this->db->from('parent_jobs pj');
        $this->db->where('pj.parent_number', $pjnum);
        $this->db->where('pj.number !=', $pjnum);
        $this->db->order_by('pj.number', 'ASC');
        
        $query = $this->db->get();
        
        foreach ($query->result() as $row) {
            
            $child_jobs[] = array(
                'id' => $row->id,
                'number' => $row->number,
                'title' => $row->title,
            );
         }
         
         return $child_jobs;
    }
    */
    
    /**
     * get_wips_records count
     * 
     * Returns number of WIPS records of a particular job
     *
     * @param integer $jid Job record ID 
     * @return int 
     */
    public function get_wips_records_count($jid) {

        $query = $this->db->query("SELECT COUNT(*) as count FROM wips_jobs WHERE pjid = " . $jid);
        $result = $query->row();
        return $result->count; 
   
    }    

    public function get_wips_account_handler($jid) {
        $query = $this->db->query("SELECT enc_account_handler FROM parent_jobs WHERE id = " . $jid);
        $result = $query->row();
        if ($result) {
            return $this->wips_decrypt($result->enc_account_handler); 
        }
        else {
            return 'Unknown';
        }


    }
    
    
    /**
     * get_wips_records
     * 
     * Returns all WIPS records of a particular job
     *
     * @param integer $jid Job record ID 
     * @return array 
     */
    public function get_wips_records($jid) {
        
        $wips = array();
        
        $this->db->select('wj.*, wjs.ws_id as status_id, ws.status as status');
        $this->db->from('wips_jobs wj');
        $this->db->where('wj.pjid', $jid);
        $this->db->join('wips_jobs_status wjs', 'wjs.wj_id = wj.id', 'left');
        $this->db->join('wips_status ws', 'ws.id = wjs.ws_id', 'left');
        $this->db->order_by('wj.created_on', 'ASC'); // latest first
        
        $query = $this->db->get();
        
        $wips_count = 0;
        
        foreach ($query->result() as $row) {
            
            $wip['row'] = $row;
            $wip = array(
                'id' => $row->id,
                'pjid' => $row->pjid,
                'status_notes' => $this->wips_decrypt($row->enc_status_notes),
                'live_date' => ($row->live_date && $row->live_date != '0000-00-00' && $row->live_date != '1970-01-01') ? date('d-m-Y', strtotime($row->live_date)) : '',
                'no_items' => $row->no_items,
                'countries' => $row->countries,
                'created_on' => $row->created_on,
                'created_on_formatted' => $this->format_date($row->created_on), 
                
                //'created_by_uid' => $row->created_by,
                //'created_by_name' => $this->get_user_name_by_uid($row->created_by),
                /* CAPI-fied */
                'created_by_uid' => $row->_created_by,
                'created_by_name' => $row->_created_by_name,

                'status_id' => $row->status_id,
                'status_name' => $row->status
            );
            
            // get wips stages
            $stageinfo = $this->get_job_stages_by_id($row->id);
            $wip['stages'] = $stageinfo['stages'];
            $wip['stages_a'] = $stageinfo['stages_a'];
            
            // get wips people details...
            // ...account handlers
            $wip['accounts'] = $this->get_people_by_type_by_id('accounts', $row->id);
            $wip['account_handler1'] = isset($wip['accounts'][0]->name) ? $wip['accounts'][0]->name : '';
            $wip['account_handler2'] = isset($wip['accounts'][1]->name) ? $wip['accounts'][1]->name : '';
            $wip['account_handler1_id'] = isset($wip['accounts'][0]->id) ? $wip['accounts'][0]->id : '0';
            $wip['account_handler2_id'] = isset($wip['accounts'][1]->id) ? $wip['accounts'][1]->id : '0';
            // ...delivery
            $wip['delivery'] = $this->get_people_by_type_by_id('delivery', $row->id);
            $wip['delivery_lead1'] = isset($wip['delivery'][0]->name) ? $wip['delivery'][0]->name : '';
            $wip['delivery_lead1_id'] = isset($wip['delivery'][0]->id) ? $wip['delivery'][0]->id : '0';
            // ...creatives
            $wip['creatives'] = $this->get_people_by_type_by_id('creatives', $row->id);
            $wip['lead_creative1'] = isset($wip['creatives'][0]->name) ? $wip['creatives'][0]->name : '';
            $wip['lead_creative2'] = isset($wip['creatives'][1]->name) ? $wip['creatives'][1]->name : '';
            $wip['lead_creative1_id'] = isset($wip['creatives'][0]->id) ? $wip['creatives'][0]->id : '0';
            $wip['lead_creative2_id'] = isset($wip['creatives'][1]->id) ? $wip['creatives'][1]->id : '0';
            
            // get wips resource requests
            $wip['resource_requests_full'] = $this->get_resource_requests_by_id($row->id);
            $wip['resource_requests'] = array();
            $wip['resource_requests_client'] = array();
            
            foreach($wip['resource_requests_full'] as $key=>$value) {
                $wip['resource_requests'][] = $this->wips_decrypt($value->enc_request);
                $wip['resource_requests_client'][] = $value->client;
            }
            
            // define update markers so we can highlight if the record has changed since the last version
            $wip['updated'] = array(
                'status_notes' => ($wips_count && strcmp($wip['status_notes'], $wips[$wips_count-1]['status_notes']) !=0) ? 1 : 0,
                'live_date' => ($wips_count && strcmp($wip['live_date'], $wips[$wips_count-1]['live_date']) !=0) ? 1 : 0,
                'no_items' => ($wips_count && strcmp($wip['no_items'], $wips[$wips_count-1]['no_items']) !=0) ? 1 : 0,
                'countries' => ($wips_count && strcmp($wip['countries'], $wips[$wips_count-1]['countries']) !=0) ? 1 : 0,
                'status' => ($wips_count && ($wip['status_id'] != $wips[$wips_count-1]['status_id'])) ? 1 : 0,
                'stages' => ($wips_count && strcmp(implode('', $wip['stages']), implode('', $wips[$wips_count-1]['stages'])) !=0) ? 1 : 0,
                'accounts' => ($wips_count && (($wip['account_handler1_id'] != $wips[$wips_count-1]['account_handler1_id']) || ($wip['account_handler2_id'] != $wips[$wips_count-1]['account_handler2_id']))) ? 1 : 0,
                'delivery' => ($wips_count && (($wip['delivery_lead1_id'] != $wips[$wips_count-1]['delivery_lead1_id']))) ? 1 : 0,
                'creatives' => ($wips_count && (($wip['lead_creative1_id'] != $wips[$wips_count-1]['lead_creative1_id']) || ($wip['lead_creative2_id'] != $wips[$wips_count-1]['lead_creative2_id']))) ? 1 : 0,
                'resource_requests' => ($wips_count && strcmp(implode('', $wip['resource_requests']), implode('', $wips[$wips_count-1]['resource_requests'])) !=0) ? 1 : 0,
            );
            
            $wips[$wips_count] = $wip;
            $wips_count++;
        }
        // reverse the arrat so we have it latest first
        $wips_r = array_reverse($wips);

        return $wips_r;
    } 
    
    
    /**
     * get_people_by_type_by_id
     * 
     * Returns all people associated with a wip record by type
     *
     * @param string $type Person type accounts/delivery/creatives 
     * @param integer $id WIP record ID 
     * @return array
     */
    public function get_people_by_type_by_id($type = 'accounts', $id) {
        
        $people = array();
        
        $this->db->select('wp.id, wp.name');
        $this->db->from('wips_jobs_people_' . $type . ' wjp');
        $this->db->join('wips_persons wp', 'wp.id = wjp.wp_id');
        $this->db->where('wj_id', $id);
        $this->db->order_by('wjp.rank', 'ASC');
        
        $query = $this->db->get();
        
        foreach ($query->result() as $row) {
            $people[] = (object)array(
                'id' => $row->id,
                'name' => $row->name
            );
        }
        
        return $people;
    }
        
    /**
     * get_resource_requests_by_id
     * 
     * Returns resource requests for an individual WIPS record
     *
     * @param integer $id WIP record ID 
     * @return array
     */
    public function get_resource_requests_by_id($id) {
        
        $resource_request = array();
        
        $this->db->select('*');
        $this->db->from('wips_resource_requests wrr');
        $this->db->where('wrr.wj_id', $id);
        $this->db->order_by('wrr.rank', 'ASC');
        
        $query = $this->db->get();
        
        foreach ($query->result() as $row) {
            $resource_request[] = $row;
        }
        
        return $resource_request;
    }
    
    /**
     * get_job_stages_by_id
     * 
     * Returns job stages for an individual WIPS record
     *
     * @param integer $id WIP record ID 
     * @return array
     */
    public function get_job_stages_by_id($id) {
        
        $stages = array();
        $stages_a = array();
        
        $this->db->select('wjs.ws_id, ws.stage');
        $this->db->from('wips_jobs_stages wjs');
        $this->db->join('wips_stages ws', 'ws.id = wjs.ws_id', 'left');
        $this->db->where('wjs.wj_id', $id);
                
        $query = $this->db->get();
        
        foreach ($query->result() as $row) {
            $stages[$row->ws_id] = $row->stage;
            $stages_a[] = $row->ws_id; 
        }
        
        return array(
            'stages' => $stages,
            'stages_a' => $stages_a
        );
    }
    
    /**
     * find_job_id_by_job_number
     * 
     * Find the local db job record id based on the job number.
     * If the job can't be found returns 0
     *
     * @param integer $jnumber Job number
     * @return int
     */
    public function find_job_id_by_job_number($jnumber) {
        
        $this->db->select("id");
        $this->db->from('parent_jobs');
        $this->db->where('number', $jnumber);
      
        $query = $this->db->get();
        $row = $query->row(); 

        if($row) {
            return $row->id;
        }
        else {
            return 0;
        }
    }
    
    
    /**
     * save_wip
     * 
     * Saves a WIP record to the database
     *
     * @param integer $jid Job record ID 
     * @param object $wip WIP data 
     * @return 
     */
    public function save_wip($jid, $wip) {
    
        $userdata = $this->session->userdata('user');

        // Step 1. update the previous current wip record if there is one to no longer current.
        $this->db->where('pjid', $jid);
        $this->db->where('current', 1);
        $this->db->update('wips_jobs', array('current' => 0)); 
        
        // 2. insert data into wips_jobs and make it the current record
        $data = array(
            'pjid' => $jid, 
            'enc_status_notes' => $this->wips_encrypt($wip->statusNotes),
            'live_date' => $wip->liveDate ? date('Y-m-d', strtotime($wip->liveDate)) : NULL,
            'no_items' => $wip->items ? $wip->items : NULL,
            'countries' => $wip->countries,
            'created_on' => time(),
            'created_by' => 0, //$this->session->userdata('urid'), 
            'current' => 1,
            /* CAPI-fied */
            '_created_by' => $this->session->userdata('urid'),
            '_created_by_name' =>  $userdata->full_name,  

        );
        $this->db->insert('wips_jobs', $data);
        
        // 3. get the new record id
        $wj_id = $this->db->insert_id(); 
        
        // 4. add status to wips_job_status table
        $this->db->insert('wips_jobs_status', array(
            'wj_id' => $wj_id,
            'ws_id' => $wip->currentStatus)
        );
        
        // 5. add stages into wips_job_stages table
        foreach($wip->currentStage as $key => $ws_id) {
            $this->db->insert('wips_jobs_stages', array(
                'wj_id' => $wj_id, 
                'ws_id' => $ws_id)
            );
        }
        
        // 6. add resource requests data to wips_resource_requests table
        $rcount = 1;
        foreach($wip->resourceRequest as $key => $request) {
            $this->db->insert('wips_resource_requests', array(
                'wj_id' => $wj_id,
                //'request' => $request,
                'enc_request' => $this->wips_encrypt($request),
                'rank' => $rcount,
                'complete' => 0,
                'complete_by' => 0,
                'client' => isset($wip->resourceRequestClient[$key]) ? $wip->resourceRequestClient[$key] : 0)                  
            );
            $rcount++;
        }
       // $this->db->insert('wips_resource_request', $stage_data);
        
        
        // 7. Add people data to relevant tables...
        // ... account handlers
        if ($wip->accountHandler1_id) {
            $this->db->insert('wips_jobs_people_accounts', array('wj_id' => $wj_id, 'wp_id' => $wip->accountHandler1_id, 'rank' => 1));
        }
        if ($wip->accountHandler2_id && ($wip->accountHandler1_id != $wip->accountHandler2)) {
            $this->db->insert('wips_jobs_people_accounts', array('wj_id' => $wj_id, 'wp_id' => $wip->accountHandler2_id, 'rank' => 2));
        }
        // delivery
        if ($wip->deliveryLead1_id) {
            $this->db->insert('wips_jobs_people_delivery', array('wj_id' => $wj_id, 'wp_id' => $wip->deliveryLead1_id, 'rank' => 1));
        }
        // creatives
        if ($wip->leadCreative1_id) {
            $this->db->insert('wips_jobs_people_creatives', array('wj_id' => $wj_id, 'wp_id' => $wip->leadCreative1_id, 'rank' => 1));
        }
        if ($wip->leadCreative2_id && ($wip->leadCreative1_id != $wip->leadCreative2)) {
            $this->db->insert('wips_jobs_people_creatives', array('wj_id' => $wj_id, 'wp_id' => $wip->leadCreative2_id, 'rank' => 2));
        }
        
        return $wj_id.$jid;
        
    }
    
    /** 
     * save_capi_job_and_wip
     *
     *  *** NEW FOR CAPI ****
     *  Save capi job details to parent_jobs / parent_job_number / _pjid_capi
     *  and then call save_wip with new parent_jobs id
     * 
     */ 
    public function save_capi_job_and_wip($capi_job, $wip) {

        $pj_input = array();

        $now = time();

        // build parent_jobs import array from capi job data  
        $pj_input = array(
            'number' => 0,
            'type' => $capi_job->location_id,
            'title' => $capi_job->job_name,
            'title_leg' => '',
            
            'description' => '',
            'client' => $capi_job->client_name,
            'account_handler_id' => $capi_job->lead_account_handler_id,
            'digital' => $capi_job->digital,
            
            'sector' => $capi_job->sector_id == 0 ? 6+$capi_job->discipline_id :  $capi_job->sector_id,  // ($capi_job->sector_id < 5 ? $capi_job->sector_id : 6+$capi_job->discipline_id),
            'parent_number' => '',
            
            'access_created' => strtoupper(date('j M y', $now)),
            'access_modified' => strtoupper(date('j M y', $now)),
            'record_added' => $now,
            'record_modified' => $now,

            'job_open' => $capi_job->active,
            
            'enc_brand' => $this->wips_encrypt($capi_job->brand_name),
            'enc_account_handler' => $this->wips_encrypt($capi_job->lead_account_handler_name),

            'alt_client' => '',
            'alt_brand' => '',
            'alt_ah' => '',
            'alt_created' => 0,
            'alt_updated' => 0,

            'active' => $capi_job->active,
        );

        // insert parent jobs record
        $this->db->insert('parent_jobs', $pj_input);
        $pjid = $this->db->insert_id();

        $pjn_input = array(
            'pjid' => $pjid,
            'search_title' => $capi_job->job_name,
            'job_number' => NULL,
            'job_number_alt' => NULL,
            'job_number_alt_uk' => $this->get_capi_job_number($capi_job->numbers, 1),
            'job_number_alt_us' => $this->get_capi_job_number($capi_job->numbers, 2),
            'search_title_leg' => '',
            'job_number_alt_uk_leg' => NULL, 
            'job_number_alt_us_leg' => NULL, 
            'jbjid' => 0,
            'capi_jid' => $capi_job->id            
        );

        // insert parent job numbers record
        $this->db->insert('parent_jobs_numbers', $pjn_input);

        // now insert into id 'join' table
        $pjid_capi = array(
            'pjid' => $pjid,
            'capi_jid' => $capi_job->id
        );
        $this->db->insert('_pjid_capi', $pjid_capi);


        if ($wip) {
            // update wip object with id and save wip
            $wip->currentWipParentID = $pjid;
            return $this->save_wip($pjid, $wip);
        }
        else {
            return $pjid;
        }


    }

    /*  *** NEW FOR CAPI ****/
    public function get_capi_job_number($numbers, $type) {

        foreach($numbers as $key => $number) {
            if ($number->job_number_type_id == $type) {
                return $number->number;
            }
        }
        return NULL;

    }

    /**
     * get_wips_statuses
     * 
     * Returns all statuses from reference table
     *
     * @return array
     */
    public function get_wips_statuses() {
        
        $statuses = array();
        
        $this->db->select('id, status');
        $this->db->from('wips_status');
        $this->db->order_by('rank', 'ASC');
        
        $query = $this->db->get();
        
        foreach ($query->result() as $row) {
            $statuses[] = (object)array(
                'key' => $row->id,
                'status' => $row->status
            );        
        } 
        return $statuses;
    }
    
    /**
     * get_wips_stages
     * 
     * Returns all stages from reference table
     *
     * @return array
     */
    public function get_wips_stages() {
        
        $stages = array();
        
        $this->db->select('id, stage');
        $this->db->from('wips_stages');
        $this->db->order_by('rank', 'ASC');
        
        $query = $this->db->get();
        
        foreach ($query->result() as $row) {
            $stages[] = (object)array(
                'key' => $row->id,
                'status' => $row->stage
            );        
        } 
        return $stages;
    }
   
    /**
     * get_wips_sectors
     * 
     * Returns all sectors from reference table
     *
     * @return array
     */
    public function get_wips_sectors() {
        
        $sectors = array();
        
        $this->db->select('id, name');
        $this->db->from('sectors');
        /* exclude new CTE discipline so as not to duplicate - add back into to get_wips query later if old CTE sector filter set */
        $this->db->where('id !=', 10);
        $this->db->order_by('name', 'ASC');
        
        $query = $this->db->get();
        
        foreach ($query->result() as $row) {
            $sectors[] = (object)array(
                'key' => $row->id,
                'sector' => $row->name
            );        
        } 
        return $sectors;
    }
    
    /**
     * get_wips_job_types
     * 
     * Returns all job types from reference table
     *
     * @return array
     */    
    public function get_wips_job_types() {
        
        $jobtypes = array();
        
        $this->db->select('id, type');
        $this->db->from('job_types');
        $this->db->order_by('id', 'ASC');
        
        $query = $this->db->get();
        
        foreach ($query->result() as $row) {
            $jobtypes[] = (object)array(
                'key' => $row->id,
                'type' => $row->type
            );        
        } 
        return $jobtypes;
    }

    /**
     * get_active_wips_clients
     * 
     * Returns all clients from parent_jobs table
     *
     * @return array
     */
    
    public function get_active_wips_clients() {
        
        $clients = array();
         
        $query = $this->db->query("
            SELECT DISTINCT(pj.client) as client
            FROM parent_jobs pj
            ORDER BY pj.client ASC");
        
        foreach ($query->result() as $row) {
            $clients[] = (object)array(
                'name' => $row->client
            );        
        } 
        return $clients;
    }
    
    
    /**
     * get_user_name_by_uid
     * 
     * Get the name of a user based upon their record id
     *
     * @param integer $uid User record ID 
     * @return string
     */    
    /*
    public function get_user_name_by_uid______xxx($uid) {
        
        if($uid == 0) {
            return 'System';
        }
        
        $this->db->select('enc_full_name');
        $this->db->from('users');
        $this->db->where('urid', $uid);
        
        $query = $this->db->get();
        $row = $query->row(); 

        if($row) {
            return $this->wips_decrypt($row->enc_full_name);
        }
        else {
            return 'unknown';
        }
    }
    */
    /*
     * search_jobs
     * 
     * Search jobs, this will search job number, parent job number and title against the input
     *
     * 
     *  REDUNDANT - Replaced with CAPI
     *
     public function search_jobs($string) {
        
        $jobs = array();
 
        $query = $this->db->query("
            SELECT pjn.*, (SELECT COUNT(*) FROM wips_jobs wj WHERE wj.pjid = pj.id) as wips_count,
            (SELECT wj2.id FROM wips_jobs wj2 WHERE wj2.pjid = pj.id ORDER BY wj2.created_on DESC LIMIT 1) as wip_id
            FROM parent_jobs_numbers pjn 
            LEFT JOIN parent_jobs pj ON pj.id = pjn.pjid
            WHERE 
            (pjn.job_number LIKE '%" . $string . "%' 
            OR pjn.job_number_alt LIKE '%" . $string . "%' 
            OR pjn.job_number_alt_us LIKE '%" . $string . "%' 
            OR pjn.job_number_alt_uk LIKE '%" . $string . "%'
            OR pjn.job_number_alt_uk_leg LIKE '%" . $string . "%'
            OR pjn.job_number_alt_us_leg LIKE '%" . $string . "%'
            OR pjn.search_title LIKE '%" . $string . "%'
            OR pjn.search_title_leg LIKE '%" . $string . "%') AND pjn.job_number_alt_uk != '' LIMIT 50");
            
            
        foreach ($query->result() as $row) {
            $jobs[] = $this->get_job($row->pjid);
        }
        
        return $jobs;
    }
    
    */

    /* CAPI-fied */
    /* new service to integrate CAPI search results with 'local' WIP data */
     public function build_job_search_list($jobs) {

        $jobs_list = array();

        foreach($jobs as $key=>$job) {

        //    if ($job->active == '1') {

                $pjid = $this->get_pjid_from_pjid_capi($job->id);

                $job = (object)array(
                    "capi_jid" => $job->id,
                    "id" => $pjid,
                    "job_number_display" => $this->get_capi_job_numbers_display($job->numbers),
                    "title" => $job->job_name,
                    "job_type" => $job->location_name,
                    "client" => $job->client_name,
                    "brand" => $job->brand_name,
                    "job_sector" => $job->discipline_name ? $job->discipline_name : $job->sector_name,
                    "job_sector_raw" => $job->discipline_id != '0' ? $job->discipline_id + 5 : $job->sector_id,
                    "digital" => 0,
                    "account_handler" => $job->lead_account_handler_name == 'Unknown' ? $this->get_wips_account_handler($pjid) : $job->lead_account_handler_name,
                    "wips_count" => $this->get_wips_records_count($pjid),
                    "wips" => $this->get_wips_records($pjid)
                );

                $jobs_list[] = $job;

        //    }

        }
        return $jobs_list;

    }

    public function get_capi_job_numbers_display($numbers) {

        $numa = array('', 'uk', 'us');

        $numstring = "";
        $numstring_a = array();

        foreach($numbers as $key=>$num) {

            $numstring_a[] = 
                "<span class='jn" . $numa[$num->job_number_type_id] . "'>" .
                    "<strong>" . $num->number . "</strong> [" . strtoupper($numa[$num->job_number_type_id]) . "]</span>"; 

        }

        $numstring = implode('<br />', $numstring_a);
        return $numstring;
    }

    /**
     * get_wip_by_id
     * 
     * Get a full WIPS record based on WIP record ID
     *
     * @param integer $wip_id WIP record ID 
     * @return object
     */    
    public function get_wip_by_id($wip_id) {
        
        $query = $this->db->query("SELECT * FROM wips_jobs WHERE id=" . $wip_id);
        $result = $query->row();
        $result->created_on_formatted = $this->format_date($result->created_on);
        
        //$result->created_by_name = $this->get_user_name_by_uid($result->created_by);   
        // CAPI-fied
        $result->created_by_name = $result->_created_by_name;        
        return $result;
    }
   
    /**
     * get_wips
     * 
     * Get WIPS records based upon supplied filters.
     * This function builds a complex query string based upon the filters set.
     * 
     * *** ONCE MORE DATA IS IN WE WILL NEED TO MONITOR PERFORMANCE ***
     *
     * @param object $filters Filters set by user
     * @return array
     */    
    public function get_wips($filters = null) {
       
        $filters = json_decode($filters);
                
        // initialise the result set
        $wips = array();
        
        // initialis a load of arrays used to construct the sql query
        $sql_a_select = array();
        $sql_a_where = array();
        $sql_a_joins = array();
        $sql2_a_where = array();
        
        // set some initial selects
        $sql_a_select[] = "wj.id AS wid";
        $sql_a_select[] = "wj.pjid AS pjid";
        $sql_a_select[] = "pj.client AS pjclient";

        
        // initial query result set if only looking for WIPS you have contributed to...
        if ($filters->only_mine) {
            
            $pjids = array(); 
            
            // get array of pjids where the current user has created/edited the childs... 
            /* CAPI-fied */
            $sql = "SELECT DISTINCT(pjid) FROM wips_jobs WHERE _created_by = " . $this->session->userdata('urid');
            $query = $this->db->query($sql);
            foreach ($query->result() as $row) {
                $pjids[] = $row->pjid;
            }
            
            if(count($pjids) == 0) {
                return $rows; 
            }
            else {
                $sql_a_where[] = "wj.pjid IN (" . implode(',',$pjids) . ")";
            }
        }
        
        // in to the logic based on filters set
        if (count($filters->stages)>0) {
            $sql_a_select[] = "wj_stages.ws_id AS stages";
            $sql_a_joins[] = "LEFT JOIN wips_jobs_stages wj_stages ON wj_stages.wj_id = wj.id";
            $sql2_a_where[] = "B.stages IN (" . implode(',', $filters->stages) . ")";
        }

        if (count($filters->account_handlers)>0) {
            $sql_a_select[] = "wjpa.wp_id AS accounts";
            $sql_a_joins[] = "LEFT JOIN wips_jobs_people_accounts wjpa ON wjpa.wj_id = wj.id";
            $sql2_a_where[] = "B.accounts IN (" . implode(',', $filters->account_handlers) . ")";
        }
        
        if (count($filters->lead_delivery)>0) {
            $sql_a_select[] = "wjpd.wp_id AS delivery";
            $sql_a_joins[] = "LEFT JOIN wips_jobs_people_delivery wjpd ON wjpd.wj_id = wj.id";
            $sql2_a_where[] = "B.delivery IN (" . implode(',', $filters->lead_delivery) . ")";
        }
            
        if (count($filters->lead_creatives)>0) {
            $sql_a_select[] = "wjpc.wp_id AS creatives";
            $sql_a_joins[] = "LEFT JOIN wips_jobs_people_creatives wjpc ON wjpc.wj_id = wj.id";
            $sql2_a_where[] = "B.creatives IN (" . implode(',', $filters->lead_creatives) . ")";
        }
        
        if ($filters->resource_requests) {
           // $sql_a_joins[] = "JOIN (SELECT wj_id, COUNT(*) mycount FROM wips_resource_requests WHERE request != '' group by wj_id ) A ON A.wj_id = wj.id";
            $sql_a_joins[] = "JOIN (SELECT wj_id, COUNT(*) mycount FROM wips_resource_requests WHERE enc_request != '' group by wj_id ) A ON A.wj_id = wj.id";
        }
        
        // some required joins
        $sql_a_joins[] = "LEFT JOIN parent_jobs pj ON pj.id = wj.pjid";
        $sql_a_joins[] = "LEFT JOIN wips_jobs_status wj_status ON wj_status.wj_id = wj.id";
        
        // only select the current record..
        $sql_a_where[] = "wj.current = 1";
        
        // do not show completed or cancelled.... unless requested
        // add some addtional status values on completed and cancelled flags
        if ($filters->only_completed) {
            $filters->status = array(5);
        }
        else if ($filters->only_cancelled) {
            $filters->status = array(6);
        }
        else {
            if (!count($filters->status)) {
                $filters->status = array(1,2,3,4);
            }
            if ($filters->show_completed) {
                $filters->status[] = 5;
            }
            if ($filters->show_cancelled) {
                $filters->status[] = 6;
            }
        }
            
        // set conditions based on filters...
        /* to handle new CTE discipline when old CTE sector selected */
        if (in_array(3, $filters->sectors)) {
            $filters->sectors[] = 10;
        }

        if (count($filters->sectors) > 0) {
            $sql_a_where[] = "pj.sector IN (" . implode(',', $filters->sectors) . ")";
        }
        
        if (count($filters->clients) > 0) {
            $sql_a_where[] = "pj.client IN (" . "'" . implode("','", $filters->clients) . "'" . ")";
        }
        
        if (count($filters->types) > 0) {
            $sql_a_where[] = "pj.type IN (" . implode(',', $filters->types) . ")";
        }
        
        if (count($filters->digital) > 0) {
            $sql_a_where[] = "pj.digital IN (" . implode(',', $filters->digital) . ")";
        }
        
        if (count($filters->status) > 0) {
            $sql_a_where[] = "wj_status.ws_id IN (" . implode(',', $filters->status) . ")";
        }
        
        // build the sub-query
        $sql = "SELECT " . implode(', ', $sql_a_select) . 
                " FROM wips_jobs wj " . implode(' ', $sql_a_joins) . 
                " WHERE " . implode(' AND ', $sql_a_where) . 
                " ORDER BY pj.client ASC";         
        
        // build the query
        $sql2 = "SELECT DISTINCT (B.wid) as wid FROM(" . $sql . ") B ";
        
        // add the conditions
        if (count($sql2_a_where) > 0) {
            $sql2 .= "WHERE " . implode(' AND ', $sql2_a_where); 
        }

        
        // execute the query
        $query = $this->db->query($sql2);
        
        // build results set of wip objects
        foreach ($query->result() as $row) {
            $wips[] = $this->get_wip($row->wid);
        } 
        
        // here return the generated query also - sql2
        // then if the view gets saved we can save the sql query
        // then on load of view, if sql query exists use that instead of re-genetating the whole thing...
        
        $return = array(
            'wips' => $wips,
            'sql' => $sql,
        );
              
        return $return;
//        return $wips;
    } 
    


    /* CAPI-fied *
    /* Get parent_jobs id based on new capi jobs id - using new 'join' table */
    public function get_pjid_from_pjid_capi($capi_jid) {

        $this->db->select('pjid');
        $this->db->from('_pjid_capi');
        $this->db->where('capi_jid', $capi_jid);
        $query = $this->db->get();
        $row = $query->row(); 

        if ($row) 
            return $row->pjid;
        else 
            return 0;
        
    }

    /* CAPI-fied *
    /* Get capi jobs id based on parent_jobs id - using new 'join' table */
    public function get_capi_jid_from_pjid_capi($pjid) {

        $this->db->select('capi_jid');
        $this->db->from('_pjid_capi');
        $this->db->where('pjid', $pjid);
        $query = $this->db->get();
        $row = $query->row(); 

        if ($row) 
            return $row->capi_jid;
        else 
            return 0;
        
    }


    /**
     * get_wip
     * 
     * Get a single wip object based upon wip record id.
     * This will include parent job data.
     * 
     * @param integer $id WIP record ID 
     * @return object
     */    
    public function get_wip($id) {
        
        $wip = (object)array();
     
        // the query
        $query = $this->db->query("
            SELECT wj.*,
                pj.*, 
                s.name as sector_name,

                jt.type as job_type,
                wjs.ws_id as status_id,
                ws.status as status_name,
                pjn.id as pjnid,
                pjn.pjid as pjnpjid,
                pjn.job_number as job_number_old,
                pjn.job_number_alt_uk as job_number_uk,
                pjn.job_number_alt_us as job_number_us,
                pjn.job_number_alt_uk_leg as job_number_uk_leg,
                pjn.job_number_alt_us_leg as job_number_us_leg
                FROM wips_jobs wj
                LEFT JOIN parent_jobs pj ON pj.id = wj.pjid
                LEFT JOIN sectors s ON s.id = pj.sector
                LEFT JOIN job_types jt ON jt.id = pj.type
                LEFT JOIN wips_jobs_status wjs ON wjs.wj_id = wj.id
                LEFT JOIN wips_status ws ON ws.id = wjs.ws_id
                LEFT JOIN parent_jobs_numbers pjn ON pjn.pjid = pj.id
                WHERE wj.id = " . $id);
        
        $result = $query->row();
     
        // core wip record
        $wip->id = $id;
        $wip->pjid = $result->pjid;

        $wip->capi_jid = $this->get_capi_jid_from_pjid_capi($result->pjid);

    //    $wip->status_notes = $result->status_notes;
        $wip->status_notes = $this->wips_decrypt($result->enc_status_notes);
        $wip->live_date =  ($result->live_date && $result->live_date != '0000-00-00' && $result->live_date != '1970-01-01')  ? date('d.m.Y', strtotime($result->live_date)) : '';
        $wip->live_date_raw = ($result->live_date && $result->live_date != '0000-00-00' && $result->live_date != '1970-01-01')  ? strtotime($result->live_date) : 0;
        $wip->no_items = $result->no_items;
        $wip->countries = $result->countries;
        $wip->created_on_raw = $result->created_on;
        $wip->created_on_formatted = $this->format_date($result->created_on);
        //$wip->created_by_id = $result->created_by;
        //$wip->created_by_name = $this->get_user_name_by_uid($result->created_by);

        /* CAPI-fied */
        $wip->created_by_id = $result->_created_by;
        $wip->created_by_name = $result->_created_by_name;

        // status
        $wip->status_id = $result->status_id;
        $wip->status = $result->status_name;
        
        // stages
        $stageinfo = $this->get_job_stages_by_id($id);
        $wip->stages = $stageinfo['stages'];
        $wip->stages_string = implode(', ', $stageinfo['stages']);
        $wip->stages_a = $stageinfo['stages_a'];
            
        // people...
        // ... account handlers
        $wip->accounts = $this->get_people_by_type_by_id('accounts', $id);
        $wip->account_handler1 = isset($wip->accounts[0]->name) ? $wip->accounts[0]->name : '';
        $wip->account_handler2 = isset($wip->accounts[1]->name) ? $wip->accounts[1]->name : '';
        $wip->account_handlers_x = $wip->account_handler1 . ($wip->account_handler2 ? ' | ' . $wip->account_handler2 : '');
        $wip->account_handlers_pdf = $wip->account_handler1 . ($wip->account_handler2 ? ',' . $wip->account_handler2 : '');
        $wip->account_handler1_id = isset($wip->accounts[0]->id) ? $wip->accounts[0]->id : 0;
        $wip->account_handler2_id = isset($wip->accounts[1]->id) ? $wip->accounts[1]->id : 0;
        // ...delivery
        $wip->delivery = $this->get_people_by_type_by_id('delivery', $id);
        $wip->delivery_lead1 = isset($wip->delivery[0]->name) ? $wip->delivery[0]->name : '';
        $wip->delivery_leads_x = $wip->delivery_lead1;
        $wip->delivery_leads_pdf = $wip->delivery_lead1;
        $wip->delivery_lead1_id = isset($wip->delivery[0]->id) ? $wip->delivery[0]->id : 0;

        // ...creatives
        $wip->creatives = $this->get_people_by_type_by_id('creatives', $id);
        $wip->lead_creative1 = isset($wip->creatives[0]->name) ? $wip->creatives[0]->name : '';
        $wip->lead_creative2 = isset($wip->creatives[1]->name) ? $wip->creatives[1]->name : '';
        $wip->lead_creatives_x = $wip->lead_creative1 . ($wip->lead_creative2 ? ' | ' . $wip->lead_creative2 : ''); 
        $wip->lead_creatives_pdf = $wip->lead_creative1 . ($wip->lead_creative2 ? ',' . $wip->lead_creative2 : '');
        $wip->lead_creative1_id = isset($wip->creatives[0]->id) ? $wip->creatives[0]->id : 0;
        $wip->lead_creative2_id = isset($wip->creatives[1]->id) ? $wip->creatives[1]->id : 0;
            
        // resource requests
        $wip->resource_requests_full = $this->get_resource_requests_by_id($id);
        $wip->resource_requests = array();
        $wip->resource_requests_client = array();
        foreach($wip->resource_requests_full as $key=>$value) {
            //if ($value->request!= '') 
            //    $wip->resource_requests[] = $value->request;
            if ($value->enc_request !=  '') {
                $wip->resource_requests[] = $this->wips_decrypt($value->enc_request);
                $wip->resource_requests_client[] = $value->client;
            }
        }    
        $wip->resource_requests_x = implode(" | ", $wip->resource_requests);
        
        // parent job data (should probably be another object???)
        $wip->number = $result->number;
        $wip->parent_number = $result->parent_number;
        $wip->is_parent = $wip->number == $wip->parent_number ? true : false;
        
         
        $job_number_display_array = array();
        $job_number_display_raw_array = array();
        if ($result->job_number_uk) {
            $job_number_display_array[] = "<span class='jnuk'>[UK]<br /><strong>" . substr($result->job_number_uk, 0, 15) . "</strong>" . substr($result->job_number_uk, 15) . "<span>";
            $job_number_display_raw_array[] = $result->job_number_uk;
        }
        if ($result->job_number_us) {
            $job_number_display_array[] = "<span class='jnus'>[US]<br /><strong>" . substr($result->job_number_us, 0, 15) . "</strong>" .substr($result->job_number_us, 15). "<span>";
            $job_number_display_raw_array[] = $result->job_number_us;
        }

        /* PS removed display of legacy data 07/07/20
        if ($result->job_number_uk_leg && $result->job_number_uk_leg != $result->job_number_uk) {
            $job_number_display_array[] = "<span class='jnuk'>[Legacy UK]<br />" . $result->job_number_uk_leg . "<span>";
            $job_number_display_raw_array[] = $result->job_number_uk_leg;
        }
        if ($result->job_number_us_leg && $result->job_number_us_leg != $result->job_number_us) {
            $job_number_display_array[] = "<span class='jnus'>[Legacy US]<br />" . $result->job_number_us_leg . "<span>";
            $job_number_display_raw_array[] = $result->job_number_us_leg;
        }
        */
        
        /*
        if ($result->job_number_old) {
            $job_number_display_array[] = "<span class='jnold'>" . $result->job_number_old . " [Legacy]<span>";
            $job_number_display_raw_array[] = $result->job_number_old;
        }
        */
        
        $wip->job_number_display = implode('<br />', $job_number_display_array );
        $wip->job_number_display_inline = implode(' / ', $job_number_display_array );
        $wip->job_number_display_raw_inline = implode(',', $job_number_display_raw_array );
        $wip->job_number_display_raw_inline2 = implode(' | ', $job_number_display_raw_array );
        
        
        if ($result->job_number_uk) {
            $wip->job_number_shorthand = $this->shorthand_alt_job_number_main($result->job_number_uk, $result->sector_name);
        }
        else if ($result->job_number_us) {
            $wip->job_number_shorthand = $this->shorthand_alt_job_number_main($result->job_number_us, $result->sector_name);
        }
        else {
            $wip->job_number_shorthand = $result->job_number_old;
        }
        
        $wip->type_id = $result->type;
        $wip->type = $result->job_type;
        
        $wip->title = trim($result->title);
        $wip->description = $result->description;
        
        $wip->client = $result->client;
        if (substr($result->title, 0, 2) == 'C,') {
            $title_explode = explode(',', $result->title);
            $wip->client = $title_explode[1];
        }

        $wip->job_name_legacy = false;
        $wip->job_name_legacy_hover = false;
        if ($result->title && ($result->title_leg != $result->title)) {
            $wip->job_name_legacy = $result->title_leg; 
            $wip->job_name_legacy_hover = "[Legacy job name]<br />" . $result->title_leg;  
        }

        $wip->brand = $this->wips_decrypt($result->enc_brand);
        
        $wip->account_handler = $this->wips_decrypt($result->enc_account_handler);
        
        $wip->digital = $result->digital;
        $wip->digital_formatted = $result->digital ? 'YES' : 'NO';
        
        $wip->sector_id = $result->sector;
        $wip->sector = $result->sector_name;
        
        //if ($wip->sector_id == 3) { // ctr 
        //    $wip->sector_badge = 'badge-sector-ctr-' . (($wip->type_id == 2) ? 'us' : 'uk'); 
        //}
        //else {
            $wip->sector_badge = 'badge-sector-' . strtolower($wip->sector);
        //}
        
        if (strtolower($wip->sector) == 'ctr') {
           $wip->sector_cell = 'sector-ctr' . ($wip->type == 'US' ? 'us' : 'uk'); 
        }    
        else {
           $wip->sector_cell = 'sector-' . strtolower($wip->sector); 
        }
            
        $wip->access_created = $result->access_created;
        $wip->access_modified = $result->access_modified;
        
        $wip->record_added_raw = $result->record_added;
        $wip->record_modified_raw = $result->record_modified;
        
        $wip->record_added_formatted = $this->format_date($result->record_added);
        $wip->record_modified_formatted = $this->format_date($result->record_modified);
        
        $wip->job_open_bool = $result->job_open;
        $wip->job_open = $result->job_open ? 'YES' : 'NO';
        
        $wip->interval = $this->day_diff($wip->created_on_raw, null, false);
        $wip->interval_style = $this->get_interval_style($wip->created_on_raw);
        
      //  $wip->finance_data = $this->get_job_finance_data($result->pjid);
        //$wip->total_costs_x = isset($wip->finance_data) && isset($wip->finance_data->total_costs) ? $wip->finance_data->total_costs : '';
      //  $wip->job_estimates = $this->get_job_estimates($result->pjid, strtolower($wip->type));
     //   $wip->total_costs_x = $wip->finance_data['totals']->total_costs;
     //   $wip->project_team = $this->get_job_project_team($result->pjid, strtolower($wip->type));
        
        return $wip;
    }
    
    public function shorthand_alt_job_number_main($alt_number, $sector) {
        
        $formatted = '';
        
        $alt_number_a = explode('-', $alt_number);
        
        $formatted = substr($alt_number, 0, 1) . ltrim($alt_number_a[1], '0'); // . '-' . $alt_number_a[2];

        if ($sector == 'CTR' && $alt_number_a[2] !='00' || $sector != 'CTR') {
            $formatted .= '-' . $alt_number_a[2];
        }
        
        if (count($alt_number_a) == 4) {
            $formatted = $formatted . '-' . $alt_number_a[3];
        }    

        /*
        if($sector == 'CTR' || $alt_number_a[2] != '00')
            $formatted .= '-' . $alt_number_a[2];
        */
        return $formatted; 
        
        
    }
    
    /**
     * get_interval_style
     * 
     * Return a style to use as a visual indication on the output table 
     * of when record was last updated 
     * 
     * @param timestamp $timestart When the record was updated
     * @param timestamp $timeend The interval end point, will default to 'now' if none
     * @return string
     */    
    public function get_interval_style($timestart, $timeend = null) {
        
        $timeend = $timeend ? $timeend : time();
        $int = $timeend - ($timestart + (7*60*60*24)); // difference in seconds...
        
        $days = ceil($int/(60*60*24));
        
        if ($days > 6) {
            $intstyle = 'lu7';
        }
        else if ($days > 0) {
            $intstyle = 'lu'.($days);
        }
        else {
            $intstyle = 'lu0';
        }
                
        return $intstyle;
    }
    
    /**
     * day_diff
     * 
     * Find the difference in days/hours between 2 times. 
     * Used for a textual description of when record was last updated.
     * 
     * @param timestamp $timestart When the record was updated
     * @param timestamp $timeend The interval end point, will default to 'now' if none
     * @param boolean $inc_hours Use hours in description (true) or just days (false) 
     * @return string
     */    
    public function day_diff($timestart, $timeend = null, $inc_hours) {
        
        $timeend = $timeend ? $timeend : time();
        $int = $timeend - $timestart; // difference in seconds...
        return $this->seconds_to_time($int, $inc_hours);
    }

    /**
     * seconds_to_time
     * 
     * Construct the string for the day_diff function
     * 
     * @param int $seconds Time interval in seconds
     * @param boolean $inc_hours Use hours in description (true) or just days (false) 
     * @return string
     */    
    public function seconds_to_time($seconds, $inc_hours = true) {
        $dtF = new \DateTime('@0');
        $dtT = new \DateTime("@$seconds");
        
        $days = $dtF->diff($dtT)->format('%a');
        $hours = $dtF->diff($dtT)->format('%h');
        
        $daystring = $days == 1 ? ' day ' : ' days ';
        $hourstring = $hours == 1 ? ' hour ' : ' hours ';
        
        if ($days == 0 && $hours == 0) {
            if ($inc_hours) {
                return 'less than 1 hour ago';
            }
            else {
                return '';
            }
        }
      
        if ($inc_hours) {
            return 'over ' . ($days ? $days . $daystring : '') . ($hours ? $hours . $hourstring : '') . 'ago';
        }
        elseif ($days) {
            return 'over ' . ($days ? $days . $daystring : '') . 'ago';
        }
        else {
            return '';
        }
    }
    
    
    public function format_hours($hours) {
        
        return +$hours . ' hrs';
        
    }
    
    
    
    /**
     * format_date
     * 
     * Format a timestamp to date/time string. 
     * Putting in a function keeps things consistent.  
     * We can add more here as we need them.
     * 
     * @param timestamp $timestamp Timestamp to format
     * @param int $format Required format
     * @return string
     */    
    public function format_date($timestamp, $format = 1) {
        
        switch($format) {
            
            case 2:
                return date('d.m.Y', $timestamp);
                break;
            
            default: 
            case 1:
                return date('d.m.Y H:i', $timestamp);
                break;
            
             
        }
        
    }
    
    /**
     * save_view_data
     * 
     * Save a new view against a user profile
     * 
     * @param string $title View title
     * @param string $description View description
     * @param string $public_private View type
     * @param boolean $set_default Is this user default view?
     * @param object $columns Columns visible/hidden settings
     * @param object $filters Filter settings
     * @param string $ordercol Which column order based upon
     * @param string $orderorder Column order asc/desc
     * @return integer $vid View record id
     */          
    public function save_view_data($title, $description, $public_private, $set_default, $columns, $filters, $ordercol, $orderorder, $viewsql) {
        
        $uid = $this->session->userdata('urid');
        
        // Save the view...
        $data = array(
            'title' => $title ? $title : 'User Initials ' . time(),
            'description' => $description, 
            'columns' => serialize($columns),
            'filters' => serialize($filters),
            'ordercol' => $ordercol,
            'orderorder' => $orderorder,
            'datetime' => time(),
            'uid' => 1,
            'private' => $public_private == 'private' ? 1 : 0,
            'public' => $public_private == 'public' ? 1 : 0,
            'sql' => $viewsql,
            /* CAPI-fied */
            '_uid' => $this->session->userdata('urid'),
            
        );
        $this->db->insert('views', $data);
        $vid = $this->db->insert_id(); 
        
        // ...if default set then clear the user view default values...
        if ($set_default == 1) {
            $this->db->where('_uid', $this->session->userdata('urid'));
            $this->db->update('users_views', array('default' => 0));
        }
        
        // ...and insert the user view record...
        $data2 = array(
            'vid' => $vid,
            'uid' => $uid,
            'default' => $set_default,
            'order' => 0,
            /* CAPI-fied */
            '_uid' => $uid,
        );
        $this->db->insert('users_views', $data2);
        
        return $vid;
    }
    
    /**
     * update_view_data
     * 
     * Update a view record
     * 
     * @param int $vid View record ID
     * @param string $title View title
     * @param string $description View description
     * @param string $public_private View type
     * @param boolean $set_default Is this user default view?
     * @param object $columns Columns visible/hidden settings
     * @param object $filters Filter settings
     * @param string $ordercol Which column order based upon
     * @param string $orderorder Column order asc/desc
     * @return integer $vid View record id
     */   
    public function update_view_data($vid, $title, $description, $public_private, $set_default, $columns, $filters, $ordercol, $orderorder, $viewsql) {
        
        $uid = $this->session->userdata('urid');
        
        // update the view...
        $data = array(
            'title' => $title ? $title : 'User Initials ' . time(),
            'description' => $description, 
            'columns' => serialize($columns),
            'filters' => serialize($filters),
            'ordercol' => $ordercol,
            'orderorder' => $orderorder,
            'datetime' => time(),
            'uid' => $uid,
            'private' => $public_private == 'private' ? 1 : 0,
            'public' => $public_private == 'public' ? 1 : 0,
            'sql' => $viewsql,
            /* CAPI-fied */
            '_uid' => $uid,
            
        );
        
        $this->db->where('vid', $vid);
        $this->db->update('views', $data); 
        
        // update the user view record with the default value
        /* CAPI-fied */
        if ($set_default == 1) {
            $this->db->where('_uid', $this->session->userdata('urid'));
            $this->db->update('users_views', array('default' => 0));
        } 
        
        $this->db->where('_uid', $this->session->userdata('urid'));
        $this->db->where('vid', $vid);
        $this->db->update('users_views', array('default' => $set_default));
        
        return $vid;
    }
   
    /**
     * update_view_data
     * 
     * Update a view record from the prompt screen rather than the form
     * 
     * @param int $vid View record ID
     * @param string $title View title
     * @param string $description View description
     * @param string $public_private View type
     * @param boolean $set_default Is this user default view?
     * @param object $columns Columns visible/hidden settings
     * @param object $filters Filter settings
     * @param string $ordercol Which column order based upon
     * @param string $orderorder Column order asc/desc
     * @return integer $vid View record id
     */   
    public function prompt_update_view_data($vid, $columns, $filters, $ordercol, $orderorder, $viewsql) {
        
        // update the view...
        $data = array(
            'columns' => serialize($columns),
            'filters' => serialize($filters),
            'ordercol' => $ordercol,
            'orderorder' => $orderorder,
            'datetime' => time(),
            'sql' => $viewsql
            
        );
        
        $this->db->where('vid', $vid);
        $this->db->update('views', $data); 
        
        return $vid;
    }
    

    /**
     * remove_view
     * 
     * Remove a view from a user - but keep in the view db
     * 
     * @param int $vid View record ID
     */
    public function remove_view($vid) {
        
        /* CAPI-fied */
        $this->db->where('vid', $vid);
        $this->db->where('_uid', $this->session->userdata('urid'));
        $this->db->delete('users_views');
        
        return;
    }
    
    
    /**
     * get_view_data
     * 
     * Get all data associated to a single view
     * 
     * @param int $vid View record ID
     * @return array
     */   
    public function get_view_data($vid) {
        
        $viewdata = array();
        
        $this->db->select('v.*');
        $this->db->select('uv.default');
        $this->db->from('views v');
        $this->db->join('users_views uv', 'uv.vid = v.vid', 'left');
        $this->db->where('v.vid', $vid);
        /* CAPI-fied */
        $this->db->where('uv._uid', $this->session->userdata('urid'));
        
        $query = $this->db->get();
        $row = $query->row(); 
        
        if ($row) {
            
            // handle new 'total_costs' column
            // inject into saved view if it is not already in there
            $cols = unserialize($row->columns);
            if (!isset($cols->total_costs)) {
                $carray = (array)$cols;
                // get the position correct....
                $newcarray = array_merge(array_slice($carray,0, 16), array('total_costs' => false), array_slice($carray,16));
                $cols = (object)$newcarray;
            }
            
            $viewdata = array(
                'vid' => $row->vid,
                'name' => $row->title,
                'description' => $row->description,
                'columns' => $cols, // unserialize($row->columns),
                'filters' => unserialize($row->filters),
                'ordercol' => $row->ordercol,
                'orderorder' => $row->orderorder,
                 /* CAPI-fied */
                'uid' => $row->_uid,
                'private' => $row->private,
                'public' => $row->public,
                'default' => $row->default
            );
            return $viewdata;
        }
        else {
            return 0;
        }
    }
    
    /**
     * get_all_active_persons
     * 
     * Get all active people and split into array of types.
     * This is used for the people selections on WIPS form.
     * 
     * @return array
     */   
    public function get_all_active_persons() {
        
        $persons = array();
        
        $this->db->select('wp.tid, wpt.type, wp.id, wp.name');
        $this->db->from('wips_persons wp');
        $this->db->join(' wips_persons_type wpt', 'wpt.tid = wp.tid', 'left');
        $this->db->where('wp.active', 1);
        $this->db->order_by('wp.tid', 'ASC');
        $this->db->order_by('wp.sticky', 'DESC');
        $this->db->order_by('wp.name', 'ASC');
        
        $query = $this->db->get();
     
        foreach ($query->result() as $row) {
          
            $key = strtolower(str_replace(' ', '_', $row->type));
            
            $persons[$key][] = (object)array(
                'id' => $row->id,
                'name' => $row->name,
                'type' => $row->type, 
            );
                 
        } 
        
        $persons['delivery_filter'] = array_merge($persons['dpm'], $persons['account_handler']);
        return $persons; 
    }
    
    /**
     * get_user_views
     * 
     * Get all views saved against a user account
     * 
     * @param integer $uid User record ID
     * @return array
     */   
    public function get_user_views($uid = null) {
        
        $uid = $uid ? $uid : $this->session->userdata('urid');
        
        $views = array();
          
        $this->db->select('v.vid, v.title, v.description');
        $this->db->from('users_views uv');
        $this->db->join('views v', 'v.vid = uv.vid', 'left');
        /* CAPI-fied */
        $this->db->where('uv._uid', $uid);
        $this->db->order_by('uv.default', 'desc');
        $this->db->order_by('uv.order', 'asc');
        $this->db->order_by('v.datetime', 'asc');
        
        $query = $this->db->get();
     
        foreach ($query->result() as $row) {
          
            $views[] = (object)array(
                'id' => $row->vid,
                'title' => $row->title, // . ' (' . $row->vid . ')',
                'description' => $row->description,
            );
        } 
        
        return $views; 
    
    }
    
    /**
     * lock_wip
     * 
     * Lock a WIP record so it can't be opened for updating by another user.
     * 
     * @param integer $pjid Job record ID
     */   
    public function lock_wip($pjid) {

        $userdata = $this->session->userdata('user');
        
        $data = array(
            'pjid' => $pjid,
            'locked_by_uid' => $this->session->userdata('urid'),
            'locked_on' => time(),
            /* CAPI-fied */
            '_locked_by_uid' => $this->session->userdata('urid'),
            '_locked_by_name' => $userdata->full_name,
        );
        
        $this->db->insert('wips_lock', $data);
    }
    
    /**
     * unlock_wip
     * 
     * Unlock a WIP record.
     * 
     * @param integer $pjid Job record ID
     */   
    public function unlock_wip($pjid) {

        if ($pjid == 0) {
            $this->db->empty_table('wips_lock'); 
        }
        else {
            $this->db->where('pjid', $pjid);
            $this->db->delete('wips_lock');
        }
        return;
    }
    
    /**
     * unlock_wip_bu_userid
     * 
     * Unlock any WIPs records locked by a particular user.
     * 
     * @param integer $uid User record ID
     */   
    public function unlock_wip_bu_userid($uid) {
        /* CAPI-fied */
        $this->db->where('_locked_by_uid', $uid);
        $this->db->delete('wips_lock');
        return;
    }

    /**
     * is_wip_locked
     * 
     * Check to see if a WIP is locked or not. 
     * If locked return lock data. Else return 0
     * 
     * @param integer $pjid Job record ID
     * @return array/boolean
     */   
    public function is_wip_locked($pjid) {
        
        /* CAPI-fied */
        $this->db->select('wl.pjid, wl._locked_by_uid, wl._locked_by_name,  wl.locked_on');
        $this->db->from('wips_lock wl');
        //$this->db->join('users u', 'u.urid = wl.locked_by_uid');
        $this->db->where('wl.pjid', $pjid);
        $this->db->where('wl.locked_by_uid !=', $this->session->userdata('urid'));
        
        $query = $this->db->get();
        $row = $query->row(); 
        
        if ($row) {
            
           /* CAPI-fied */
            $lockdata = array(
                'locked_pjid' => $pjid,
                'locked_by_id' => $row->_locked_by_uid,
                //'locked_by_name' => $this->wips_decrypt($row->enc_full_name),
                'locked_by_name' => $row->_locked_by_name,
                'locked_on' => $row->locked_on,
                'locked_on_formatted' => $this->format_date($row->locked_on),
                'locked_on_time_ago' => $this->day_diff($row->locked_on, null, true)
            );
            
            return $lockdata;
        }
        else {
            return false;
        }
    }
    
    /**
     * get_locked_wips
     * 
     * Get all wips records that have been locked for editing.
     * These are saved in db table wips_lock
     * 
     * @return array of objects
     */   
    public function get_locked_wips() {
        
        $wips = array(); 
        
        /*
        $this->db->select('wl.*, u.*');
        $this->db->from('wips_lock wl');
        $this->db->join('users u', 'u.urid = wl.locked_by_uid');
        $this->db->order_by('wl.locked_on', 'desc');
        */

        /* CAPI-fied */
        $this->db->select('wl.*');
        $this->db->from('wips_lock wl');
        $this->db->order_by('wl.locked_on', 'desc');
        

        $query = $this->db->get();
      
        foreach ($query->result() as $row) {
          
            /* CAPI-fied */
            $wips[] = (object)array(
                'locked_pjid' => $row->pjid,
                'locked_wips_record' => $this->get_job($row->pjid),
                'locked_by_id' => $row->_locked_by_uid,
                'locked_by_name' => $row->_locked_by_name,
                'locked_on' => $row->locked_on,
                'locked_on_formatted' => $this->format_date($row->locked_on),
                'locked_on_time_ago' => $this->day_diff($row->locked_on, null, true)
            );
        } 
        
        return $wips;
        
    }
   
    /**
     * wips_encrypt
     * 
     * Encrypt a string using CI encrypt library 
     * 
     * @param string $string String to encrypt
     * @return string The encrypted string
     */   
    public function wips_encrypt($string) {
        if (!$string || $string == '') {
            return $string;
        }
        else {
            return rtrim($this->encrypt->encode($string));
        }
    }

    /**
     * wips_decrypt
     * 
     * Decrypt a string using CI encrypt library 
     * 
     * @param string $string The Encrypted string
     * @return string The Decrypted string
     */   
    public function wips_decrypt($string) {
        if (!$string || $string == '') {
            return $string;
        }
        else {
            return $this->encrypt->decode($string);
        }
    }

    /**
     * get_people_list
     * 
     * Get all the 'people' records used for wips form dropdowns:
     * 'Account Handlers', 'Lead Delivery' and 'Lead Creative'
     * 
     * @return array of objects
     */   
    public function get_people_list() {
        
        $people = array(); 
        
        $this->db->select('wp.*, wpt.type');
        $this->db->from('wips_persons wp');
        $this->db->join('wips_persons_type wpt', 'wpt.tid  = wp.tid');
        $this->db->order_by('wp.tid', 'asc');
        $this->db->order_by('wp.name', 'asc');
        
        $query = $this->db->get();
      
        foreach ($query->result() as $row) {
          
            $people[] = (object)array(
                'id' => $row->id,
                'name' => $row->name,
                'type_id' => $row->tid,
                'type_name' => $row->type,
                'active' => $row->active
            );
        } 
        return $people;
    }
    
    /**
     * save_people_data
     * 
     * Save a 'people' record -  used in wips form dropdowns:
     * 'Account Handlers', 'Lead Delivery' and 'Lead Creative'
     * 
     * @param string $name Person name
     * @param integer $type Person type/category 
     * @param boolean/integer $active (1 => 'active', 0 => 'inactive') 
     * @return integer $id Inserted record id
     */   
     public function save_people_data($name, $type, $active) {
        
        $data = array(
            'name' => $name,
            'tid' => $type, 
            'active' => $active,
            'sticky' => 0,
        ); 
         
        $this->db->insert('wips_persons', $data);
        $id = $this->db->insert_id(); 
        return $id;
    }
 
    /**
     * toggle_people_active
     * 
     * Toggle 'people' record active status (1 => 'active', 0 => 'inactive')
     * 
     * @param integer $id Record ID
     * @param integer $active Current active status 
     * @return integer $newactive New active status following 'toggle'
     */   
     public function toggle_people_active($id, $active = 0) {
        
        $newactive = $active ? 0 : 1;  
         
        $this->db->where('id', $id);
        $this->db->update('wips_persons', array('active' => $newactive));
        
        return $newactive;
    }
    
    /**
     * is_in_maintenance
     * 
     * Is the site in maintenance mode.
     * 
     * @return boolean
     */   
    public function is_in_maintenance() {
        
        $this->db->select('*');
        $this->db->from('wips_settings');
        $this->db->where('type', 'maintenance');
        
        $query = $this->db->get();
        $row = $query->row(); 
        
        if ($row) {
            return $row->value;
        }
        else {
            return false;
        }
        
    }
    
    
}  
