<?php
class User_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    
    /**
     *  
     * CAPI-fied
     * 
     * validate_user_by_credentials
     * 
     * Validates posted login credentials against valid active user accounts.
     * Returns array including status and user object if valid login
     *
     * @return array 
     */
    public function validate_user_by_credentials_c() {
        
        $si_email = $this->input->post('si_email');
        $si_password = $this->input->post('si_password');
        $apiTokenEndPoint = $this->config->item('apiTokenEndPoint');

        if (!$si_email || !$si_password) {
           return array('status' => false, 'email' => $si_email, 'reason' => 'Please enter a valid email address and/or password [01]');  
        }

        // Set up and execute the curl process
        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $apiTokenEndPoint);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, array(
            'grant_type' => 'password',
            'username' => $si_email,
            'password' => $si_password
        ));

        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);

        $result = json_decode($buffer);

        if ((isset($result->status) && $result->status == 'error') || !isset($result->userInfo)) {    

            $this->session->set_flashdata('email', $si_email);
            $this->session->set_flashdata('login-failed', $result->error_description . ' [02]');
            redirect('../', 'refresh');
            exit;
        }
        else {

            // valid user, but now to see if they have access to job bags 
            $access_test1 = $this->user_has_access($result->access_token, 'creative-wips', 'access');
            $access_test2 = $this->user_has_access($result->access_token, 'prod-wips', 'access');

            if(!$access_test1 && !$access_test2) {
                $this->session->set_flashdata('email', $si_email);
                $this->session->set_flashdata('login-failed', "You do not have permissions to access Creative WIPS tool [03]");
                redirect('../', 'refresh');
                exit;
            }

            $roles = array();
            foreach($result->userInfo->roles as $key=>$role) {
                $roles[] = $role->role_name;
            }

            // set roles into cookie
            setcookie('cw_roles', implode(' ', $roles), 0, '/');

            // set user token for use with API connection
            setcookie('cw_access_token', $result->access_token, 0, '/');

            // get users default view...
            $views = $this->wips->get_user_views($result->userInfo->uid); 
            $default_vid = count($views) ? $views[0]->id : 0;

            // define user type based on new roles....
            
            // set a name based on 
            $user = $result->userInfo;

            // add to user object to align with curent build
            $user->user_type_name = $result->userInfo->job_title ? $result->userInfo->job_title : '';

            // set perms based on the new CAPI roles
            /*  
            admin (2)
            admin-super (1)
            creative-wips-admin (18)
            creative-wips-author (17)
            creative-wips-viewer (16)
            prod-wips-admin (21)
            prod-wips-author (20)
            prod-wips-viewer (19)
            */
            // default all to 0
            
            $access = false; 

            $perms = (object)array(
                'wips' => (object)array('view'=> 0, 'create'=> 0, 'update' => 0),
                'prod' => (object)array('view'=> 0, 'create'=> 0, 'update' => 0),
                'admin' => (object)array('users'=> 0, 'finance'=> 0, 'locked'=> 0, 'peoplesel' => 0),
            );

            if (in_array('admin-super', $roles) ||  in_array('admin', $roles)) {
                $perms = (object)array(
                    'wips' => (object)array('view'=> 1, 'create'=> 1, 'update' => 1),
                    'prod' => (object)array('view'=> 1, 'create'=> 1, 'update' => 1),
                    'admin' => (object)array('users'=> 1, 'finance'=> 1, 'locked'=> 1, 'peoplesel' => 1)
                );
                $access = true; 
            }   
            else {
                if(in_array('creative-wips-admin', $roles)) {
                    $perms->wips = (object)array('view'=> 1, 'create'=> 1, 'update' => 1);
                    $perms->admin = (object)array('users'=> 1, 'finance'=> 1, 'locked'=> 1, 'peoplesel' => 1);
                    $perms->prod->view = 1;
                    $access = true; 
                }    
                else if (in_array('creative-wips-author', $roles)) {
                    $perms->wips = (object)array('view'=> 1, 'create'=> 1, 'update' => 1);
                   // $perms->prod->view = 1;
                    $access = true; 
                }
                else if (in_array('creative-wips-viewer', $roles)) {
                    $perms->wips->view = 1;
                    $access = true; 
                }
                
                if(in_array('prod-wips-admin', $roles)) {
                    $perms->prod = (object)array('view'=> 1, 'create'=> 1, 'update' => 1);
                    $perms->admin = (object)array('users'=> 1, 'finance'=> 1, 'locked'=> 1, 'peoplesel' => 1);
                    $perms->wips->view = 1;
                    $access = true; 
                }    
                else if (in_array('prod-wips-author', $roles)) {
                    $perms->prod = (object)array('view'=> 1, 'create'=> 1, 'update' => 1);
                    $perms->wips->view = 1;
                    $access = true; 
                }
                else if (in_array('prod-wips-viewer', $roles)) {
                    $perms->wips->view = 1;
                    $perms->prod->view = 1;
                    $access = true; 
                }

              
            }      
            
            if (!$access) {
                $this->session->set_flashdata('email', $si_email);
                $this->session->set_flashdata('login-failed', "You do not have permissions to access Creative WIPS tool [04]");
                redirect('../', 'refresh');
                exit;
            }

            $user->perms = $perms;

            // no re-set old user types based on new roles (and see if this makes a difference???? )
            /*   1 => Administrator
                 2 => Accounts
                 3 => Traffic
                 4 => Viewer
                 5 => DPM
                 6 => Producer
            */
            $user_type = 4; // default to viewer
            if (in_array('admin-super', $roles) || in_array('admin', $roles) || in_array('creative-wips-admin', $roles) || in_array('prod-wips-admin', $roles)) {
                $user_type = 1;
            }
            else if (in_array('creative-wips-author', $roles)) {
                $user_type = 2;
            }
            else if (in_array('prod-wips-author', $roles)) {
                $user_type = 6;
            }
            $user->user_type = $user_type;
 
            return array('status' => true,  'email' => $si_email,  'user' => $result->userInfo, 'utype' => $user_type, 'reason' => 'Active Match found.', 'default_view' => $default_vid);
         
        }
       
    }
    

     /* CAPI-fied to check user has access */
     public function user_has_access($token, $location, $permission) {

        $apiEndPoint = $this->config->item('apiEndPoint');

        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $apiEndPoint);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, array(
            'graph'=> 'userHasPermissionsAtLocation',
            'category'=> 'LanglandToolsService',
            'access_token' => $token,
            'location'=> $location,
            'permission_name'=> $permission,
        ));
           
        $buffer = curl_exec($curl_handle);
        $result = json_decode($buffer);
        if(count($result->data) > 0) {
            return true;
        }
        else { 
            return false;
        }
    }

    /**
     * validate_user_by_credentials
     * 
     * Validates posted login credentials against valid active user accounts.
     * Returns array including status and user object if valid login
     *
     * @return array 
     */
    /*
    public function validate_user_by_credentials__________() {
        
        $si_email = $this->input->post('si_email');
        $si_password = $this->input->post('si_password');
        
        if (!$si_email || !$si_password) {
           return array('status' => false, 'email' => $si_email, 'reason' => 'Please enter a valid email address and/or password');  
        }
        
        // only checks against password initially as email address is encrypted...
        $this->db->where('password', md5($si_password));
        $this->db->select('*');
        $this->db->from('users');
        $query = $this->db->get();
        
        $result = false;
        
        // then checks on de-crypted email address
        foreach ($query->result() as $row) {
          
            if($this->wips->wips_decrypt($row->enc_email_address) == $si_email) {
                $row->email_address = $this->wips->wips_decrypt($row->enc_email_address);
                $row->full_name = $this->wips->wips_decrypt($row->enc_full_name);
                $result = $row;
                break; 
            }
        } 
        
        if ($result) {
            
            if ($result->status) {
                
                // get users default view...
                $views = $this->wips->get_user_views($result->urid); 
                $default_vid = count($views) ? $views[0]->id : 0;
                
                $login_count =  $result->login_count;
                // update last login here...
                $this->db->where('urid', $result->urid);
                $this->db->update('users', array('last_login' => time(), 'login_count' => $login_count+1)); 
                
                return array('status' => true,  'email' => $si_email,  'user' => $this->get_user($result->urid), 'utype' => $result->user_type, 'reason' => 'Active Match found.', 'default_view' => $default_vid);
            }
            else {
                return array('status' => false,  'email' => $si_email, 'user' => $this->get_user($result->urid), 'utype' => $result->user_type, 'reason' => 'Account blocked.');
            }
        }
        else {
            
        //    if ($this->is_user_registered_already($si_email)) {
        //        return array('status' => false, 'email' => $si_email, 'reason' => 'Incorrect password entered, please try again or contact finance to reset your password.'); 
        //    }
        //    else {
                return array('status' => false, 'email' => $si_email, 'reason' => 'User account not found. Please try again.'); 
        //    }
        }
    }
    */

    
    /**
     * get_user
     * 
     * Returns a user object
     *
     * @param integer $urid The User record id
     * @return object or false
     */
    /*
    public function get_user($urid) {
        
        $this->db->where('urid', $urid);
        $this->db->select('u.*');
        $this->db->select('ut.description as role');
        $this->db->from('users u');
        $this->db->join('user_types ut', 'ut.utid = u.user_type', 'left');
      
        $query = $this->db->get();
        $result = $query->row();
        if ($result != '' and !empty($result)) {
            $user = (object)array(
                'urid' => $result->urid,
                'email_address' => $this->wips->wips_decrypt($result->enc_email_address),
                'full_name' => $this->wips->wips_decrypt($result->enc_full_name),
                'job_title' => $result->job_title, 
                'department' => $result->department,
                'location' => $result->location,
                'user_type' => $result->user_type,
                'user_type_name' => $result->role,
                'status' => $result->status,
                'status_name' => $result->status ? 'Active' : 'Blocked',
                'created_on' => $result->created_on,
                'created_on_formatted' => $this->wips->format_date($result->created_on, 1),
                'created_by_id' => $result->created_by_id,
                'last_login' => $result->last_login,
                'last_login_formatted' => $result->last_login ? $this->wips->format_date($result->last_login, 1) : 'never',
                'login_count' => $result->login_count,
                'perms' => json_decode($result->permissions)
            );
            
            return $user;
        } 
        else
            return false;
    }
    */
       
    /**
     * is_user_registered_already
     * 
     * Checks whether a user is registered or not based upon their email address
     *
     * @param string $email Email address to validate against
     * @return integer (user record id) or false
     */
    /*
    public function is_user_registered_already($email) {
       
        $this->db->where('email_address', $email);
        $this->db->select('*');
        $this->db->from('users');
        $query = $this->db->get();
        $result = $query->row();    
        if ($result) {
            return $result->urid;
        } 
        else {
            return false;
        }
    }
    */
    /**
     * get_last_log_in
     * 
     * Return timestamp if users last login
     *
     * @param integer $urid User Record ID
     * @return integer last login UNIX timestamp
     */
    /*
    public function get_last_log_in($urid) {
        
        $this->db->where('urid', $urid);
        $this->db->select('last_login');
        $this->db->from('users');
        $query = $this->db->get();
        $result = $query->row();
        return $result->last_login; 
    }
    */
   
    /**
     * check_logged_in
     * 
     * Checks if a user is logged in or not based on session data
     * If not then redirects to login page with flashdata message, otherwise return true  
     *
     * @return boolean true if logged in
     */
    /*
    public function check_logged_in() {
        
        if (!$this->session->userdata('logged_in')) {
            $message = "Please log in to access site content.";
            $this->session->set_flashdata('login-failed', "<p class='inline-message'>" . $message . "</p>");
            redirect('../', 'refresh');
            exit;
        }
        return true;
    }
    */
    /**
     * is_site_admin
     * 
     * Checks if logged in user is a site admin or not based on session data
     *
     * @return boolean true if user is a site admin
     */
    /*
    public function is_site_admin() {
        
        if ($this->session->userdata('user_type') != 1) {
            redirect('../', 'refresh');
            exit;
        }
        return true;
    }
    */
    
    /**
     * is_user_active
     * 
     * Checks if a user account is active
     *
     * @param integer $urid User Record ID
     * @return integer status 1/0
     */
    /*
    public function is_user_active($uid) {
        
        $this->db->where('urid', $uid); 
        $this->db->select('status');
        $this->db->from('users');
        $query = $this->db->get();
        $result = $query->row();
        return $result->status;   

    }
    */
    
    
    /**
     * get_all_users
     * 
     * Get all system users
     *
     * @return array of user objects
     */
    /*
    
     public function get_all_users() {
        
        $users = array();
        
        $this->db->select('u.*');
        $this->db->from('users u');
        $this->db->order_by('u.user_type', 'ASC');
        $this->db->order_by('u.urid', 'ASC');
        //$this->db->order_by('u.full_name', 'ASC');
        
        $query = $this->db->get();

        foreach ($query->result() as $row) {
            $users[] = $this->get_user($row->urid);
        }
        
        return $users;
    }
    */

    /**
     * save_user
     * 
     * Save a user account
     *
     * @param integer $urid User Record ID
     * @param string $full_name
     * @param string $email_address
     * @param string $job_title
     * @param string $departmemt
     * @param string $location
     * @param string $password
     * @param integer $user_type
     * @param boolean $status (1 => active, 0 => blocked) 
     * @return integer $urid
     */
    /*
    public function save_user($urid, 
                $full_name, 
                $email_address, 
                $job_title,
                $department,
                $location,
                $password,
                $user_type,
                $status,
                $perms) {
        
        if ($urid) { // it is an update.....
            
            $data = array(
                'job_title' => $job_title,
                'department' => $department,
                'location' => $location,
                'user_type' => $user_type,
                'status' => $status,
                'enc_email_address' => $this->wips->wips_encrypt($email_address),
                'enc_full_name' => $this->wips->wips_encrypt($full_name),
                'permissions' => json_encode($perms)
            );
            
            $this->db->where('urid', $urid);
            $this->db->update('users', $data); 
            
        }
        else { // it is a new user so an insert....
            
            $data = array(
                'login_count' => 0,
                'job_title' => $job_title,
                'department' => $department,
                'location' => $location,
                'password' => $password,
                'user_type' => $user_type,
                'status' => $status,
                'created_on' => time(),
                'created_by_id' => $this->session->userdata('urid'),
                'last_login' => 0,
                'enc_email_address' => $this->wips->wips_encrypt($email_address),
                'enc_full_name' => $this->wips->wips_encrypt($full_name),
                'permissions' => json_encode($perms)
            );
            
            $this->db->insert('users', $data);
            $urid = $this->db->insert_id();
            
            // New user so add default user views here...
            $this->add_default_views($urid);
        }
        
        return $urid;
    }
    */

    /**
     * add_default_views
     * 
     * Add a set of pre-defined views to a new user account.
     * These pre-defined view settings are saved in db table 'views_default'
     *
     * @param integer $urid User Record ID
     */
    public function add_default_views($urid) {
        
        $mytime = time();
        
        $this->db->select('*');
        $this->db->from('views_default');
        $this->db->order_by('vid', 'ASC');
        $query = $this->db->get();

        $order = 1;
        
        foreach ($query->result() as $row) {
            // build the dataset, 
            $vdata = array(
                'title' => $row->title,
                'description' => $row->description,
                'columns' => $row->columns, 
                'filters' => $row->filters,
                'ordercol' => $row->ordercol,
                'orderorder' => $row->orderorder,
                'sql' => $row->sql,
                'datetime' => $mytime,
                'uid' => 0, //$urid,
                'private' => 1,
                'public' => 0,
                /* CAPI-fied */
                '_uid' => $urid,
            );
            
            // insert into views table and get vid
            $this->db->insert('views', $vdata);
            $vid = $this->db->insert_id();

            // insert into user views
            $uvdata = array(
                'vid' => $vid,
                'uid' => 0, //$urid,
                'default' => 0,
                'order' => $order,
                /* CAPI-fied */
                '_uid' => $urid,
            );
            
            $this->db->insert('users_views', $uvdata);
            $order++;
        }
    }
    
    /**
     * check_access_allowed
     * 
     * Simple check for allowing access to app. This check the session data. 
     */
    /*
    public function check_access_allowed() {
        if (!$this->session->userdata('logged_in')) {
            redirect('../', 'refresh');
            return;
        }
        return;
    }
    */

    /**
     * create_random_password
     * 
     * Create a random passwprd of x length
     *
     * @param integer $length Length of password (defaulted to 7)
     * @return string $password The generate password 
     */
    /*
    public function create_random_password($length = 7) {
        $chars = "abcdefghijkmnopqrstuvwxyz023456789";
        srand((double) microtime() * 1000000);
        $i = 0;
        $pass = '';

        while ($i <= $length) {
            $num = rand() % 33;
            $tmp = substr($chars, $num, 1);
            $pass = $pass . $tmp;
            $i++;
        }

        return $pass;
    }
    */
    /**
     * save_user_password
     * 
     * Save a new user password
     *
     * @param integer $urid user record id
     * @param string $password md5 converted password string
     * @return boolean true
     */
    /*
    public function save_user_password($urid, $password) {
   
        $this->db->where('urid', $urid);
        $this->db->update('users', array('password' => $password)); 
        
        return true;
    }
    */
    
}