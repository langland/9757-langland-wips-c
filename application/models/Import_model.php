<?php

class Import_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    
    /**
     * get_pjtab_name
     * 
     * Get the parent job data table name.
     * This was put in a function as could be variable during development. 
     * [can probably be hard-coded now] 
     * 
     * @return string
     */
    public function get_pjtab_name() {
        $parent_job_tab = 'parent_jobs';
        //$parent_job_tab = 'parent_jobs_test';
        return $parent_job_tab;
    }
    
    /**
     * create_backup_table
     * 
     * Back up the current table before updating so we can rollback if need be
     * Not being used any longer. Using file backups instead.
     * 
     * @param string $table table name to backup
     * @return string
     */
    public function create_backup_table($table) {
        
        $newtable = date('Ymd_His') . '_bak_' . $table;
        
        $sql = "CREATE TABLE " . $newtable . " (id INT NOT NULL AUTO_INCREMENT, PRIMARY KEY (id))
                SELECT * FROM " . $table;
        $query = $this->db->query($sql);
        return $newtable;
    }
    
    /****
     ****    'ACTIVE JOB IMPORT' functions
     ****/
    
    /**
     * upload_csv
     * 
     * Uploads and processes active jobs data from finance csv.
     * This function relates to the admin page 'Active Jobs Import' 
     * No return data but does display summary 'flash' message on completion
     */
    public function upload_csv()
    {
        /* removing db backup - now backed up in files 
        // create a backup table....
        $backupdbnames = array();
        $backupdbnames[] = $this->create_backup_table($this->get_pjtab_name());
        $backupdbnames[] = $this->create_backup_table('wips_jobs');
        */
        
        // back up to file also....
        $mybackup = $this->admin->backup_tables();
        $backupfilename = $_SERVER['DOCUMENT_ROOT'] . '/_bups/' . date('Ymd_His') . '_jobswipsbup.sql';
        $handle = fopen($backupfilename, 'w' );
        fwrite($handle,$mybackup);
        fclose($handle);
        
        // empty the csv 'helper' table just in case it is not empty
        $this->empty_import_csv_table();
        
        // put the file into 'helper' table and then process....
        $csv_content = array();
        
        $file = $_FILES['userfile']['tmp_name'];
        $filename = $_FILES['userfile']['name'];
        
        // load data into temporary table import_csv
        $sql = "LOAD DATA LOCAL INFILE '" . $file ."'
                INTO TABLE import_csv
                FIELDS TERMINATED by ',' LINES TERMINATED BY '\n'";
        $query = $this->db->query($sql);
        
        // get number of records
        $record_count = $this->get_import_records_count();
        
        // clean the data - remove any eronious entries we know may be in there
        $cleaned = $this->clean_import();
        
        // set up some messages
        if ($cleaned && $record_count) {
            $message = $this->process_import($filename, $backupfilename);
        }
        else {
            $message = '<p>ERROR: no data imported.</p>';
        }
         
        // empty the csv 'helper' table again now it is done
        $this->empty_import_csv_table();
        
        // add record to history....
        $idata = array(
            'datetime' => time(),
            'method' => 'manual upload',
            'type' => 'active jobs import',
            'run_by' => $this->session->userdata('urid'),
            'summary' => $message,
        //    'backuptable' => implode(', ', $backupdbnames), // table backup removed.
            'backupfile' => $backupfilename,
        );
        
        // save the process data
        $this->db->insert('import_history', $idata);
        
        // set up the message display
        $this->session->set_flashdata('import-message', $message);
        redirect('../admin/access_import', 'refresh');
    }
   
    /**
     * clean_import
     * 
     * Clean up the import data before processing.
     * This relies on certain 'marker' values existing in pre-defined columns.
     * 
     * Col D => 'A'
     * Col O => 'D'
     * Col Q => 'R'
     * Col R => '1'
     * 
     * @return true
     */
    public function clean_import() {
        
        // get rid of those mysterious records with 'S' in them...
        $this->db->delete("import_csv", array('D' => 'S'));
        $this->db->delete("import_csv", array('D' => 'R'));
        
        $this->db->select("*");
        $this->db->from("import_csv");
        
        $query = $this->db->get();        
        
        // process and format the data the save in jobs table
        foreach ($query->result() as $row) {
            
            if ($row->D != 'A' || $row->O != 'D' || $row->Q != 'R' || $row->R != '1') { // then  'marker' values are not in the right place. 
                
                // convert to array
                $row_a = array();
               
                foreach($row as $key=>$value) {
                    $row_a[] = $value;
                }
                
                // clean the row..
                $cleansed = $this->clean_row($row_a);
  
                // reset row array values with cleansed data
                $row_ass = array(
                    'A' => $cleansed[0],
                    'B' => $cleansed[1],
                    'C' => $cleansed[2],
                    'D' => $cleansed[3],
                    'E' => $cleansed[4],
                    'F' => $cleansed[5],
                    'G' => $cleansed[6],
                    'H' => $cleansed[7],
                    'I' => $cleansed[8],
                    'J' => $cleansed[9],
                    'K' => $cleansed[10],
                    'L' => $cleansed[11],
                    'M' => $cleansed[12],
                    'N' => $cleansed[13],
                    'O' => $cleansed[14],
                    'P' => $cleansed[15],
                    'Q' => $cleansed[16],
                    'R' => $cleansed[17],
                    'S' => $cleansed[18],
                    'T' => $cleansed[19],
                    'U' => isset($cleansed[20]) ? $cleansed[20] : '',
                    'V' => isset($cleansed[21]) ? $cleansed[21] : '',
                    'W' => isset($cleansed[22]) ? $cleansed[22] : '',
                    'X' => isset($cleansed[23]) ? $cleansed[23] : '',
                    'Y' => isset($cleansed[24]) ? $cleansed[24] : '',
                    'Z' => isset($cleansed[25]) ? $cleansed[25] : '',
                );
                
                // means we need to clean....
                $this->db->where('A', $row->A);
                $this->db->update('import_csv', $row_ass);
                
            }
        }
        return 1;
    }

    /**
     * clean_row
     * 
     * Clean an individual row by iterating through until the 'markers' are in 
     * their correct columns. 
     * Return the 'clean' row
     * 
     * @param array $raw_a the row to 'clean'
     * @return array
     */
    function clean_row($row_a) {
        
        if ($row_a[3] != 'A') {
            do {
                unset($row_a[3]);
                $row_a = array_values($row_a);
            } while ($row_a[3] != 'A');
        }
        
        if ($row_a[14] != 'D') {
            do {
                unset($row_a[14]);
                $row_a = array_values($row_a);
            } while ($row_a[14] != 'D');
        }
        
        if ($row_a[16] != 'R') {
            do {
                unset($row_a[16]);
                $row_a = array_values($row_a);
            } while ($row_a[16] != 'R');
        }

        if ($row_a[17] != '1') {
            do {
                unset($row_a[17]);
                $row_a = array_values($row_a);
            } while ($row_a[17] != '1');
        }
        
        $row_a_count = 26 - count($row_a);
        
        for ($i=0; $i<$row_a_count; $i++) {
            $row_a[] = '';
        }
        return $row_a;
        
    }
    
    /** 
     * process_import
     * 
     * Data should now be clean so process the import.
     * Returns a processing summary string
     * 
     * @param string $filename upload file name
     * @param string $backupfilename name of backup file generated
     * @return string 
     */
    public function process_import($filename, $backupfilename) {
        
        $records_processed = 0;
        $records_inserted = 0;
        $records_updated = 0;
        $records_no_action = 0;
        $records_re_opened = 0;
        
        // get the csv data and prepare for processing
        $this->db->select("*");
        $this->db->from("import_csv");
        $query = $this->db->get();        
        
        // process and format the data the save in jobs table
        foreach ($query->result() as $row) {
            
            // counter
            $records_processed++;
        
            // generate the sql dataset
            $myrecord = $this->generate_sql_dataset($row);
            
            // get parent record... 
            $parent_record = $this->get_parent_job_record($myrecord['number']);
                        
            if (!$parent_record) {
                // INSERT
                $this->db->insert($this->get_pjtab_name(), $myrecord); 
                $records_inserted++;
            }
            elseif ($parent_record->job_open == 0) {
                // RE-OPEN
                $this->db->where('id', $parent_record->id);
                $this->db->update($this->get_pjtab_name(), array('job_open'=> 1)); 
                $records_re_opened++;
            }
            elseif($id = $this->parent_jobs_record_updated($myrecord['number'], $myrecord['access_modified'])) {
                // UPDATE
                unset($myrecord['record_added']);
                unset($myrecord['number']);
                $this->db->where('id', $id);
                $this->db->update($this->get_pjtab_name(), $myrecord); 
                $records_updated++;
            } 
            else {
                $records_no_action++;
            }
        }
     
        // Now the logic to close any jobs no longer appearing in the CSV import...
        $records_closed = $this->close_parent_job_records();
        
        // generate summary message and return
        $message = "<p>Process complete: <ol>"
                . "<li>Upload file name: " . $filename . "</li>"
              //  . "<li>Back-up table created: " . implode(', ', $backupdbnames) . "</li>" // removed
                . "<li>Back-up file: " . $backupfilename . "</li>"
                . "<li>CSV Records processed: " . $records_processed . "</li>"
                . "<li>Records INSERTED: " . $records_inserted . "</li>"
                . "<li>Records UPDATED: " . $records_updated . "</li>"
                . "<li>Records NO ACTION: " . $records_no_action . "</li>"
                . "<li>Records CLOSED: " . $records_closed->counter . " " . ($records_closed->counter ? '[' . implode(', ', $records_closed->jobs) . ']'   : '') . "</li>"
                . "<li>WIPs COMPLETED: "  . $records_closed->wips_completed_counter . "</li>"
                . "<li>Records RE-OPENED: " . $records_re_opened . "</li>"
                . "</ol></p>";
        
        return $message; 
    }
    
    /**
     * close_parent_job_records
     * 
     * Close any parent jobs if they no longer appear in the import data
     * 
     * @return object
     */
    public function close_parent_job_records() {

        $counter = 0;
        $jobs = array(); 
        $wips_completed_counter = 0;
        
        // find the records...
        $sql = "SELECT pj.id, pj.number 
                    FROM " . $this->get_pjtab_name() . " pj
                    WHERE pj.number NOT IN (SELECT A FROM import_csv)
                    AND pj.job_open != 0";
        
        $query = $this->db->query($sql);

        // loop through and update...
        foreach ($query->result_array() as $row) {
            
            $this->db->where('id', $row['id']);
            $this->db->update($this->get_pjtab_name(), array('record_modified'=>time() , 'job_open'=>0));
            $counter++;
            $jobs[] = $row['number'];
        
            // ...and see if there is an active wips record too - if so then set to 'COMPLETED'
            $wips_completed_counter += $this->is_wips_open($row['id']);
            
        }
        
        // return object of data
        return (object)array(
            'counter' => $counter,
            'jobs' => $jobs,
            'wips_completed_counter' => $wips_completed_counter,
        );
    }
    
    /**
     * is_wips_open
     * 
     * Test if a particular WIPs record is currently open
     * 'complete' the wips records if the job is automatically closed.
     * 
     * @param integer $pjid parent job number
     * @return boolean true/false
     */
    public function is_wips_open($pjid) {
        
        $sql = "SELECT wj.* FROM wips_jobs wj JOIN wips_jobs_status wjs ON wjs.wj_id = wj.id " . 
            "WHERE wj.pjid=" . $pjid . " AND wj.current=1 AND wjs.ws_id < 5";
        $query = $this->db->query($sql);
        $result = $query->row();
        
        if ($result) {
            // update current to 0
            $this->db->where('id', $result->id);
            $this->db->where('current', 1);
            $this->db->update('wips_jobs', array('current' => 0)); 
            
            // insert a new version with 'COMPLETED' status
            $status_notes = 'Set to COMPLETED automatically by import process as Job is now CLOSED';
            
            $data = array(
                'pjid' => $result->pjid, 
                'enc_status_notes' => $this->wips->wips_encrypt($status_notes),
                'live_date' => NULL,
                'no_items' => NULL,
                'countries' => NULL,
                'created_on' => time(),
                'created_by' => 0, 
                'current' => 1
            );
            $this->db->insert('wips_jobs', $data);
            $wj_id = $this->db->insert_id(); 
        
            // add status to wips_job_status table
            $this->db->insert('wips_jobs_status', array(
                'wj_id' => $wj_id,
                'ws_id' => 5)
            );
            
            return 1; 
        }
        return 0; 
    }
    
    
    /**
     * parent_jobs_record_updated
     * 
     * If job has been updated then update the record
     * 
     * @param integer $job_number
     * @param date $last_modified
     * @return integer - record id if found. 0 (false) if not.
     */
    public function parent_jobs_record_updated($job_number, $last_modified) {
        
        $query = $this->db->query("SELECT id FROM " . $this->get_pjtab_name() . " WHERE number = '" . $job_number . "' AND access_modified != '" . $last_modified . "' AND job_open = 1");
        $result = $query->row();
        if ($result) 
            return $result->id;
        else 
            return 0;
        
    }

    /**
     * get_parent_job_record
     * 
     * Get a parent job by job number
     * 
     * @param integer $job_number
     * @return object/false
     */
    public function get_parent_job_record($job_number) {
        
        $sql = "SELECT * FROM " . $this->get_pjtab_name() . " WHERE number = '" . $job_number . "'";
        $query = $this->db->query($sql);
        $result = $query->row();
        if ($result) {
            return $result;
        }
        else {
            return false;
        }
        
    }
    
    /**
     * get_parent_job_id
     * 
     * Get a parent job id by job number
     * 
     * @param integer $job_number
     * @return integer/false 
     */
    public function get_parent_job_id($job_number) {
        
        $sql = "SELECT id FROM " . $this->get_pjtab_name() . " WHERE number = '" . $job_number . "'";
        $query = $this->db->query($sql);
        $result = $query->row();
        if ($result) {
            return $result->id;
        }
        else {
            return false;
        }
        
    }
    
    /**
     * generate_sql_dataset
     * 
     * Generate the SQL statement for INSERT/UPDATE based on the clean row.
     * 
     * @param object $row
     * @return array
     */
    public function generate_sql_dataset($row) {
        
        $now = time();
        
            $myrecord = array(); 
            
            $myrecord['number'] = $row->A;
            $myrecord['type'] = $this->get_type($row->A);
            $myrecord['title'] = $row->B;
            $myrecord['description'] = $row->C;
            
            $myrecord['record_added'] = $now;
            $myrecord['record_modified'] = $now;
            $myrecord['job_open'] = 1;
            
            $myrecord['client'] = $row->E;
            $myrecord['brand'] = $row->F;
            $myrecord['account_handler'] = $row->G;
            $myrecord['digital'] = $row->I == 'DIGITAL' ? 1 : 0;
            $myrecord['sector'] = $this->get_sector($row->K);
            $myrecord['parent_number'] = $row->L;
            $myrecord['access_created'] = trim($row->S);
            $myrecord['access_modified'] = trim($row->T);

            // handle encryted entries
            $myrecord['enc_brand'] = $this->wips->wips_encrypt($myrecord['brand']);
            $myrecord['enc_account_handler'] = $this->wips->wips_encrypt($myrecord['account_handler']);
            unset($myrecord['brand']);
            unset($myrecord['account_handler']);
            
            return $myrecord;
    }
    
    /**
     * get_sector
     * 
     * Get sector ID based on Sector name within the data
     * 
     * @param string $sector
     * @return integer
     */
    public function get_sector($sector) {
        
        $sid = 5; // default to OTHER...

        switch ($sector) {
            
            case 'CORPORATE':
                $sid = 1;
                break;
            
            case 'CONSUMER':
                $sid = 2;
                break;
            
            case 'PAR':
            case 'NEW PAR':
                $sid = 3;
                break;
            
            case 'ETHICAL':
                $sid = 4;
                break;
            
            default:
                $sid = 5;
                break;
            
        }
        
        return $sid;
    }
    
    /**
     * get_type
     * 
     * get job type based on job number structure
     * 
     * @param string $number
     * @return integer 
     */
    public function get_type($number) {
        
        $tid = 5; // OTHER
        
        if (strlen($number)==6 && substr($number, 0, 1)=='1') {
            $tid = 2; // US
        } 
        elseif (strlen($number)==5) {
            $tid = 1; // UK
        }
        elseif (strlen($number)==4 && substr($number, 0, 1)=='8') {
            $tid = 4; // CHARITY
        }
        elseif (strlen($number)==4 && substr($number, 0, 1)=='9') {
            $tid = 3; // INTERNAL
        }
        else {
            $tid = 5; // OTHER
        } 

        return $tid;
    }
    
    /**
     * get_import_records_count
     * 
     * @return integer
     */
    public function get_import_records_count() {
        
        $query = $this->db->query("SELECT COUNT(*) as count FROM import_csv");
        $result = $query->row();
        return $result->count; 
        
    }
    
    /**
     * empty_import_csv_table
     * 
     * Empty import 'helper' table
     */
    public function empty_import_csv_table() {
        
        $this->db->truncate('import_csv');
        return; 
        
    }
    
    /****
     ****    End of 'ACTIVE JOB IMPORT' functions
     ****/
    
    
    /**
     * temporary function to delete import data to whilst testing the process..
     */
    /* DELETE
    public function dev_empty_data() {
        
        $this->empty_import_csv_table();
        $this->db->truncate($this->get_pjtab_name());
        
        $this->session->set_flashdata('empty-message', "<p>Tables emptied: <ul><li>import_csv</li><li>parent_jobs</li></ul></p>");
        redirect('../admin/access_import', 'refresh');
        
    }
     * 
     */
    
    
    /****
     ****   'FINANCE DATA IMPORT' functions
     ****/
    
    
    /**
     * get_master_project_date_time
     * 
     * Retrieve the finance data csv date/time stamp. 
     * This is held with the csv as a row in format....
     *  'Master Project Report as at 04/12/2017 at 14:30:30'
     * 
     * @return array/false
     */
    public function get_master_project_date_time() {
       
        $query = $this->db->query("SELECT A FROM import_csv2 WHERE A LIKE 'Master Project%'");
        $result = $query->row();
        if ($result) {
            $mpr_a = explode(' ', $result->A);
            return array(
                'date' => date('Y-m-d', strtotime(str_replace('/','-',$mpr_a[5]))),
                'time' => $mpr_a[7]
            );
        }
        return false;
    }
    
     /**
     * upload_csv2
     * 
     * Uploads and processes finance data from supplied csv.
     * This function relates to the admin page 'Finance Data Import' 
     * No return data but does display summary 'flash' message on completion
     */
    public function upload_csv2()
    {
        $message = '';
        
        $process = $_POST['process']; // both , wips , dahsboard
        
        // back up to file
        $mybackup = $this->admin->backup_tables();
        $backupfilename = $_SERVER['DOCUMENT_ROOT'] . '/_bups/' . date('Ymd_His') . '_financebup.sql';
        $handle = fopen($backupfilename, 'w' );
        fwrite($handle,$mybackup);
        fclose($handle);
        
        // empty the csv 'helper' table before we start just in case it is not empty
        $this->empty_import_csv2_table();
        
        // put the file into a database table and then process....
        $csv_content = array();
        
        // upload file
        $file = $_FILES['userfile']['tmp_name'];
        $filename = $_FILES['userfile']['name'];
        
        // insert data into 'helper' table 'import_csv2'
        $sql = "LOAD DATA LOCAL INFILE '" . $file ."'
                INTO TABLE import_csv2
                FIELDS TERMINATED by ',' LINES TERMINATED BY '\n'";
                
        $query = $this->db->query($sql);
        
        // get the date/time stamp
        $report_date_time = $this->get_master_project_date_time(); 
        // 
        // **** should look at breaking the process and returning if this date stamp is not available ****
        //
        
        // delete the rows we dont want - where col A is not numeric
        $sql = "DELETE FROM import_csv2 WHERE A REGEXP '[^0-9].+'"; 
        $query = $this->db->query($sql);
        
        // clean the import - looking for broken title - this will be indicated by column E not being a date string..
        $cleaned = $this->clean_import2();
        
        // save the raw data as encryted into a new table...
        
        /****
         **** START OF Dashboard Reports Data processing
         ****/
        $dashboard_messages = array();
        
        if ($process == 'both' || $process == 'dashboard') {
            
            $dashboard_records_count = 0;

            // format date suffix    
            $date_for_tables = str_replace('-', '', $report_date_time['date']);

            // new table name 
            $newtable = '_dashboard_import_' . $date_for_tables;

            // if table already exists then drop it and we'll start again
            $query = $this->db->query("DROP TABLE IF EXISTS " . $newtable);

            // create table mysql
            $sql = "CREATE TABLE " . $newtable . " 
                (id INT NOT NULL AUTO_INCREMENT, 
                    job_no VARCHAR(200), 
                    client VARCHAR(200), 
                    sector VARCHAR(20), 
                    job_name VARCHAR(1000), 
                    created_on VARCHAR(15), 
                    year VARCHAR(4),
                    digital VARCHAR(1),
                    on_hold VARCHAR(1),
                    us_job VARCHAR(1),
                    estimate VARCHAR(200),
                    total_costs VARCHAR(200),
                    time VARCHAR(200),
                    bought_in VARCHAR(200),
                    committed VARCHAR(200),
                    invoiced VARCHAR(200),
                    PRIMARY KEY (id))";

            $query = $this->db->query($sql);

            // set a message
            $dashboard_messages[] = "Dashboard table created: " . $newtable;

            // no select all form the 'helper table' 
            $this->db->select('*');
            $this->db->from('import_csv2');
            $query = $this->db->get();

            foreach ($query->result() as $row) {

                // counter 
                $dashboard_records_count++;

                // construct the insert data array WITH ENCRYPTION ...
                $data = array(
                    'job_no' => $this->wips->wips_encrypt($row->A),
                    'client' => $this->wips->wips_encrypt($row->B),
                    'sector' => ($row->C == 'PAR' || $row->C == 'NEW PAR') ? 'CTR' : $row->C,
                    'job_name' => $this->wips->wips_encrypt($row->D),
                    'created_on' => $row->E,
                    'year' => substr($row->E, 6, 4),
                    'digital' => $row->Q == 'DIGITAL' ? 1 : 0, 
                    'on_hold' => $row->R == 'On Hold' ? 1 : 0,
                    'us_job' => $row->A > 99999 ? 1 : 0,
                    'estimate' => $this->wips->wips_encrypt($row->G),
                    'total_costs' => $this->wips->wips_encrypt($row->J),
                    'time' => $this->wips->wips_encrypt($row->K),
                    'bought_in' => $this->wips->wips_encrypt($row->L),
                    'committed' => $this->wips->wips_encrypt($row->M),
                    'invoiced' => $this->wips->wips_encrypt($row->N),
                ); 
                // ... and insert
                $this->db->insert($newtable, $data);

            }

            $dashboard_messages[] = "Dashboard records processed: " . $dashboard_records_count;

            // reset and add this record to the imports lookup table
            $this->db->delete("_dashboard_imports", array('suffix' => $date_for_tables));
            $this->db->insert("_dashboard_imports", array(
                'suffix' => $date_for_tables,
                'date_string' => $report_date_time['date'],
                'time_string' => $report_date_time['time'],
                'uploaded' => time(),
                'uid' => $this->session->userdata('urid'),
            ));
            
        }
        
        /****
         **** END OF Dashboard Reports Data processing
         ****/
        
        /****
         **** START OF WIPs Finance Data processing
         ****/
        
        $finance_messages = array();
        
        if ($process == 'both' || $process == 'wips') {
        
            // select the data we need from the 'helper' table
            $this->db->select('A as job_no, G as sales_estimate, J as total_costs, K as agency_time, L as third_party_bought_in, M as third_party_committed, N as invoiced');
            $this->db->from('import_csv2');

            $query = $this->db->get();

            $finance_record_count = 0; 
            $insert_count = 0;
            $update_count = 0;

            // process the records
            foreach ($query->result() as $row) {

                // counter
                $finance_record_count++;

                // get parent record id 
                $pj_id = $this->get_parent_job_id($row->job_no);

                if ($pj_id) {

                    // build the update/insert data array WITH ENCRYPTION
                    $fdata = array(
                        'number' => $row->job_no, 
                        'enc_total_costs' => $this->wips->wips_encrypt($row->total_costs),
                        'enc_agency_time_cost' => $this->wips->wips_encrypt($row->agency_time),
                        'enc_third_party_bought_in' => $this->wips->wips_encrypt($row->third_party_bought_in),
                        'enc_third_party_committed' => $this->wips->wips_encrypt($row->third_party_committed),
                        'enc_invoiced' => $this->wips->wips_encrypt($row->invoiced),
                        'enc_estimated' => $this->wips->wips_encrypt($row->sales_estimate),
                        'estimated' => $row->sales_estimate,
                        'updated' => time(),
                    );

                    if ($this->finance_record_exists($pj_id)) {
                        // update
                        $this->db->where('pj_id', $pj_id);
                        $this->db->update('wips_finance_data', $fdata);
                        $update_count++;
                    }
                    else { 
                        // insert
                        $fdata['pj_id'] = $pj_id;
                        $this->db->insert('wips_finance_data', $fdata);
                        $insert_count++;
                    }
                }
            }

            $finance_messages[] = "WIPS Finance records processed: " . $finance_record_count; 
            $finance_messages[] = "WIPS Finance records INSERTED: " . $insert_count; 
            $finance_messages[] = "WIPS Finance records UPDATED: " . $update_count;

        }
        /****
         **** END OF WIPs Finance Data processing
         ****/
        
        // empty the csv 'helper' table just in case it is not empty
        $this->empty_import_csv2_table();
        
        // generate summary message and return
        $message = "<p>Process complete: <ol>"
                . "<li>Upload file name: " . $filename . "</li>"
                . "<li>Upload file date (time) stamp: " . $report_date_time['date'] . " (" . $report_date_time['time'] . ")</li>"
                . "<li>Back-up file: " . $backupfilename . "</li>";
                
        if (count($finance_messages)) {
            foreach($finance_messages as $key=>$valuemessage) {
                $message .=  "<li>" . $valuemessage . "</li>";
            }
        }
                
        if (count($dashboard_messages)) {
            foreach($dashboard_messages as $key=>$valuemessage) {
                $message .=  "<li>" . $valuemessage . "</li>";
            }
        }
        $message .= "</ol></p>";
        
        // add record to history....
        $idata = array(
            'datetime' => time(),
            'method' => 'manual upload',
            'type' => 'finance data import',
            'run_by' => $this->session->userdata('urid'),
            'summary' => $message,
            'backuptable' => '',
            'backupfile' => $backupfilename,
        );
        
        // save the process data
        $this->db->insert('import_history', $idata);
      
        // return message
        $this->session->set_flashdata('import-message2', $message);
        redirect('../admin/access_import2', 'refresh');
    }
    
    /**
     * empty_import_csv2_table
     * 
     * Empty import 'helper' table
     */
    public function empty_import_csv2_table() {
    
        $this->db->truncate('import_csv2');
        return; 
    
    }
    
    /**
     * finance_record_exists
     * 
     * Does finance record exist. Find out based on record count. 
     * 
     * @param integer $pj_id parent job id
     * @return integer 
     */
    public function finance_record_exists($pj_id) {
        
        $query = $this->db->query("SELECT COUNT(*) as count FROM wips_finance_data WHERE pj_id=". $pj_id);
        $result = $query->row();
        return $result->count; 
        
    }
    
    /**
     * clean_import2
     * 
     * Clean up the import data before processing.
     * This relies on checking for a date in column E,
     * This needs doing as the job name column may include commas and so pushes data out
     * 
     * @return true
     */
    public function clean_import2() {
        
        $this->db->select("*");
        $this->db->from("import_csv2");
        $query = $this->db->get();     
        
        // loop through the table
        foreach ($query->result() as $row) {
             
            // if we can't find a date structure in col E...
            if (!strpos($row->E, '/')) {
                
                // convert to array
                $row_a = array();
               
                foreach($row as $key=>$value) {
                    $row_a[] = $value;
                } 
                
                // and send to the cleaning funciton
                $cleansed = $this->clean2_row($row_a);
                
                // rebuild the array witgh cleansed data
                $row_ass = array(
                    'A' => $cleansed[0],
                    'B' => $cleansed[1],
                    'C' => $cleansed[2],
                    'D' => $cleansed[3],
                    'E' => $cleansed[4],
                    'F' => $cleansed[5],
                    'G' => $cleansed[6],
                    'H' => $cleansed[7],
                    'I' => $cleansed[8],
                    'J' => $cleansed[9],
                    'K' => $cleansed[10],
                    'L' => $cleansed[11],
                    'M' => $cleansed[12],
                    'N' => $cleansed[13],
                    'O' => $cleansed[14],
                    'P' => $cleansed[15],
                    'Q' => $cleansed[16],
                    'R' => $cleansed[17],
                    'S' => $cleansed[18],
                    'T' => $cleansed[19],
                    'U' => isset($cleansed[20]) ? $cleansed[20] : '',
                    'V' => isset($cleansed[21]) ? $cleansed[21] : '',
                    'W' => isset($cleansed[22]) ? $cleansed[22] : '',
                    'X' => isset($cleansed[23]) ? $cleansed[23] : '',
                    'Y' => isset($cleansed[24]) ? $cleansed[24] : '',
                    'Z' => isset($cleansed[25]) ? $cleansed[25] : ''
                );
                
                // update the row with cleansed data
                $this->db->where('A', $row->A);
                $this->db->update('import_csv2', $row_ass);
            }
        }
        return 1;
        
    }
    
    
    /**
     * clean_row
     * 
     * Clean an individual row by iterating through until the the date field
     * is in the correct column. 
     * Return the 'clean' row
     * 
     * @param array $row_a the row to 'clean'
     * @return array
     */
    function clean2_row($row_a) {
        
        if (!strpos($row_a[4], '/')) {
            do {
                $row_a[3] .= ' ' . $row_a[4];
                unset($row_a[4]);
                $row_a = array_values($row_a);
            } while (!strpos($row_a[4], '/'));
        }
        return $row_a; 
    }
    
    
}