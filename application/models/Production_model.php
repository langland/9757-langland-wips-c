<?php

class Production_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    
    /**
     * search_jobs
     * 
     * Search jobs, this will search job number, parent job number and title against the input
     *
     *  pj.job_open = 1 
            AND
     * 
     * @param string $string Inputed search string 
     * @return array of objects
     */    
    /* will be replaced by CAPI search 
       public function search_jobs($string) {
        
        $jobs = array();
        
        $query = $this->db->query("
            SELECT pjn.*, pj.id as mypjid
            FROM parent_jobs_numbers pjn 
            LEFT JOIN parent_jobs pj ON pj.id = pjn.pjid
            WHERE 
            (pjn.job_number LIKE '%" . $string . "%' 
            OR pjn.job_number_alt LIKE '%" . $string . "%' 
            OR pjn.job_number_alt_us LIKE '%" . $string . "%' 
            OR pjn.job_number_alt_uk LIKE '%" . $string . "%'
            OR pjn.job_number_alt_us_leg LIKE '%" . $string . "%' 
            OR pjn.job_number_alt_uk_leg LIKE '%" . $string . "%'
            OR pjn.search_title LIKE '%" . $string . "%' 
            OR pjn.search_title_leg LIKE '%" . $string . "%') AND pjn.job_number_alt_uk != '' LIMIT 50");
       
            //AND pj.active = 1

        foreach ($query->result() as $row) {
        
            $rec = $this->wips->get_job($row->pjid);
            $rec->prod_records = $this->get_production_records_summary($row->mypjid); 

            $jobs[] = $rec;
        
        }
        
        return $jobs;
    }
     */          

     

    public function build_production_job_search_list($jobs) {

        

        $jobs_list = array();

        foreach($jobs as $key=>$job) {

        //    if ($job->active == '1') {


                $pjid = $this->wips->get_pjid_from_pjid_capi($job->id);
                
            //    print "xx" . $pjid . 'yy'; exit;
              //  $rec = $this->wips->get_job($pjid);
                $job->prod_records = $this->get_production_records_summary($pjid); 
                $job->job_number_display = $this->wips->get_capi_job_numbers_display($job->numbers);
                $job->title =$job->job_name;
                $job->job_type = $job->location_name;
                $job->client = $job->client_name_short;
                $job->brand = $job->brand_name;
                $job->job_sector = $job->discipline_name ? $job->discipline_name : $job->sector_name;
                $job->job_sector_raw = $job->discipline_id != '0' ? $job->discipline_id + 5 : $job->sector_id;
                $job->account_handler = $job->lead_account_handler_name;
                $job->parent_jobs_id = $pjid;
                $jobs_list[] = $job;
                

        //    }

        }
        return $jobs_list;

    }
     
   

    
    /**
     * get_production_records_table
     * 
     * Get the production records for the main table display
     *
     * @param string $action type of table 'active' or 'closed'
     * @param array $filters array of filter values - lead producers / support producers / production types  
     * @return array of objects
     */    
    public function get_production_records_table($action = 'active', $filters = NULL) { 
        
        $filters = json_decode($filters);
        
        $closed_states = array(9,10); 
        
        $production_records = [];
        
        $this->db->select('pr.pr_id, pr.pr_pid, prr.pjid');
        $this->db->from('production_record pr');
        $this->db->join('production_record_ref prr', 'pr.pr_pid = prr.pr_pid');
        $this->db->join('production_record_type prt', 'pr.pr_id = prt.pr_id');
        $this->db->join('production_record_stage prs', 'pr.pr_id = prs.pr_id');
        $this->db->join('parent_jobs pj', 'pj.id = prr.pjid');
        
        if ($action == 'closed') {
            $this->db->where('prs.ps_id IN (' . implode(',', $closed_states) . ')');
        }
        else {
            $this->db->where('prs.ps_id NOT IN (' . implode(',', $closed_states) . ')');
        }
        
        if (isset($filters->lead_producers) && count($filters->lead_producers)) {
            $this->db->where('pr.lead_producer IN (' . implode(',', $filters->lead_producers) . ')');
        }
        
        if (isset($filters->support_producers) && count($filters->support_producers)) {
            $this->db->where('pr.support_producer IN (' . implode(',', $filters->support_producers) . ')');
        }
        
        if (isset($filters->production_types) && count($filters->production_types)) {
            $this->db->where('prt.pt_id IN (' . implode(',', $filters->production_types) . ')');
        }
        
        if (isset($filters->priority_jobs) && $filters->priority_jobs == true) {
            $this->db->where('pr.priority_job', 1);
        }
        
        $this->db->where('pr.current', 1);
        $this->db->group_by('pr.pr_pid');
        $this->db->order_by('pj.client', 'ASC');
        
        $query = $this->db->get();
        
        foreach ($query->result() as $row) {
            
            $jobinfo = $this->wips->get_job($row->pjid, false);
            $record = $this->get_production_record($row->pr_id);
            
            $record['number'] = $jobinfo->number;
            $record['job_number_display'] = $jobinfo->job_number_display;
            $record['job_number_display_raw_inline'] = $jobinfo->job_number_display_raw_inline;
            $record['job_number_shorthand'] = $jobinfo->job_number_shorthand;
            $record['client'] = $jobinfo->client;
            $record['title'] = $jobinfo->title;
            $record['sector'] = $jobinfo->sector;
            $record['sector_cell'] = $jobinfo->sector_cell;
            $record['job_type'] = $jobinfo->job_type;
            $record['job_sector_raw'] = $jobinfo->job_sector_raw;
            $record['job_sector'] = $jobinfo->job_sector;
            $record['brand'] = $jobinfo->brand;
            $record['is_parent'] = $jobinfo->is_parent;
            $record['wips_count'] = $jobinfo->wips_count;
            //$record['project_team'] = $jobinfo->project_team;

            $record['job_name_legacy'] = $jobinfo->job_title_leg ? "[Legacy job name]<br />" . $jobinfo->job_title_leg : false;
            $record['job_number_hover_display'] = $jobinfo->job_number_hover_display;

            $record['capi_jid'] = $jobinfo->capi_jid;
            
            $production_records[] = $record;
        }
        
        return $production_records;
    }
    
    /**
     * get_production_records_summary
     * 
     * Get the production records summary search base don parent job id.
     *
     * @param int $pjid parent job id
     * @return array
     */    
    public function get_production_records_summary($pjid) {
        
        $production_records = [];
        
        $this->db->select('pr.*');
        $this->db->from('production_record pr');
        $this->db->where('prr.pjid', $pjid);
        $this->db->where('pr.current', 1);
        $this->db->join('production_record_ref prr', 'prr.pr_pid = pr.pr_pid');
        
        $query = $this->db->get();
        
        foreach ($query->result() as $row) {
            
            $types_info = $this->get_production_record_types($row->pr_id);
            $row->types_names_string = implode(", ", $types_info['types_names']);
            $production_records[] = $row;
        }
        
        return $production_records;
    }
  
    /**
     * get_production_records
     * 
     * Get full production records based on production record 'parent' id
     *
     * @param int $pr_pid production record 'parent' id
     * @return array of objects
     */    
    public function get_production_records($pr_pid) {
        
        $production_records = array();
        
        $this->db->select('pr.pr_id');
        $this->db->from('production_record pr');
        $this->db->where('pr.pr_pid', $pr_pid);
        $this->db->order_by('pr.current', 'ASC');
        $this->db->order_by('pr.created_on', 'ASC');
        
        $query = $this->db->get();
        
        $record_count = 0;
        
        foreach ($query->result() as $row) {
  
            $prod_rec = $this->get_production_record($row->pr_id);    
            if ($record_count > 0) {  // check forupdated
            
                $prod_rec['updated'] = array(
                    'types' => strcmp($prod_rec['types_names_string'], $production_records[$record_count-1]['types_names_string'])!=0 ? 1 : 0,
                    'description' => strcmp($prod_rec['description'], $production_records[$record_count-1]['description']) !=0 ? 1 : 0,
                    'stage' => strcmp($prod_rec['stage_id'],$production_records[$record_count-1]['stage_id']) !=0 ? 1 : 0,
                    'status_notes' => strcmp($prod_rec['status_notes'], $production_records[$record_count-1]['status_notes']) !=0 ? 1 : 0,
                    'deadline' => strcmp($prod_rec['deadline_raw'], $production_records[$record_count-1]['deadline_raw']) !=0 ? 1 : 0,
                    'agreed_budget' => strcmp($prod_rec['agreed_budget'], $production_records[$record_count-1]['agreed_budget']) !=0 ? 1 : 0,
                   'negotiated_costs' => strcmp($prod_rec['negotiated_costs'],$production_records[$record_count-1]['negotiated_costs']) !=0  ? 1 : 0,
                    'producer_hours' =>  strcmp($prod_rec['producer_hours'], $production_records[$record_count-1]['producer_hours']) !=0 ? 1 : 0,
                    'partner' => strcmp($prod_rec['creative_partner_id'], $production_records[$record_count-1]['creative_partner_id']) !=0 ? 1 : 0,
                    'talent' => strcmp($prod_rec['talent'], $production_records[$record_count-1]['talent']) !=0 ? 1 : 0,
                    'nda' => strcmp($prod_rec['nda'], $production_records[$record_count-1]['nda']) !=0 ? 1 : 0,
                    'usage_form' => strcmp($prod_rec['usage_form'], $production_records[$record_count-1]['usage_form']) !=0 ? 1 : 0,
                    'file_directory_path' => strcmp($prod_rec['file_directory_path'], $production_records[$record_count-1]['file_directory_path']) !=0 ? 1 : 0,
                    'lead_producer' => strcmp($prod_rec['lead_producer_id'], $production_records[$record_count-1]['lead_producer_id']) !=0 ? 1 : 0,
                    'support_producer' => strcmp($prod_rec['support_producer_id'], $production_records[$record_count-1]['support_producer_id']) !=0 ? 1 : 0,
                    'lead_account_handler' => strcmp($prod_rec['lead_account_handler_id'], $production_records[$record_count-1]['lead_account_handler_id']) !=0 ? 1 : 0,
                    'lead_creative' => strcmp($prod_rec['lead_creative_id'], $production_records[$record_count-1]['lead_creative_id']) !=0 ? 1 : 0,
                    'priority_job' => strcmp($prod_rec['priority_job'], $production_records[$record_count-1]['priority_job']) !=0 ? 1 : 0,
                ); 
            }
        
            $production_records[$record_count] = $prod_rec;
            $record_count++;
        
        }
        
        $production_records_r = array_reverse($production_records);
        
        return $production_records_r;
    }
    
    /**
     * get_production_record
     * 
     * Get full production record based on production record id
     *
     * @param int $pr_id production record id
     * @return object
     */    
    public function get_production_record($pr_id) {

        $production_record = array();
        
     //   $this->db->select('pr.*, ps.stage, prs.ps_id, pp.partner, prp.pp_id');
        $this->db->select('pr.*, ps.stage, prs.ps_id');
        $this->db->from('production_record pr');
        $this->db->join('production_record_stage prs', 'prs.pr_id = pr.pr_id');
        $this->db->join('production_stage ps', 'ps.id = prs.ps_id');
       // $this->db->join('production_record_partners prp', 'prp.pr_id = pr.pr_id');
       // $this->db->join('production_partners pp', 'pp.id = prp.pp_id');
        $this->db->where('pr.pr_id', $pr_id);
        
        $query = $this->db->get();
        $row = $query->row(); 

        if($row) {
            
            $types_info = $this->get_production_record_types($row->pr_id);
            $partners_info = $this->get_production_record_partners($row->pr_id);
            
            $description_raw = $this->wips->wips_decrypt($row->enc_description);
            $description_raw2  = str_replace(PHP_EOL, ' ' . PHP_EOL, $description_raw);
            $description_explode = explode(' ', $description_raw2);
            $description_wc = count($description_explode);
            $description_html_full = nl2br($description_raw);
            
            if ($description_wc > $this->config->item('prod_word_limit')) {
                $description_trimmed_a = array_slice($description_explode, 0, $this->config->item('prod_word_limit'));
                $description_html_trimmed = nl2br(implode(' ', $description_trimmed_a)) . '...';
                $description_trimmed = 1;
            }
            else {
                $description_html_trimmed = $description_html_full;
                $description_trimmed = 0;
            }
        
            $status_notes_raw =  $this->wips->wips_decrypt($row->enc_status_notes);
            $status_notes_raw2 = str_replace(PHP_EOL, ' ' . PHP_EOL, $status_notes_raw);
            $status_notes_explode = explode(' ',  $status_notes_raw2);
            $status_notes_wc = count($status_notes_explode);
            $status_notes_html_full = nl2br($status_notes_raw);
            
            if ($status_notes_wc > $this->config->item('prod_word_limit')) {
                $status_notes_trimmed_a = array_slice($status_notes_explode, 0, $this->config->item('prod_word_limit'));
                $status_notes_html_trimmed = nl2br(implode(' ', $status_notes_trimmed_a)) . '...';
                $status_notes_trimmed = 1;
            }
            else {
                $status_notes_html_trimmed = $status_notes_html_full;
                $status_notes_trimmed = 0;
            }
            
            
            $production_record = array(
                'pr_id' => $row->pr_id, // production record id
                'pr_pid' => $row->pr_pid, // production record parent id
                'pjid' => $this->get_pjid_from_pr_id($row->pr_id), // parent job id 
                
                'created_on' => $row->created_on,
                'created_on_formatted' => $this->wips->format_date($row->created_on),
                
                //'created_by_uid' => $row->created_by,
                //'created_by_name' => $this->wips->get_user_name_by_uid($row->created_by),
                /* CAPI-fied */
                'created_by_uid' => $row->_created_by,
                'created_by_name' => $row->_created_by_name,
                
                'current' => $row->current,
                
                'lead_producer_id' => $row->lead_producer,
                'lead_producer_name' => $this->get_people_name_by_id($row->lead_producer),
                'support_producer_id' => $row->support_producer, 
                'support_producer_name' => $this->get_people_name_by_id($row->support_producer),
                
                'description' => $description_raw,
                'description_html' => $description_html_full,
                'description_html_limited' => $description_html_trimmed,
                'description_trimmed' => $description_trimmed,
                
                'deadline' => ($row->deadline && $row->deadline != '0000-00-00' && $row->deadline != '1970-01-01') ? date('d.m.Y', strtotime($row->deadline)) : '',
                'deadline_raw' => ($row->deadline && $row->deadline != '0000-00-00' && $row->deadline != '1970-01-01') ? strtotime($row->deadline) : 0,
                
                'status_notes' => $status_notes_raw,
                'status_notes_html' => $status_notes_html_full,
                'status_notes_html_limited' => $status_notes_html_trimmed,
                'status_notes_trimmed' => $status_notes_trimmed,
                
                'agreed_budget' => (int)$this->wips->wips_decrypt($row->enc_agreed_budget),
                'agreed_budget_formatted' => $row->enc_agreed_budget ? number_format((int)$this->wips->wips_decrypt($row->enc_agreed_budget)) : '',
                
                'negotiated_costs' => (int)$this->wips->wips_decrypt($row->enc_negotiated_costs),
                'negotiated_costs_formatted' => $row->enc_negotiated_costs ? number_format((int)$this->wips->wips_decrypt($row->enc_negotiated_costs)) : '',
                
                'producer_hours' => $row->producer_hours ? $row->producer_hours : '',
                
                'creative_partner_id' => count($partners_info['partners_ids']) ? $partners_info['partners_ids'][0] : '0',
                'creative_partner_name' => count($partners_info['partners_names']) ? reset($partners_info['partners_names']) : '',
                'talent' => $this->wips->wips_decrypt($row->enc_talent),
                
                'nda' => $row->nda,
                'usage_form' => $row->usage_form,
                
                'lead_account_handler_id' => $row->lead_account_handler,
                'lead_account_handler_name' => $this->get_people_name_by_id($row->lead_account_handler),
                
                'lead_creative_id' => $row->lead_creative,
                'lead_creative_name' => $this->get_people_name_by_id($row->lead_creative),
                
                'priority_job' => $row->priority_job,
                'file_directory_path' => $row->file_directory_path ? $row->file_directory_path : '', 
                
                // stage...
                'stage_id' => $row->ps_id,
                'stage_name' => $row->stage,

                // types...
                'types' => $types_info['types_ids'],
                'types_names' => $types_info['types_names'],
                'types_names_string' => implode(", ", $types_info['types_names']),
                
                // project team...
                //'project_team' => $this->wips->get_job_project_team($pr_id, strtolower($job->job_type)),
                
            );
        }
        
        return $production_record;
    }
    
    //public function firstxwords($s, $c = 5) {
    //    return preg_replace('/((\w+\W*){' . $c . '}(\w+))(.*)/', '${1}', $s);    
    //}    
    
    /**
     * get_production_record_types
     * 
     * get production record types based on production record id
     *
     * @param int $pr_id production record id
     * @return array
     */    
    public function get_production_record_types($pr_id) {
        
        $types_ids = array();
        $types_names = array();
        
        $this->db->select('prt.pt_id, pt.type');
        $this->db->from('production_record_type prt');
        $this->db->join('production_type pt', 'pt.id = prt.pt_id');
        $this->db->where('prt.pr_id', $pr_id);
        
        $query = $this->db->get();
       
        foreach ($query->result() as $row) {
            $types_ids[] = $row->pt_id;
            $types_names[$row->pt_id] = $row->type;
        }
        
        return array(
            'types_ids' => $types_ids, 
            'types_names' => $types_names,
        );
        
    }
    
    /**
     * get_production_record_types
     * 
     * get production record types based on production record id
     *
     * @param int $pr_id production record id
     * @return array
     */    
    public function get_production_record_partners($pr_id) {
        
        $partners_ids = array();
        $partners_names = array(); 
        
        $this->db->select('prp.pp_id, pp.partner');
        $this->db->from('production_record_partners prp');
        $this->db->join('production_partners pp', 'pp.id = prp.pp_id');
        $this->db->where('prp.pr_id', $pr_id);
      
        
        $query = $this->db->get();
       
        foreach ($query->result() as $row) {
            $partners_ids[] = $row->pp_id;
            $partners_names[$row->pp_id] = $row->partner;
        }
        
        return array(
            'partners_ids' => $partners_ids, 
            'partners_names' => $partners_names,
        );
        
        
    }
    
    
    /**
     * get_production_data
     * 
     * get production data based on parent job id or production record 'parent' id
     *
     * @parem int $pjid parent job id
     * @param int $pr_pid production record 'parent' id
     * @return object
     */    
    public function get_production_data($pjid = 0, $pr_pid  = 0) {
        
        $production_record = (object)array();
        
        if ($pjid && !$pr_pid) {
            // this is a new record so we can only get job data at this point. 
            $production_record->pjid = $pjid;
            $production_record->jobinfo = $this->wips->get_job($pjid, true);
            $production_record->records = [];
            
        }
        else if (!$pjid && $pr_pid) {
            $production_record->pjid = $this->get_pjid_from_pr_pid($pr_pid);
            $production_record->jobinfo = $this->wips->get_job($production_record->pjid, false);
            $production_record->records = $this->get_production_records($pr_pid);
        }
        else if ($pjid && $pr_pid) {

            $production_record->pjid = $pjid;
            $production_record->jobinfo = $this->wips->get_job($pjid, true);
            $production_record->records = $this->get_production_records($pr_pid);
        }
        
        return $production_record;
    }
    
    /**
     * get_pjid_from_pr_pid
     * 
     * get parent job id from the production record 'parent' id
     *
     * @param int $pr_pid production record 'parent' id
     * @return int
     */    
    public function get_pjid_from_pr_pid($pr_pid) {
        
        $this->db->select('prr.pjid');
        $this->db->from('production_record_ref prr');
        $this->db->where('prr.pr_pid', $pr_pid);
        
        $query = $this->db->get();
        $row = $query->row(); 
        
        return $row->pjid;
    }
  
    /**
     * get_pjid_from_pr_id
     * 
     * get parent job id from the production record id
     *
     * @param int $pr_id production record id
     * @return int
     */    
    public function get_pjid_from_pr_id($pr_id) {
        
        $this->db->select('prr.pjid');
        $this->db->from('production_record_ref prr');
        $this->db->join('production_record pr', 'pr.pr_pid = prr.pr_pid');
        $this->db->where('pr.pr_id', $pr_id);
        
        $query = $this->db->get();
        $row = $query->row(); 
        
        return $row->pjid;
    }        
        
    /**
     * get_people_name_by_id
     * 
     * get persons name based on their id
     *
     * @param int $id persons id
     * @return string
     */         
    public function get_people_name_by_id($id) {
        
        $this->db->select('*');
        $this->db->from('wips_persons');
        $this->db->where('id', $id);
        
        $query = $this->db->get();
        $row = $query->row(); 
        
        if ($row) {
            return $row->name;
        }
        return '';
            
    }
    
    /**
     * get_production_stages
     * 
     * get production stages
     *
     * @return array
     */         
    public function get_production_stages() {
        
        $stages = array();
        
        $this->db->select('id, stage');
        $this->db->from('production_stage');
        $this->db->order_by('stage', 'ASC');
       
        $query = $this->db->get();
        
        foreach ($query->result() as $row) {
            $stages[] = array(
                'id' =>  $row->id,
                'stage' => $row->stage,
            );
        }
        return $stages;
    }
    
    /**
     * get_production_types
     * 
     * get production types
     *
     * @return array
     */             
    public function get_production_types() {
        
        $types = array();
        
        $this->db->select('id, type');
        $this->db->from('production_type');
        $this->db->order_by('rank', 'ASC');
        $this->db->order_by('type', 'ASC');
       
        $query = $this->db->get();
        
        foreach ($query->result() as $row) {
            $types[] = array(
                'id' =>  $row->id,
                'type' => $row->type,
            );
        }
        return $types;
    }
    
    /**
     * get_production_partners
     * 
     * get production partners
     *
     * @return array
     */             
    public function get_production_partners() {
        
        $types = array();
        
        $this->db->select('id, partner, type');
        $this->db->from('production_partners');
        $this->db->order_by('rank', 'ASC');
        $this->db->order_by('partner', 'ASC');
        $this->db->where('status', 1);
       
        $query = $this->db->get();
        
        foreach ($query->result() as $row) {
            $types[] = array(
                'id' =>  $row->id,
                'partner' => $row->partner,
                'type' => $row->type,
            );
        }
        return $types;
    }
    
    /**
     * get_wips_lead_account_handler
     * 
     * get wips lead account object by pjid
     *
     * @param int $pjid parent job id
     * @return object
     */            
    public function get_wips_lead_account_handler($pjid) {
        
        $this->db->select('wp.name');
        $this->db->from('wips_jobs_people_accounts wjpa');
        $this->db->join('wips_jobs wj', 'wj.id = wjpa.wj_id');
        $this->db->join('wips_persons wp', 'wp.id = wjpa.wp_id');
        $this->db->where('wj.pjid', $pjid);
        $this->db->where('wjpa.rank', 1);
                
        $query = $this->db->get();
        $row = $query->row(); 
        
        if ($row)
            return $row;

        return false;
    }
    
    /**
     * save_production_record
     * 
     * Save the full production record
     *
     * @param int $pjid parent job id
     * @param int $pr_pid production record 'parent' id
     * @param object record
     * @return object
     */        
    public function save_production_record($pjid, $pr_pid, $record) {
        
        // Step 1. If $pr_pid is not set then we need to add a new record into
        // production_record_ref and get the pr_pid value.
        if (!$pr_pid || $pr_pid == 0) {
            $this->db->insert('production_record_ref', array('pjid'=>$pjid));
            $pr_pid = $this->db->insert_id(); 
        }

        // Step 2. updated previous records so they are no longer current
        $this->db->where('pr_pid', $pr_pid);
        $this->db->where('current', 1);
        $this->db->update('production_record', array('current' => 0));
        
        $userdata = $this->session->userdata('user');

        // Step 3. insert main data into production_record
        $data = array(
            'pr_pid' => $pr_pid,
            
            /* CAPI-fied */
            'created_by' => 0, 
            '_created_by' => $this->session->userdata('urid'), 
            '_created_by_name' => $userdata->full_name, 
            
            'created_on' => time(),
            'current' => 1,
            'lead_producer' => $record->leadProducer,
            'support_producer' => $record->supportProducer,
            'enc_description' => $this->wips->wips_encrypt($record->description),
            'deadline' => $record->deadline ? date('Y-m-d', strtotime($record->deadline)) : NULL,
            'enc_status_notes' => $this->wips->wips_encrypt($record->stageNotes),
            'enc_agreed_budget' => $this->wips->wips_encrypt($record->agreedBudget),
            'enc_negotiated_costs' => $this->wips->wips_encrypt($record->negotiatedCosts),
            'producer_hours' => $record->producerHours, 
            'enc_talent' => $this->wips->wips_encrypt($record->talent),
            'nda' => $record->nda,
            'usage_form' => $record->usageForm,
            'lead_account_handler' => $record->leadAccountHandlerID,
            'lead_creative' => $record->leadCreativeID, 
            'priority_job' => $record->priorityJob,
            'file_directory_path' => $record->fileDirectoryPath    
        );
            
        $this->db->insert('production_record', $data);
        
        // Step 4. Get the new id
        $pr_id = $this->db->insert_id();
        
        // Step 5. Save stage
        $this->db->insert('production_record_stage', array(
            'pr_id' => $pr_id,
            'ps_id' => $record->stage)
        );
        
        // Step 6. Save types
        foreach($record->type as $key => $pt_id) {
            $this->db->insert('production_record_type', array(
                'pr_id' => $pr_id, 
                'pt_id' => $pt_id)
            );
        }

        // Step 7. Save Production Partner
        if ($record->creativePartnerID == 106 ) {// other 
            // we should have a new creative partner to add to the pot...
            $new_creative_partner = $record->creativePartnerOther;
            
            // check to see if it exists...
            $creativePartnerID = $this->get_creative_partner_by_name($new_creative_partner);
                    
            if (!$creativePartnerID) {
                // insert a new one and get the id
                $this->db->insert('production_partners', array('partner' => $new_creative_partner, 'type' => NULL, 'status' => 1, 'rank' => 0));
                $creativePartnerID = $this->db->insert_id(); 
            }    
        }
        else {
            $creativePartnerID = $record->creativePartnerID;
        }
        
        $this->db->insert('production_record_partners', array(
            'pr_id' => $pr_id,
            'pp_id' => $creativePartnerID)
        );
       
        return 'done';
    }
    
    
    function get_creative_partner_by_name($partner) {
        
        $sql = "SELECT id FROM production_partners WHERE lower(partner) like '". strtolower($partner) . "'";
        $query = $this->db->query($sql);
        $row = $query->row(); 
        if ($row)
            return $row->id;
        else 
            return false;
        
    }
    
    
    /**
     * get_report_three_stats
     * 
     * Get the stats user to generate report 3
     *
     * @return array
     */        
    function get_report_three_stats() {
        
        $closed_states = array(9,10); 
        $cpstats = array(); 
        $cpstats['totals'] = array(
            'name' => 'Totals',
            'count' => 0,
            'onhold' => 0,
            'budget' => 0,
        );
 
        $this->db->select('pp.partner as partner, pp.id as partner_id, pr.enc_agreed_budget as budget, prs.ps_id as stageid');
        $this->db->from('production_record pr');
        $this->db->join('production_record_stage prs', 'prs.pr_id = pr.pr_id');
        $this->db->join('production_record_partners prp', 'prp.pr_id = pr.pr_id');
        $this->db->join('production_partners pp', 'pp.id = prp.pp_id');
        $this->db->where('prs.ps_id NOT IN (' . implode(',', $closed_states) . ')');
        $this->db->where('current', 1);
       
        $query = $this->db->get();
       
        foreach ($query->result() as $row) {
         //   $partner = $this->wips->wips_decrypt($row->partner);
            $key = $row->partner_id;
            if (isset($cpstats[$key])) {
                $cpstats[$key]['count']++;
                $cpstats[$key]['onhold'] = $row->stageid == 2 ? $cpstats[$key]['onhold']+1 : $cpstats[$key]['onhold'];
                $cpstats[$key]['budget'] += (int)$this->wips->wips_decrypt($row->budget);
            }
            else {
                $cpstats[$key] = array(
                    'name' => $row->partner,
                    'count' => 1,
                    'onhold' => $row->stageid == 2 ? 1 : 0,
                    'budget' => (int)$this->wips->wips_decrypt($row->budget)
                );
            }
            $cpstats['totals']['count']++;
            $cpstats['totals']['onhold'] = $row->stageid == 2 ? $cpstats['totals']['onhold']+1 : $cpstats['totals']['onhold'];
            $cpstats['totals']['budget'] = $cpstats['totals']['budget'] + (int)$this->wips->wips_decrypt($row->budget);
        }
        
        foreach($cpstats as $key=>$value) {
            $cpstats[$key]['budget'] = number_format($value['budget']);
        }

        return $cpstats;
    }
    
    /**
     * get_report_two_stats
     * 
     * Get the stats user to generate report 2
     *
     * @return array
     */        
    function get_report_two_stats() {
        
        $closed_states = array(9,10); 
        $jtstats = array(); 
        $jtstats['totals'] = array(
            'name' => 'Totals',
            'count' => 0,
            'onhold' => 0,
            'budget' => 0,
        );
       
        $this->db->select('pj.type as type_id, jt.type as type_name, pr.enc_agreed_budget as budget, prs.ps_id as stageid');
        $this->db->from('production_record pr');
        $this->db->join('production_record_stage prs', 'prs.pr_id = pr.pr_id');
        $this->db->join('production_record_ref prr', 'pr.pr_pid = prr.pr_pid');
        $this->db->join('parent_jobs pj', 'prr.pjid = pj.id');
        $this->db->join('job_types jt', 'jt.id = pj.type');
        $this->db->where('prs.ps_id NOT IN (' . implode(',', $closed_states) . ')');
        $this->db->where('current', 1);
       
        $query = $this->db->get();
       
        foreach ($query->result() as $row) {
            $key = $row->type_id;
            if (isset($jtstats[$key])) {
                $jtstats[$key]['count']++;
                $jtstats[$key]['onhold'] = $row->stageid == 2 ? $jtstats[$key]['onhold']+1 : $jtstats[$key]['onhold'];
                $jtstats[$key]['budget'] += (int)$this->wips->wips_decrypt($row->budget);
            }
            else {
                $jtstats[$key] = array(
                    'name' => $row->type_name,
                    'count' => 1,
                    'onhold' => $row->stageid == 2 ? 1 : 0,
                    'budget' => (int)$this->wips->wips_decrypt($row->budget)
                );
            }
            $jtstats['totals']['count']++;
            $jtstats['totals']['onhold'] = $row->stageid == 2 ? $jtstats['totals']['onhold']+1 : $jtstats['totals']['onhold'];
            $jtstats['totals']['budget'] = $jtstats['totals']['budget'] + (int)$this->wips->wips_decrypt($row->budget);
            
        }
        
        foreach($jtstats as $key=>$value) {
            $jtstats[$key]['budget'] = number_format($value['budget']);
        }
       // $jtstats['totals']['budget'] = number_format($jtstats['totals']['budget']);
        
        
        return $jtstats;
    }
    
    /**
     * get_report_one_stats
     * 
     * Get the stats user to generate report 1
     *
     * @return array
     */        
    function get_report_one_stats() {
        
        $stats = array(); 
        $producers = $this->get_producers();
        
      //  print_r($producers);
        $types = $this->get_production_types();
        
     //   $stats['producers'] = $producers;
        $stats['types'] = $types;
        
        $totals = array(); 
        $titles = array();
        
        $totals['active'] = 0;
        $totals['onhold'] = 0;
        $totals['budget'] = 0;
        
        foreach($types as $key=>$value) {
            $totals['type_' . $value['id']] = 0;
            $titles['type_' . $value['id']] = $value['type'];
        }
        
        $stats['titles'] = $titles;
        $stats['totals'] = $totals;
        
      
        
        /*
        
        $totals = array(
            'active' => 0,
            'onhold' => 0,
            'type_1' => 0, // Merchandise
            'type_2' => 0, // Film
            'type_3' => 0, // Illustration
            'type_4' => 0, // Post Production
            'type_5' => 0, // Animation
            'type_6' => 0, // Photography
            'type_7' => 0, // Stills 
            'type_8' => 0, // Brochure
            'type_9' => 0, // CGI
            'type_10' => 0, // Experiental
            'type_11' => 0, // Radio
            'type_12' => 0, // Voice Over
            'budget' => 0
        );
         * */
         
        $stats['producers'] = array();    
            
        foreach($producers as $key=>$value) {
            $producer_stats = $this->get_producer_stats($value['id']);
            
            $totals['active'] += $producer_stats['active'];
            $totals['onhold'] += $producer_stats['onhold'];
            $totals['budget'] += $producer_stats['budget'];
            
            foreach($types as $key2=>$value2) {
                $totals['type_' . $value2['id']] += isset($producer_stats['type_' . $value2['id']]) ? $producer_stats['type_' . $value2['id']] : 0;
            }
            
            /*
            $totals['type_1'] += isset($producer_stats['type_1']) ? $producer_stats['type_1'] : 0;
            $totals['type_2'] += isset($producer_stats['type_2']) ? $producer_stats['type_2'] : 0;
            $totals['type_3'] += isset($producer_stats['type_3']) ? $producer_stats['type_3'] : 0;
            $totals['type_4'] += isset($producer_stats['type_4']) ? $producer_stats['type_4'] : 0;
            $totals['type_5'] += isset($producer_stats['type_5']) ? $producer_stats['type_5'] : 0;
            $totals['type_6'] += isset($producer_stats['type_6']) ? $producer_stats['type_6'] : 0;
            $totals['type_7'] += isset($producer_stats['type_7']) ? $producer_stats['type_7'] : 0;
            $totals['type_8'] += isset($producer_stats['type_8']) ? $producer_stats['type_8'] : 0; 
            $totals['type_9'] += isset($producer_stats['type_9']) ? $producer_stats['type_9'] : 0;
            $totals['type_10'] += isset($producer_stats['type_10']) ? $producer_stats['type_10'] : 0;
            $totals['type_11'] += isset($producer_stats['type_11']) ? $producer_stats['type_11'] : 0;
            $totals['type_12'] += isset($producer_stats['type_12']) ? $producer_stats['type_12'] : 0;
            $totals['budget'] += $producer_stats['budget'];
               */
            $producer_stats['budget'] = number_format($producer_stats['budget']);
            
            $stats['producers'][] = array(
                'person' => array(
                    'id' => $value['id'],
                    'name' => $value['name']
                ),
                'stats' => $producer_stats,
            );
        }
        
           
       
        
        
        
        $totals['budget'] = number_format($totals['budget']);
        $stats['totals'] = $totals;
        
        return $stats;
    }

    /**
     * get_producer_stats
     * 
     * Get producer stats (for report 1)
     * 
     * @param int $pid producer id
     * @return array
     */        
    function get_producer_stats($pid) {
        
        $closed_states = array(9,10); 
        $tstats = array(); 
        $pstats = array(); 
        
        $types = $this->get_production_types();
        
        $this->db->select('COUNT(pr.pr_id) as count, prt.pt_id as type');
        $this->db->from('production_record pr');
        $this->db->join('production_record_type prt', 'prt.pr_id = pr.pr_id');
        $this->db->join('production_record_stage prs', 'prs.pr_id = pr.pr_id');
        $this->db->where('prs.ps_id NOT IN (' . implode(',', $closed_states) . ')');
        $this->db->where('pr.current', 1);
        $this->db->where('pr.lead_producer', $pid);
        $this->db->group_by('prt.pt_id');
        $this->db->order_by('prt.pt_id', 'ASC');
        
        $query = $this->db->get();
        
        $total = 0;
        
        foreach ($query->result() as $row) {
            $tstats['type_'. $row->type] = $row->count;
            $total += $row->count;
        }
    
        foreach($types as $key=>$value) {
            $pstats['type_'. $value['id']] = isset($tstats['type_'. $value['id']]) ? $tstats['type_'. $value['id']] : 0;
        }
        
        $pstats['total'] = $total;
        $pstats['active'] = $this->get_active_by_producer($pid);
        $pstats['onhold'] = $this->get_on_hold_by_producer($pid);
        $pstats['budget'] = $this->get_budget_by_producer($pid);
        return $pstats;
    }
    
    /**
     * get_active_by_producer
     * 
     * Get active records by producer (for report 1)
     * 
     * @param int $pid producer id
     * @return array
     */        
    function get_active_by_producer($pid) {
        
        $closed_states = array(9,10); // completed, cancelled
        
        $production_records = [];
        
        $this->db->select('COUNT(pr.pr_id) as active');
        $this->db->from('production_record pr');
        $this->db->join('production_record_stage prs', 'prs.pr_id = pr.pr_id');
        $this->db->where('prs.ps_id NOT IN (' . implode(',', $closed_states) . ')');
        $this->db->where('pr.lead_producer', $pid);
        $this->db->where('pr.current', 1);
        
        $query = $this->db->get();
        $row = $query->row(); 
        
        if ($row)
            return $row->active;

        return false;
    }
    
    
    /**
     * get_on_hold_by_producer
     * 
     * Get on hold records by producer (for report 1)
     * 
     * @param int $pid producer id
     * @return array
     */        
    function get_on_hold_by_producer($pid) {
        
        $on_hold_state = 2; // completed, cancelled
        
        $production_records = [];
        
        $this->db->select('COUNT(pr.pr_id) as onhold');
        $this->db->from('production_record pr');
        $this->db->join('production_record_stage prs', 'prs.pr_id = pr.pr_id');
        $this->db->where('prs.ps_id', $on_hold_state);
        $this->db->where('pr.lead_producer', $pid);
        $this->db->where('pr.current', 1);
        
        $query = $this->db->get();
        $row = $query->row(); 
        
        if ($row)
            return $row->onhold;

        return false;
    }
    
    /**
     * get_budget_by_producer
     * 
     * Get budget by producer (for report 1)
     * 
     * @param int $pid producer id
     * @return array
     */        
    function get_budget_by_producer($pid) {
        
        $closed_states = array(9,10); // completed, cancelled
        
        $budget = 0;
        
        $this->db->select('pr.enc_agreed_budget as budget');
        $this->db->from('production_record pr');
        $this->db->join('production_record_stage prs', 'prs.pr_id = pr.pr_id');
        $this->db->where('prs.ps_id NOT IN (' . implode(',', $closed_states) . ')');
        $this->db->where('pr.current', 1);
        $this->db->where('pr.lead_producer', $pid);
             
        $query = $this->db->get();
        $row = $query->row(); 
        
        foreach ($query->result() as $row) {
            if ($row->budget) {
                $budget += (int)$this->wips->wips_decrypt($row->budget);
            }
        }

        return $budget;
    }
    
    /**
     * get_producers
     * 
     * Get all producers
     * 
     * @return array
     */        
    function get_producers() {
        
        $producers = array();
        
        $this->db->select('id, name');
        $this->db->from('wips_persons');
        $this->db->where('tid', 4);
        $this->db->where('active', 1);
        $this->db->order_by('name', 'ASC');
        
        $query = $this->db->get();
        
        foreach ($query->result() as $row) {
            $producers[] = array(
                'id' =>  $row->id,
                'name' => $row->name,
            );
        }
        
        return $producers;
    }
    
}
