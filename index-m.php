<html lang="en"><head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <title>Langland WIPs Status</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="/public/css/bootstrap-css/css/bootstrap.min.css">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i" rel="stylesheet">
    
    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="/public/css/main.css">

</head>
<body class="login-page localhost">

    <div class="container-fluid">

        <div class="content">

            <div class="col-sm-1 col-md-2 col-lg-3"></div>    
            <div class="col-sm-10 col-md-8 col-lg-6" style="text-align: center; margin-top: 50px;" >
                  <h2 style="color: #5d666c"><span class="glyphicon glyphicon-info-sign" aria-hidden="true" style="font-size: 50px;"></span><br /><br />
                        The Langland Internal Tools are currently unavailable whilst we perform some essential maintenance.<br /><br />
                        They will be back up and running from 9am on Monday morning (1st March).</h2>  
            </div>
            <div class="col-sm-1 col-md-2 col-lg-3"></div>  

        </div>
        
    </div>
    


</body></html>